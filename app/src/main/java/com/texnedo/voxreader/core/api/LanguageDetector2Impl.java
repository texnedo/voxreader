package com.texnedo.voxreader.core.api;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.texnedo.voxreader.system.Config;
import com.texnedo.voxreader.utils.FileLog;

import java.util.Locale;

public class LanguageDetector2Impl implements LanguageDetector {
    private static final int MIN_TEXT_LENGTH_TO_DETECT = 500;
    private static final String LOG_TAG = "LanguageDetector";
    private final Config config;
    private StringBuilder builder;
    private Locale detected;

    public LanguageDetector2Impl(@NonNull Config config) {
        this.config = config;
    }

    @Override
    public void process(@NonNull String text) {
        if (TextUtils.isEmpty(text)) {
            return;
        }
        if (detected != null) {
            return;
        }
        if (builder == null) {
            builder = new StringBuilder(MIN_TEXT_LENGTH_TO_DETECT);
        }
        builder.append(text);
        if (builder.length() > MIN_TEXT_LENGTH_TO_DETECT) {
            detect();
        }
    }

    private void detect() {
        if (builder == null || builder.length() == 0 || detected != null) {
            return;
        }
        try {
            FileLog.d(LOG_TAG, "try to detect language after length %d", builder.length());
            String locale = detect(builder.toString());
            if (TextUtils.isEmpty(locale)) {
                FileLog.d(LOG_TAG, "failed to detect language after length %d", builder.length());
                return;
            }
            detected = new Locale(locale);
        } catch (Exception e) {
            FileLog.e(LOG_TAG, "failed to detect text %s language", builder);
        }
        builder.setLength(0);
        builder = null;
    }

    @Override
    public Locale getDetectedLocale() {
        return detected;
    }

    @Override
    public void complete() {
        detect();
    }

    private native String detect(String text);

    static {
        System.loadLibrary("languagedetector");
    }
}
