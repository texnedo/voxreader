package com.texnedo.voxreader.storage;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.texnedo.voxreader.core.timeline.BaseTimeLineItem;
import com.texnedo.voxreader.core.timeline.TimeLineItem;
import com.texnedo.voxreader.storage.cache.TimeLineCache;
import com.texnedo.voxreader.system.Config;
import com.texnedo.voxreader.utils.FileLog;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class TimeLineStorageImpl implements TimeLineStorage {
    private static final String LOG_TAG = "TimeLineStorage";
    private static final int TRANSACTION_SIZE = 1000;
    private static final int MEMORY_CACHE_ITEMS_SIZE = 2000;
    private static final int MEMORY_CACHE_RANGE_SIZE = 50;
    private static final String RANGE_ID_QUERY_FORMAT = "%s>=%d AND %s<=%d";
    private static final String COUNT_ITEMS_QUERY = "select count(*) from " + TimeLineOpenHelper.TABLE_TIMELINE;
    private final Config config;
    private final String timelineName;
    private final TimeLineCache timeLineCache;
    private final ItemFactory itemFactory;
    private final ConcurrentHashMap<ItemsCountChangedListener, Boolean> listeners
            = new ConcurrentHashMap<>();
    private SQLiteDatabase database;
    private ItemTransaction transaction;
    private boolean fileChecked = false;
    private AtomicLong itemId = new AtomicLong();

    public TimeLineStorageImpl(@NonNull Config config,
                               @NonNull ItemFactory itemFactory,
                               @NonNull String timelineName) {
        this.config = config;
        this.itemFactory = itemFactory;
        this.timelineName = timelineName;
        this.timeLineCache = new TimeLineCache(MEMORY_CACHE_ITEMS_SIZE, MEMORY_CACHE_RANGE_SIZE);
    }

    @Override
    public TimeLineStorage saveItem(@NonNull final BaseTimeLineItem item) {
        config.getStorageExecutor().submit(new Runnable() {
            @Override
            public void run() {
                checkFileNotExists();
                checkBeginTransaction();
                try {
                    TimeLineModel model = item.getModel();
                    long id = getDatabase().insert(
                            TimeLineOpenHelper.TABLE_TIMELINE,
                            null,
                            model.getData()
                    );
                    if (id == -1) {
                        FileLog.e(LOG_TAG, "failed to save timeline item %s", item);
                        rollbackTransaction();
                    }
                    //make id to be a zero based index
                    model.setId(id - 1);
                    timeLineCache.put(item, false);
                } catch (Exception ex) {
                    FileLog.e(LOG_TAG, ex, "failed to save timeline item %s", item);
                    rollbackTransaction();
                }
                checkEndTransaction();
            }
        });
        return this;
    }

    @Override
    public void loadItemsRange(final long from, @NonNull final ItemsReceiver receiver) {
        final long rangeFrom = from / MEMORY_CACHE_RANGE_SIZE * MEMORY_CACHE_RANGE_SIZE;
        final long rangeTo = rangeFrom + MEMORY_CACHE_RANGE_SIZE - 1;
        boolean foundInCache = findCacheItems(rangeFrom, rangeTo, new ItemsReceiver() {
            @Override
            public void onComplete(@NonNull List<TimeLineItem> items) {
                receiver.onComplete(filterRangeItems(items, rangeTo, from, rangeFrom));
            }

            @Override
            public void onFailure(@NonNull Exception ex) {
                receiver.onFailure(ex);
            }
        });
        if (foundInCache) {
            return;
        }
        loadItems(rangeFrom, rangeTo, new ItemsReceiver() {
            @Override
            public void onComplete(@NonNull List<TimeLineItem> items) {
                receiver.onComplete(filterRangeItems(items, rangeTo, from, rangeFrom));
            }

            @Override
            public void onFailure(@NonNull Exception ex) {
                receiver.onFailure(ex);
            }
        });
    }

    @NonNull
    private ArrayList<TimeLineItem> filterRangeItems(@NonNull List<TimeLineItem> items, long rangeTo, long from, long rangeFrom) {
        int resultLength = (int) (rangeTo - from + 1);
        int resultOffset = (int) (from - rangeFrom);
        ArrayList<TimeLineItem> result = new ArrayList<>(resultLength);
        for (int i = resultOffset; i < items.size(); i++) {
            result.add(items.get(i));
        }
        return result;
    }

    private void checkEndTransaction() {
        if (transaction == null) {
            return;
        }
        transaction.increment();
        if (!transaction.canAddMoreItems()) {
            completeTransaction();
        }
    }

    private void checkBeginTransaction() {
        if (transaction == null) {
            FileLog.d(LOG_TAG, "transaction started for %s", timelineName);
            transaction = new ItemTransaction();
            getDatabase().beginTransaction();
        }
    }

    private void rollbackTransaction() {
        if (transaction == null) {
            throw new IllegalStateException("Transaction must be started before completion");
        }
        getDatabase().endTransaction();
        transaction = null;
    }

    private void completeTransaction() {
        if (transaction == null) {
            FileLog.v(LOG_TAG, "no started transaction found");
            return;
        }
        try {
            getDatabase().setTransactionSuccessful();
            for (ItemsCountChangedListener listener : listeners.keySet()) {
                listener.onComplete(transaction.getCount());
            }
            FileLog.d(LOG_TAG, "transaction with %s items completed", transaction.getCount());
        } catch (Exception ex) {
            FileLog.e(LOG_TAG, "failed to mark transaction as successful", ex);
        } finally {
            getDatabase().endTransaction();
        }
        FileLog.d(LOG_TAG, "transaction ended");
        transaction = null;
    }

    private void checkFileNotExists() {
        if (!fileChecked) {
            deleteInternal();
            fileChecked = true;
        }
    }

    @Override
    public void flush() {
        config.getStorageExecutor().submit(new Runnable() {
            @Override
            public void run() {
                completeTransaction();
            }
        });
    }

    @Override
    public void loadAllItems(@NonNull ItemsReceiver receiver) {
        loadItemsInternal(receiver, null);
    }

    private void loadItemsInternal(@NonNull final ItemsReceiver receiver, @Nullable final String query) {
        config.getStorageExecutor().submit(new Runnable() {
            @Override
            public void run() {
                if (!exists()) {
                    //noinspection unchecked
                    receiver.onComplete(Collections.EMPTY_LIST);
                    return;
                }
                Cursor cursor = null;
                try {
                    cursor = getDatabase().query(TimeLineOpenHelper.TABLE_TIMELINE, null, query, null, null, null, null);
                    if (cursor.moveToFirst()) {
                        List<TimeLineItem> items = new ArrayList<>(cursor.getCount());
                        while (!cursor.isAfterLast()) {
                            items.add(itemFactory.create(new TimeLineModel(cursor)));
                            cursor.moveToNext();
                        }
                        receiver.onComplete(items);
                        FileLog.d(LOG_TAG, "%d items found using request %s", items.size(), query);
                    } else {
                        //noinspection unchecked
                        receiver.onComplete(Collections.EMPTY_LIST);
                        FileLog.d(LOG_TAG, "no items found using request %s", query);
                    }
                } catch (Exception ex) {
                    FileLog.e(LOG_TAG, "failed to read all items", ex);
                    receiver.onFailure(ex);
                } finally {
                    if (cursor != null) {
                        cursor.close();
                    }
                }
            }
        });
    }

    @Override
    public void loadItems(long from, long to, @NonNull final ItemsReceiver receiver) {
        if (findCacheItems(from, to, receiver)) {
            return;
        }
        loadItemsInternal(
                new ItemsReceiver() {
                    @Override
                    public void onComplete(@NonNull List<TimeLineItem> items) {
                        receiver.onComplete(items);
                        timeLineCache.putAll(items, true);
                    }

                    @Override
                    public void onFailure(@NonNull Exception ex) {
                        receiver.onFailure(ex);
                    }
                },
                String.format(
                        Locale.US,
                        RANGE_ID_QUERY_FORMAT,
                        TimeLineModel.COLUMN_ID,
                        from + 1,
                        TimeLineModel.COLUMN_ID,
                        to + 1
                )
        );
    }

    private boolean findCacheItems(long from, long to, @NonNull ItemsReceiver receiver) {
        if (from == to) {
            TimeLineItem item = timeLineCache.get(from);
            if (item != null) {
                receiver.onComplete(Collections.singletonList(item));
                return true;
            }
        } else {
            List<TimeLineItem> items = timeLineCache.getAll(from, to);
            if (items != null) {
                receiver.onComplete(items);
                return true;
            }
        }
        return false;
    }

    @Override
    public void loadItem(long id, @NonNull final ItemsReceiver receiver) {
        loadItems(id, id, receiver);
    }

    @Override
    public void loadCount(@NonNull final ItemsCountReceiver receiver) {
        final long currentCount = itemId.get();
        if (currentCount != 0) {
            receiver.onComplete(currentCount);
        } else {
            config.getStorageExecutor().submit(new Runnable() {
                @Override
                public void run() {
                    if (!exists()) {
                        //noinspection unchecked
                        receiver.onComplete(0);
                        return;
                    }
                    try {
                        SQLiteStatement countStatement =
                                getDatabase().compileStatement(COUNT_ITEMS_QUERY);
                        long totalCount = countStatement.simpleQueryForLong();
                        itemId.compareAndSet(currentCount, totalCount);
                        receiver.onComplete(totalCount);
                    } catch (Exception ex) {
                        FileLog.e(LOG_TAG, "failed to count items", ex);
                        receiver.onFailure(ex);
                    }
                }
            });
        }
    }

    @Override
    public int getRangeSize() {
        return MEMORY_CACHE_RANGE_SIZE;
    }

    @Override
    public List<TimeLineItem> getItems(long from, long to) {
        return timeLineCache.getAll(from, to);
    }

    @Override
    public TimeLineItem getItem(long id) {
        return timeLineCache.get(id);
    }

    private void deleteInternal() {
        timeLineCache.evictAll();
        itemId.set(0);
        fileChecked = false;
        closeDatabase();
        String fileName = TimeLineOpenHelper.getDatabaseFileName(timelineName);
        try {
            File file = config.getContext().getDatabasePath(fileName);
            if (file.exists() && !file.delete()) {
                File fileTmp = config.getContext().getDatabasePath(fileName + "-tmp");
                if (!file.renameTo(fileTmp)) {
                    FileLog.e(LOG_TAG, "failed to rename database file before deletion");
                    return;
                }
                FileLog.d(LOG_TAG, "temporary file deletion result = %s", fileTmp.delete());
            }
            FileLog.d(LOG_TAG, "timeline storage %s has been removed", timelineName);
        } catch (Exception ex) {
            FileLog.e(LOG_TAG, ex, "failed to delete database file");
        }
    }

    @Override
    public void delete() {
        config.getStorageExecutor().submit(new Runnable() {
            @Override
            public void run() {
                deleteInternal();
            }
        });
    }

    @Override
    public boolean exists() {
        String fileName = TimeLineOpenHelper.getDatabaseFileName(timelineName);
        try {
            File file = config.getContext().getDatabasePath(fileName);
            return file.exists();
        } catch (Exception ex) {
            FileLog.e(LOG_TAG, ex, "failed to check database file");
            return false;
        }
    }

    @Override
    public void addItemsCountChangedListener(@NonNull ItemsCountChangedListener listener) {
        listeners.put(listener, true);
    }

    @Override
    public void removeItemsCountChangedListener(@NonNull ItemsCountChangedListener listener) {
        listeners.remove(listener);
    }

    @Override
    public TimeLineModel createModel(int type, int flags, int duration, String text, String meta) {
        return new TimeLineModel(itemId.getAndIncrement(), type, flags, duration, text, meta);
    }

    private void closeDatabase() {
        if (database != null) {
            database.close();
            database = null;
        }
    }

    private SQLiteDatabase getDatabase() {
        if (database == null) {
            database = new TimeLineOpenHelper(config.getContext(), timelineName).getWritableDatabase();
        }
        return database;
    }

    private static class ItemTransaction {
        private int count = 0;

        public boolean increment() {
            if (count >= TRANSACTION_SIZE) {
                return false;
            }
            count++;
            return true;
        }

        public boolean canAddMoreItems() {
            return count < TRANSACTION_SIZE;
        }

        int getCount() {
            return count;
        }
    }
}
