package com.texnedo.voxreader.system;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;

import com.texnedo.voxreader.BuildConfig;
import com.texnedo.voxreader.core.api.DataSource;
import com.texnedo.voxreader.core.api.LanguageDetector;
import com.texnedo.voxreader.core.api.LanguageDetector2Impl;
import com.texnedo.voxreader.core.api.TextToSpeechProvider;
import com.texnedo.voxreader.core.api.TimeLineBuilder;
import com.texnedo.voxreader.core.api.TimeLineBuilderImpl;
import com.texnedo.voxreader.net.CachedHttpConnectionImpl;
import com.texnedo.voxreader.net.ClientException;
import com.texnedo.voxreader.net.ConnectionBuilder;
import com.texnedo.voxreader.sound.TextToSpeechProviderImpl;
import com.texnedo.voxreader.storage.DatabaseManager;
import com.texnedo.voxreader.storage.DatabaseManagerImpl;
import com.texnedo.voxreader.storage.ItemFactory;
import com.texnedo.voxreader.storage.ItemsChangeListener;
import com.texnedo.voxreader.storage.TimeLineModelFactory;
import com.texnedo.voxreader.storage.TimeLineStorage;
import com.texnedo.voxreader.storage.TimeLineStorageImpl;
import com.texnedo.voxreader.utils.FileLog;
import com.texnedo.voxreader.utils.Utils;

import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class ConfigImpl implements Config {
    private static final String LOG_TAG = "ConfigImpl";
    private static final String DISPATCHER_THREAD_NAME = "dispatcher";
    private static final String NETWORK_WORKER_THREAD_NAME = "network_worker";
    private static final String STORAGE_WORKER_THREAD_NAME = "storage_worker";
    private static final String LAYOUT_RENDERER_THREAD_NAME = "layout_renderer";
    private final UncaughtExceptionHandler uncaughtExceptionHandler = new UncaughtExceptionHandler();
    private final Context context;
    private final Preferences preferences;
    private HandlerThread handlerThread;
    private Handler dispatcher;
    private ExecutorService storageExecutor;
    private ExecutorService networkExecutor;
    private ExecutorService layoutRenderer;
    private TextToSpeechProvider textToSpeechProvider;

    public ConfigImpl(@NonNull Context context) {
        this.context = context;
        this.preferences = new PreferenceImpl(context);
    }

    @Override
    public Context getContext() {
        return context;
    }

    @Override
    public Locale getCurrentLocale() {
        return Locale.getDefault();
    }

    @Override
    public Handler getDispatcher() {
        if (handlerThread == null) {
            handlerThread = new HandlerThread(DISPATCHER_THREAD_NAME);
            handlerThread.setUncaughtExceptionHandler(uncaughtExceptionHandler);
            handlerThread.start();
            dispatcher = new Handler(handlerThread.getLooper());
        }
        return dispatcher;
    }

    @Override
    public ExecutorService getStorageExecutor() {
        if (storageExecutor == null) {
            storageExecutor = Executors.newSingleThreadExecutor(new ThreadFactory() {
                @Override
                public Thread newThread(@NonNull Runnable r) {
                    Thread thread = new Thread(r);
                    thread.setPriority(Thread.NORM_PRIORITY);
                    thread.setName(STORAGE_WORKER_THREAD_NAME);
                    thread.setUncaughtExceptionHandler(uncaughtExceptionHandler);
                    return thread;
                }
            });
        }
        return storageExecutor;
    }

    @Override
    public ExecutorService getNetworkExecutor() {
        if (networkExecutor == null) {
            networkExecutor = Executors.newFixedThreadPool(2, new ThreadFactory() {
                private final AtomicInteger id = new AtomicInteger(0);
                @Override
                public Thread newThread(@NonNull Runnable r) {
                    Thread thread = new Thread(r);
                    thread.setPriority(Thread.NORM_PRIORITY);
                    thread.setName(String.format("%s-%d", NETWORK_WORKER_THREAD_NAME, id.getAndIncrement()));
                    thread.setUncaughtExceptionHandler(uncaughtExceptionHandler);
                    return thread;
                }
            });
        }
        return networkExecutor;
    }

    @Override
    public ExecutorService getLayoutRenderExecutor() {
        if (layoutRenderer == null) {
            layoutRenderer = Executors.newSingleThreadExecutor(new ThreadFactory() {
                @Override
                public Thread newThread(@NonNull Runnable r) {
                    Thread thread = new Thread(r);
                    thread.setPriority(Thread.MAX_PRIORITY);
                    thread.setName(LAYOUT_RENDERER_THREAD_NAME);
                    thread.setUncaughtExceptionHandler(uncaughtExceptionHandler);
                    return thread;
                }
            });
        }
        return layoutRenderer;
    }

    @Override
    public ConnectionBuilder getConnectionBuilder(@NonNull String url) throws IOException, ClientException {
        return CachedHttpConnectionImpl.getBuilder(context, url);
    }

    @Override
    public TimeLineStorage getStorage(@NonNull DataSource dataSource,
                                      @NonNull ItemFactory itemFactory) {
        return new TimeLineStorageImpl(this, itemFactory, Utils.stringToMD5(dataSource.getId()));
    }

    @Override
    public DatabaseManager getDatabaseManager(@NonNull ItemsChangeListener listener) {
        return new DatabaseManagerImpl(this, listener);
    }

    @Override
    public TextToSpeechProvider getTTS() {
        //due to the bug in TextToSpeech system class, it is better to have a single instance
        //per application
        if (textToSpeechProvider == null) {
            textToSpeechProvider = new TextToSpeechProviderImpl(this);
        }
        return textToSpeechProvider;
    }

    @Override
    public TimeLineBuilder getTimelineBuilder(@NonNull TimeLineModelFactory factory,
                                              @NonNull TimeLineBuilder.Receiver receiver) {
        return new TimeLineBuilderImpl(this, factory, receiver);
    }

    @Override
    public LanguageDetector getLanguageDetector() {
        return new LanguageDetector2Impl(this);
    }

    @Override
    public Preferences getSettings() {
        return preferences;
    }

    private static class UncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
        @Override
        public void uncaughtException(Thread thread, Throwable ex) {
            FileLog.e(LOG_TAG, ex, "FATAL ERROR at %s", thread);
            if (BuildConfig.DEBUG) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException ignored) {
                }
                System.exit(-1);
            }
        }
    }
}
