package com.texnedo.voxreader.ui.notifications;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;

import com.texnedo.voxreader.R;
import com.texnedo.voxreader.core.api.TimeLineAccess;

public class ProgressNotification extends NotificationBase {
    private final TimeLineAccess dataSource;
    private NotificationCompat.Builder builder;

    public ProgressNotification(@NonNull Context context, @NonNull TimeLineAccess dataSource) {
        super(context);
        this.dataSource = dataSource;
    }

    @Override
    public NotificationId getId() {
        return NotificationId.PROGRESS;
    }

    @Override
    public String getTag() {
        return dataSource.getId();
    }

    @Override
    protected NotificationCompat.Builder getBuilder() {
        if (builder == null){
            builder = new NotificationCompat.Builder(context);
            fillBuilder(builder);
        }
        return builder;
    }

    @Override
    protected void fillBuilder(NotificationCompat.Builder builder) {
        super.fillBuilder(builder);
        builder.setSmallIcon(R.drawable.ic_cached_white_36dp);
        builder.setAutoCancel(false);
        builder.setContentTitle(dataSource.getName());
        builder.setContentText(dataSource.getDescription());
        builder.setTicker(dataSource.getName());
        builder.setProgress(0, 0, true);
        builder.setDeleteIntent(getDeleteIntent(context, dataSource));
        builder.setContentIntent(getTimeLineIntent(context, dataSource));
        builder.setStyle(new NotificationCompat.BigTextStyle().bigText(dataSource.getDescription()));
    }

    @Override
    public boolean isSilent() {
        return true;
    }


    @Override
    public boolean shouldReplacePrevious() {
        return false;
    }
}
