package com.texnedo.voxreader.storage;

import com.texnedo.voxreader.storage.dao.DataSourceModel;

public interface ItemsChangeListener {
    void onFailure();

    void onInserted(DataSourceModel dataSource);

    void onDeleted(DataSourceModel dataSource);

    void onUpdated(DataSourceModel dataSource);

    void onCleared();
}
