package com.texnedo.voxreader;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.support.annotation.NonNull;

import com.texnedo.voxreader.net.ClientException;
import com.texnedo.voxreader.net.ConnectionBuilder;
import com.texnedo.voxreader.net.HttpConnectionImpl;
import com.texnedo.voxreader.system.Config;
import com.texnedo.voxreader.system.ConfigImpl;

import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Components {
    public static Config createDefaultConfig(@NonNull final Context context) {
        return new ConfigImpl(context);
    }
}
