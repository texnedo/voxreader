package com.texnedo.voxreader.sound;

import android.support.annotation.NonNull;

class SpeechRateCommand extends Command {
    private float rate;

    @Override
    public String id() {
        return null;
    }

    @Override
    public boolean waitForCompletion() {
        return false;
    }

    protected SpeechRateCommand(CommandContext context) {
        super(context);
    }

    @Override
    protected void recycleInternal() {
        rate = 0;
    }

    @Override
    public void run() {
        context.setSpeechRate(rate);
    }

    protected static Command obtain(@NonNull CommandContext context,
                                    float rate) {
        SpeechRateCommand command = new SpeechRateCommand(context);
        command.rate = rate;
        return command;
    }
}
