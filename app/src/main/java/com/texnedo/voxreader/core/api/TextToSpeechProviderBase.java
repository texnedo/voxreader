package com.texnedo.voxreader.core.api;

public abstract class TextToSpeechProviderBase implements TextToSpeechProvider {
    protected volatile StateListener listener;

    @Override
    public void setStateListener(StateListener listener) {
        this.listener = listener;
    }
}
