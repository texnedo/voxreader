package com.texnedo.voxreader.storage.cache;

import android.support.annotation.NonNull;

class Range implements Comparable<Range> {
    final long from;
    final long to;

    public static Range getRange(long from, long to) {
        return new Range(from, to);
    }

    private Range(long from, long to) {
        this.from = from;
        this.to = to;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Range range = (Range) o;

        if (from != range.from) return false;
        return to == range.to;

    }

    @Override
    public int hashCode() {
        int result = (int) (from ^ (from >>> 32));
        result = 31 * result + (int) (to ^ (to >>> 32));
        return result;
    }

    @Override
    public int compareTo(@NonNull Range another) {
        if (to < another.from) {
            return -1;
        } else if (from > another.to) {
            return 1;
        }
        return 0;
    }

    @Override
    public String toString() {
        return "Range{" +
                "from=" + from +
                ", to=" + to +
                '}';
    }
}
