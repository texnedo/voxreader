package com.texnedo.voxreader.storage;

import android.support.annotation.NonNull;

import com.texnedo.voxreader.core.timeline.TimeLineItem;

import java.util.List;

public interface ItemsReceiver {
    void onComplete(@NonNull List<TimeLineItem> items);

    void onFailure(@NonNull Exception ex);
}
