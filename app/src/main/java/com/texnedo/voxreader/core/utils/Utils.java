package com.texnedo.voxreader.core.utils;

import android.text.TextUtils;

public class Utils {
    public static boolean isDigits(String text, int start) {
        if (TextUtils.isEmpty(text)) {
            return false;
        }
        if (start > text.length() - 1) {
            return false;
        }
        for(int i = start; i < text.length(); ++i) {
            if (!Character.isDigit(text.charAt(i))) {
                return false;
            }
        }
        return true;
    }
}
