package com.texnedo.voxreader.data;

import android.support.annotation.NonNull;
import android.test.ActivityUnitTestCase;
import android.util.Log;

import com.texnedo.voxreader.Components;
import com.texnedo.voxreader.MainActivity_;
import com.texnedo.voxreader.core.api.DataSource;
import com.texnedo.voxreader.core.timeline.BaseTimeLineItem;
import com.texnedo.voxreader.system.Config;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class HtmlDataSourceTest extends ActivityUnitTestCase {
    private static final String LOG_TAG = "HtmlDataSourceTest";

    public HtmlDataSourceTest() {
        super(MainActivity_.class);
    }

//    public void testReadItems() throws Exception {
//        Config config = Components.createDefaultConfig(getInstrumentation().getContext());
//        HtmlDataSource source = new HtmlDataSource(config, "http://www.bbc.com/news/world-middle-east-35681250");
//        final CountDownLatch latch = new CountDownLatch(1);
//        final Result result = new Result();
//        source.readItems(new DataSource.Receiver() {
//            @Override
//            public void onComplete(@NonNull List<BaseTimeLineItem> items) {
//                result.items = items;
//                latch.countDown();
//            }
//
//            @Override
//            public void onItemCreated(@NonNull BaseTimeLineItem item) {
//                result.created++;
//            }
//
//            @Override
//            public void onFailure(DataSource.FailReason reason) {
//                result.reason = reason;
//            }
//        });
//        latch.await(5000, TimeUnit.MILLISECONDS);
//        assertEquals(result.reason, DataSource.FailReason.OK);
//        assertNotNull(result.items);
//        assertNotSame(result.items.size(), 0);
//        assertEquals(result.items.size(), result.created);
//        for(BaseTimeLineItem item : result.items) {
//            Log.v(LOG_TAG, String.format("%d - %s", item.getId(), item.toString()));
//        }
//
//        final Result result1 = new Result();
//        source.readItems(new DataSource.Receiver() {
//            @Override
//            public void onComplete(@NonNull List<BaseTimeLineItem> items) {
//                result1.items = items;
//            }
//
//            @Override
//            public void onItemCreated(@NonNull BaseTimeLineItem item) {
//                result1.created++;
//            }
//
//            @Override
//            public void onFailure(DataSource.FailReason reason) {
//                result1.reason = reason;
//            }
//        });
//        assertEquals(result.items.size(), result1.items.size());
//        assertEquals(result.created, result1.created);
//        assertEquals(result.reason, result1.reason);
//    }
//
//    public void testCreateCursor() throws Exception {
//        Config config = Components.createDefaultConfig(getInstrumentation().getContext());
//        HtmlDataSource source = new HtmlDataSource(config, "http://www.bbc.com/news/world-middle-east-35681250");
//        final DataSource.Cursor cursor = source.createCursor();
//        final Result result = new Result();
//        result.items = new ArrayList<>();
//        final CountDownLatch latch = new CountDownLatch(1);
//        final List<Integer> chunks = new LinkedList<>();
//        cursor.readItems(new DataSource.Receiver() {
//            @Override
//            public void onComplete(@NonNull List<BaseTimeLineItem> items) {
//                chunks.add(items.size());
//                result.items.addAll(items);
//                if (items.size() == 0) {
//                    latch.countDown();
//                } else {
//                    cursor.readItems(this, 20);
//                }
//            }
//
//            @Override
//            public void onItemCreated(@NonNull BaseTimeLineItem item) {
//                result.created++;
//            }
//
//            @Override
//            public void onFailure(DataSource.FailReason reason) {
//                result.reason = reason;
//            }
//        }, 20);
//        latch.await(5000, TimeUnit.MILLISECONDS);
//        assertEquals(result.reason, DataSource.FailReason.OK);
//        assertNotNull(result.items);
//        assertNotSame(result.items.size(), 0);
//        assertEquals(result.items.size(), result.created);
//        for (Integer chunk : chunks) {
//            if (chunk > 20) {
//                throw new IllegalStateException("A chunk must not be bigger than the provided value");
//            }
//        }
//        final Result result1 = new Result();
//        source.readItems(new DataSource.Receiver() {
//            @Override
//            public void onComplete(@NonNull List<BaseTimeLineItem> items) {
//                result1.items = items;
//            }
//
//            @Override
//            public void onItemCreated(@NonNull BaseTimeLineItem item) {
//                result1.created++;
//            }
//
//            @Override
//            public void onFailure(DataSource.FailReason reason) {
//                result1.reason = reason;
//            }
//        });
//        assertEquals(result.items.size(), result1.items.size());
//        assertEquals(result.created, result1.created);
//        assertEquals(result.reason, result1.reason);
//    }
//
//    private static class Result {
//        int created = 0;
//
//        List<BaseTimeLineItem> items;
//
//        DataSource.FailReason reason = DataSource.FailReason.OK;
//    }
}