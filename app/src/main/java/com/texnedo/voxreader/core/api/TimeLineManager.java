package com.texnedo.voxreader.core.api;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.texnedo.voxreader.system.Config;

import java.util.List;

public interface TimeLineManager {
    void add(@NonNull Uri uri);

    void add(@NonNull String text);

    void requestDataSources(@NonNull DataSourceRequestListener listener);

    void requestDataSource(@NonNull String id, @NonNull DataSourceRequestListener listener);

    void delete(@NonNull String id);

    void deletedAll();

    void play(@NonNull String id);

    void pause(@NonNull String id);

    void stop(@NonNull String id);

    void next(@NonNull String id);

    void playNext(@NonNull String currentId);

    void previous(@NonNull String id);

    void addListener(@NonNull DataSourceChangeListener listener);

    void removeListener(@NonNull DataSourceChangeListener listener);

    Context getContext();

    Config getConfig();

    interface DataSourceChangeListener {
        void onItemsChanged();

        void onStateChanged(@NonNull TimeLineAccess items);

        void onFailure();
    }

    interface DataSourceRequestListener {
        void onComplete(@NonNull List<TimeLineAccess> items);

        void onFailure();
    }
}
