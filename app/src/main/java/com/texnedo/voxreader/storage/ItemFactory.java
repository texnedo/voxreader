package com.texnedo.voxreader.storage;

import android.support.annotation.NonNull;

import com.texnedo.voxreader.core.timeline.TimeLineItem;

public interface ItemFactory {
    @NonNull
    TimeLineItem create(@NonNull TimeLineModel model);
}
