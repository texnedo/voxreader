package com.texnedo.voxreader.storage;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;

public class TimeLineModel {
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_TEXT = "text";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_META = "meta";
    public static final String COLUMN_DURATION = "duration";
    public static final String COLUMN_FLAGS = "flags";

    private long _id;
    private final int flags;
    private final String text;
    private final String meta;
    private final int type;
    private final int duration;

    TimeLineModel(@NonNull Cursor cursor) {
        //make id to be zero based index
        _id = cursor.getLong(cursor.getColumnIndex(COLUMN_ID)) - 1;
        type = cursor.getInt(cursor.getColumnIndex(COLUMN_TYPE));
        flags = cursor.getInt(cursor.getColumnIndex(COLUMN_FLAGS));
        duration = cursor.getInt(cursor.getColumnIndex(COLUMN_DURATION));
        meta = cursor.getString(cursor.getColumnIndex(COLUMN_META));
        text = cursor.getString(cursor.getColumnIndex(COLUMN_TEXT));
    }

    TimeLineModel(long id, int type, int flags, int duration, String text, String meta) {
        this._id = id;
        this.flags = flags;
        this.text = text;
        this.meta = meta;
        this.type = type;
        this.duration = duration;
    }

    ContentValues getData() {
        ContentValues values = new ContentValues();
        values.put(COLUMN_TEXT, text);
        values.put(COLUMN_TYPE, type);
        values.put(COLUMN_FLAGS, flags);
        values.put(COLUMN_DURATION, duration);
        values.put(COLUMN_META, meta);
        return values;
    }

    void setId(long value) {
        _id = value;
    }

    public long getId() {
        return _id;
    }

    public int getFlags() {
        return flags;
    }

    public String getText() {
        return text;
    }

    public int getType() {
        return type;
    }

    public int getDuration() {
        return duration;
    }

    public String getMeta() {
        return meta;
    }

    @Override
    public String toString() {
        return "TimeLineModel{" +
                "_id=" + _id +
                ", flags=" + flags +
                ", text='" + text + '\'' +
                ", meta='" + meta + '\'' +
                ", type=" + type +
                ", duration=" + duration +
                '}';
    }
}

