package com.texnedo.voxreader.storage;

public interface TimeLineModelFactory {
    TimeLineModel createModel(int type, int flags, int duration, String text, String meta);
}
