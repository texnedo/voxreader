package com.texnedo.voxreader.storage;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.annotation.NonNull;

import com.texnedo.voxreader.storage.dao.DaoMaster;
import com.texnedo.voxreader.storage.dao.DaoSession;
import com.texnedo.voxreader.storage.dao.DataSourceModel;
import com.texnedo.voxreader.storage.dao.DataSourceModelDao;
import com.texnedo.voxreader.system.Config;
import com.texnedo.voxreader.utils.FileLog;

public class DatabaseManagerImpl implements DatabaseManager {
    private static final String LOG_TAG = "DatabaseManager";
    private static final String DB_NAME = "vox-data";
    private final Config config;
    private final ItemsChangeListener listener;
    private final DaoMaster.OpenHelper dbHelper;
    private SQLiteDatabase database;
    private DaoMaster master;
    private DaoSession session;

    public DatabaseManagerImpl(@NonNull Config config,
                               @NonNull ItemsChangeListener listener) {
        this.config = config;
        this.listener = listener;
        this.dbHelper = new DaoMaster.OpenHelper(config.getContext(), DB_NAME, null) {
            @Override
            public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            }
        };
    }

    private void closeDbConnection() {
        if (database != null) {
            session.clear();
            if (database.isOpen()) {
                database.close();
            }
            database = null;
            session = null;
        }
    }

    private void openReadableDb() throws SQLiteException {
        if (database == null) {
            database = dbHelper.getReadableDatabase();
            master = new DaoMaster(database);
            session = master.newSession();
        }
    }

    private void openWritableDb() throws SQLiteException {
        if (database != null && database.isReadOnly()) {
            closeDbConnection();
        }
        if (database == null) {
            database = dbHelper.getWritableDatabase();
            master = new DaoMaster(database);
            session = master.newSession();
        }
    }

    @Override
    public void close() {
        config.getNetworkExecutor().submit(new Runnable() {
            @Override
            public void run() {
                try {
                    closeDbConnection();
                } catch (SQLiteException ex) {
                    listener.onFailure();
                    FileLog.e(LOG_TAG, ex, "failed to close db");
                }
            }
        });
    }

    @Override
    public void insert(@NonNull final DataSourceModel dataSource) {
        config.getStorageExecutor().submit(new Runnable() {
            @Override
            public void run() {
                try {
                    openWritableDb();
                    session.getDataSourceModelDao().insertOrReplace(dataSource);
                    listener.onInserted(dataSource);
                    FileLog.d(LOG_TAG, "item %s saved", dataSource.getContent());
                } catch (SQLiteException ex) {
                    listener.onFailure();
                    FileLog.e(LOG_TAG, ex, "failed to delete item %s", dataSource.getContent());
                }
            }
        });
    }

    @Override
    public void delete(@NonNull final DataSourceModel dataSource) {
        config.getStorageExecutor().submit(new Runnable() {
            @Override
            public void run() {
                try {
                    openWritableDb();
                    session.getDataSourceModelDao().delete(dataSource);
                    listener.onDeleted(dataSource);
                    FileLog.d(LOG_TAG, "item %s deleted", dataSource.getContent());
                } catch (SQLiteException ex) {
                    listener.onFailure();
                    FileLog.e(LOG_TAG, ex, "failed to delete item %s", dataSource.getContent());
                }
            }
        });
    }

    @Override
    public void update(@NonNull final DataSourceModel dataSource) {
        config.getStorageExecutor().submit(new Runnable() {
            @Override
            public void run() {
                try {
                    openWritableDb();
                    session.getDataSourceModelDao().update(dataSource);
                    listener.onUpdated(dataSource);
                    FileLog.d(LOG_TAG, "item %s updated", dataSource.getContent());
                } catch (SQLiteException ex) {
                    listener.onFailure();
                    FileLog.e(LOG_TAG, ex, "failed to update item %s", dataSource.getContent());
                }
            }
        });
    }

    @Override
    public void deleteAll() {
        config.getStorageExecutor().submit(new Runnable() {
            @Override
            public void run() {
                try {
                    openReadableDb();
                    session.getDataSourceModelDao().deleteAll();
                    listener.onCleared();
                } catch (SQLiteException ex) {
                    listener.onFailure();
                    FileLog.e(LOG_TAG, "failed to delete all", ex);
                }
            }
        });
    }

    @Override
    public void requestAll(@NonNull final LoadItemsListener listener) {
        config.getStorageExecutor().submit(new Runnable() {
            @Override
            public void run() {
                try {
                    openReadableDb();
                    listener.onComplete(
                            session.getDataSourceModelDao().queryBuilder().list()
                    );
                } catch (SQLiteException ex) {
                    listener.onFailure();
                    FileLog.e(LOG_TAG, "failed to request items", ex);
                }
            }
        });
    }

    @Override
    public void requestOne(@NonNull final String id, @NonNull final LoadItemsListener listener) {
        config.getStorageExecutor().submit(new Runnable() {
            @Override
            public void run() {
                try {
                    openReadableDb();
                    listener.onComplete(
                            session.getDataSourceModelDao()
                                    .queryBuilder()
                                    .where(DataSourceModelDao.Properties.Id.eq(Long.parseLong(id)))
                                    .list()
                    );
                } catch (SQLiteException ex) {
                    listener.onFailure();
                    FileLog.e(LOG_TAG, "failed to request items", ex);
                }
            }
        });
    }

}
