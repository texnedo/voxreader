package com.texnedo.voxreader.core.api;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.texnedo.voxreader.core.timeline.BaseTimeLineItem;

import org.jsoup.nodes.Node;

public interface TimeLineBuilder {
    TimeLineBuilder appendChar(char c);

    TimeLineBuilder appendText(@NonNull String text);

    TimeLineBuilder appendText(@NonNull String text, TextImportance importance);

    TimeLineBuilder appendParagraph();

    TimeLineBuilder appendHeader();

    TimeLineBuilder appendImage(@NonNull String url, @Nullable String description);

    TimeLineBuilder appendLink(@NonNull String url, @Nullable String description);

    TimeLineBuilder appendNode(@NonNull Node node);

    void complete();

    enum TextImportance {
        NORMAL,
        TITLE,
        H1,
        H2,
        H3
    }

    interface Receiver {
        void onComplete(@NonNull BaseTimeLineItem item);

        void onCompleted();
    }
}
