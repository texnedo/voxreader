package com.texnedo.voxreader.ui;

import android.support.annotation.NonNull;

interface TimeLineGroupListener {
    void onActivated(@NonNull TimeLineGroupViewHolder group, int offset);
}
