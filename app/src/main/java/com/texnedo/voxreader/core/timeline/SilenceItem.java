package com.texnedo.voxreader.core.timeline;

import android.support.annotation.NonNull;

import com.texnedo.voxreader.core.api.TextToSpeechProvider;
import com.texnedo.voxreader.storage.TimeLineModel;

public class SilenceItem extends BaseTimeLineItem {
    protected SilenceItem(@NonNull TimeLineModel model) {
        super(model);
    }

    @Override
    public String getText() {
        return null;
    }

    private int getDuration() {
        return model.getDuration();
    }

    @Override
    protected void playInternal(@NonNull TextToSpeechProvider provider) {
        provider.playSilentUtterance(getId(), getDuration());
    }

    @Override
    public String toString() {
        return "silence duration " + model.getDuration();
    }
}
