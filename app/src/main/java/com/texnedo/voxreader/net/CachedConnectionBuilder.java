package com.texnedo.voxreader.net;

import android.support.annotation.NonNull;

import java.io.File;

public interface CachedConnectionBuilder extends ConnectionBuilder {
    CachedConnectionBuilder setMaxCacheSize(long maxCacheSize);

    CachedConnectionBuilder setCacheFolder(@NonNull File cacheFolder);

    CachedConnectionBuilder setKeepCacheItemTimeout(long timeout);
}
