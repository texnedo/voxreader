#ifndef LANGUAGEDETECTOR_H
#define LANGUAGEDETECTOR_H

#include "languagedetector_global.h"
#include <jni.h>

class LanguageDetector
{
public:
    LanguageDetector();
    const char* detect(const char* text);
};

extern "C" {
    JNIEXPORT jstring JNICALL Java_com_texnedo_voxreader_core_api_LanguageDetector2Impl_detect(JNIEnv* env, jobject thiz, jstring text);
}
#endif // LANGUAGEDETECTOR_H
