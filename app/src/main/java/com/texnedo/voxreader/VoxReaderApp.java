package com.texnedo.voxreader;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import com.crashlytics.android.Crashlytics;
import com.texnedo.voxreader.core.api.TimeLineManager;
import com.texnedo.voxreader.core.api.TimeLineManagerImpl;
import com.texnedo.voxreader.system.ConfigImpl;
import com.texnedo.voxreader.ui.TimeLineAdapter;
import com.texnedo.voxreader.utils.FileLog;

import io.fabric.sdk.android.Fabric;

public class VoxReaderApp extends Application {
    private final static String LOG_TAG = "VoxReaderApp";
    private static TimeLineManager manager;

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            FileLog.init(this, null);
        }
        Fabric.with(this, new Crashlytics());
        manager = new TimeLineManagerImpl(new ConfigImpl(this));
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        FileLog.d(LOG_TAG, "trim memory level %d", level);
        if (level == TRIM_MEMORY_UI_HIDDEN) {
            TimeLineAdapter.onTrimMemory();
        }
    }

    public static TimeLineManager getManager() {
        return manager;
    }
}
