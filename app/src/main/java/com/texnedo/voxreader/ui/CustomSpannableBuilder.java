package com.texnedo.voxreader.ui;

import android.support.annotation.NonNull;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ScaleXSpan;
import android.util.LruCache;

import com.texnedo.voxreader.utils.FileLog;

class CustomSpannableBuilder implements SpannableBuilder {
    private static final String LOG_TAG = "SpannableBuilder";
    private static final int SPACE_WIDTHS_CACHE_SIZE = 10;
    private static final LruCache<TextPaint, Float> spaceWidths =
            new LruCache<>(SPACE_WIDTHS_CACHE_SIZE);
    private static final String SPACE_MARK = " ";
    private static final char SPACE_MARK_CHAR = ' ';
    private static final float SPACE_WIDTH_PROPORTION_EPSILON = 0.01f;
    private static float[] textWidths;
    private static int[] spacePositions;
    private final SpannableStringBuilder builder = new SpannableStringBuilder();
    private boolean built = false;

    public CustomSpannableBuilder() {
    }

    @Override
    public SpannableBuilder append(CharSequence text) {
        builder.append(text);
        return this;
    }

    @Override
    public SpannableBuilder disableTextJustify() {
        built = true;
        return this;
    }

    @Override
    public Spannable build(@NonNull TextPaint textPaint, int screenWidth) {
        //TODO - research why not all lines are perfectly justified
        if (!built) {
            try {
                //TODO - optimize to do this action during append method call
                float spaceWidth = getSpaceWidth(textPaint);
                float[] widths = getTextWidths(builder.length());
                //measure whole text size
                if (textPaint.getTextWidths(builder, 0, builder.length(), widths) != builder.length()) {
                    throw new BuildException();
                }
                int maxSpaceCount = (int) (screenWidth / spaceWidth);
                int[] spaces = getSpacePositions(maxSpaceCount);
                float lineSum = 0;
                float lastSpaceSum = 0;
                int lineSpacesCount = 0;
                int spaceListEndIndex = 0;

                for (int i = 0; i < builder.length(); i++) {
                    lineSum += widths[i];
                    if (builder.charAt(i) == SPACE_MARK_CHAR) {
                        lineSpacesCount++;
                        lastSpaceSum = lineSum;
                        spaces[spaceListEndIndex++] = i;
                    }
                    if (lineSum >= screenWidth) {
                        lineSpacesCount--;
                        lastSpaceSum -= spaceWidth;
                        float diff = screenWidth - lastSpaceSum;
                        int spaceCount = (int) (diff / spaceWidth);
                        float proportion = 1 + (float) spaceCount / (float) lineSpacesCount;
                        if (proportion - 1 >= SPACE_WIDTH_PROPORTION_EPSILON) {
                            for (int j = 0; j < spaceListEndIndex; j++) {
                                int spaceIndex = spaces[j];
                                builder.setSpan(new ScaleXSpan(proportion), spaceIndex, spaceIndex + 1, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                            /*builder.setSpan(new BackgroundColorSpan(0xffff0000), spaceIndex, spaceIndex + 1, Spanned.SPAN_INCLUSIVE_INCLUSIVE);*/
                            }
                        }
                        lineSpacesCount = 0;
                        spaceListEndIndex = 0;
                        lineSum = lineSum - lastSpaceSum - spaceWidth;
                    }
                }
            } catch (Exception ex) {
                FileLog.e(LOG_TAG, "failed to build justified spannable object", ex);
            }
            built = true;
        }
        return builder;
    }

    @Override
    public void setSpan(Object what, int start, int end, int flags) {
        builder.setSpan(what, start, end, flags);
    }

    @Override
    public void removeSpan(Object what) {
        builder.removeSpan(what);
    }

    @Override
    public int length() {
        return builder.length();
    }

    private float getSpaceWidth(@NonNull TextPaint textPaint) throws BuildException {
        Float width = spaceWidths.get(textPaint);
        if (width != null) {
            return width;
        }
        float widths[] = new float[1];
        if (textPaint.getTextWidths(SPACE_MARK, widths) != 1) {
            throw new BuildException();
        }
        spaceWidths.put(textPaint, widths[0]);
        return widths[0];
    }

    private float[] getTextWidths(int length) {
        if (textWidths == null) {
            textWidths = new float[length];
        } else if (textWidths.length < length) {
            textWidths = new float[length * 2];
        }
        return textWidths;
    }

    private int[] getSpacePositions(int length) {
        if (spacePositions == null) {
            spacePositions = new int[length];
        } else if (textWidths.length < length) {
            spacePositions = new int[length * 2];
        }
        return spacePositions;
    }

    private static float measureStringWidth(@NonNull TextPaint textPaint, @NonNull CharSequence text) {
        float[] width = new float[text.length()];
        textPaint.getTextWidths(text, 0, text.length(), width);
        int sum = 0;
        for(int i = 0; i < text.length(); i++) {
            sum += width[i];
        }
        return sum;
    }

    private static void release() {
        textWidths = null;
        spaceWidths.evictAll();
    }

    private static final class BuildException extends Exception {

    }
}
