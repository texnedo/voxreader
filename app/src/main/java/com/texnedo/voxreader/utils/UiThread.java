package com.texnedo.voxreader.utils;

import android.os.Handler;
import android.os.Looper;

import static com.texnedo.voxreader.utils.DebugUtils.safeThrow;

public class UiThread {
    private static Handler uiHandler = new Handler(Looper.getMainLooper());

    public static abstract class UiRunnable implements Runnable {
        @Override
        public final void run() {
            if (currentThreadIsUi())
                runUi();
            else
                UiThread.run(new Runnable() {
                    @Override
                    public void run() {
                        runUi();
                    }
                });
        }

        protected abstract void runUi();
    }

    public static void checkUi() {
        if (!currentThreadIsUi()) {
            safeThrow(new IllegalStateException("Method should be called from UI thread."));
        }
    }

    public static void checkNotUi() {
        if (currentThreadIsUi()) {
            safeThrow(new IllegalStateException("Method should not be called from UI thread."));
        }
    }

    public static boolean currentThreadIsUi() {
        return uiHandler.getLooper().getThread() == Thread.currentThread();
    }

    public static void run(Runnable task) {
        if (currentThreadIsUi()) {
            task.run();
        } else {
            uiHandler.post(task);
        }
    }

    public static void runLater(Runnable task) {
        runLater(task, 0);
    }

    public static void runLater(Runnable task, long delay) {
        uiHandler.postDelayed(task, delay);
    }

    public static void runAtTime(Runnable task, long delay) {
        uiHandler.postAtTime(task, delay);
    }

    public static void cancelDelayedTask(Runnable task) {
        uiHandler.removeCallbacks(task);
    }
}
