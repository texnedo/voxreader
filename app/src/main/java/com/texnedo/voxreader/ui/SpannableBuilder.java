package com.texnedo.voxreader.ui;

import android.support.annotation.NonNull;
import android.text.Spannable;
import android.text.TextPaint;

interface SpannableBuilder {
    SpannableBuilder append(CharSequence text);

    SpannableBuilder disableTextJustify();

    Spannable build(@NonNull TextPaint textPaint, int screenWidth);

    void setSpan(Object what, int start, int end, int flags);

    void removeSpan(Object what);

    int length();
}
