package com.texnedo.voxreader;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.texnedo.voxreader.core.api.DataSource;
import com.texnedo.voxreader.core.api.TimeLineFactory;
import com.texnedo.voxreader.ui.DataSourceAdapter;
import com.texnedo.voxreader.utils.FileLog;
import com.texnedo.voxreader.utils.UiUtils;
import com.texnedo.voxreader.utils.Utils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_data_source)
@OptionsMenu(R.menu.menu_main)
public class DataSourceActivity extends AppCompatActivity {
    private static final String LOG_TAG = "DataSourceActivity";
    private static final String ENTER_URI_DIALOG = "enter_uri_dialog";
    public static final String TEXT_PLAIN_TYPE = "text/plain";
    public static final String ANY_TYPE = "*/*";
    private DataSourceAdapter dataSourceAdapter;

    @ViewById(R.id.dataSourcesList)
    ListView dataSourcesList;

    @ViewById(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @ViewById(R.id.left_drawer)
    NavigationView leftDrawer;

    @AfterViews
    void setupControls() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                toolbar,
                R.string.app_name,
                R.string.app_name
        );
        drawerLayout.setDrawerListener(drawerToggle);
        leftDrawer.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                if (item.getItemId() == R.id.action_settings) {
                    onSettings();
                }
                drawerLayout.closeDrawer(leftDrawer);
                return true;
            }
        });
        drawerToggle.syncState();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EnterUriDialogFragment dialog = new EnterUriDialogFragment();
                dialog.show(getSupportFragmentManager(), ENTER_URI_DIALOG);
            }
        });
        if (dataSourceAdapter == null) {
            dataSourceAdapter = new DataSourceAdapter(VoxReaderApp.getManager());
        }
        dataSourcesList.setAdapter(dataSourceAdapter);
        dataSourcesList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                //TODO - show confirm dialog and menu
                DataSource dataSource = (DataSource)parent.getAdapter().getItem(position);
                VoxReaderApp.getManager().delete(dataSource.getId());
                return true;
            }
        });
        dataSourcesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DataSource dataSource = (DataSource)parent.getAdapter().getItem(position);
                Navigate.goToTimeLine(DataSourceActivity.this, dataSource.getId());
            }
        });
    }

    @OptionsItem(R.id.action_settings)
    void onSettings() {
        Navigate.goToSettings(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        processIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        processIntent(intent);
    }

    private void processIntent(Intent intent) {
        if (intent == null) {
            return;
        }
        String action = intent.getAction();
        String type = intent.getType();
        FileLog.d(
                LOG_TAG,
                "process intent:\n action %s\n type %s\n extras %s \n data %s",
                action,
                type,
                Utils.bundleToString(intent.getExtras()),
                intent.getData()
        );
        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if (type.startsWith(TEXT_PLAIN_TYPE)) {
                handleSendText(intent); // Handle text being sent
            } else if (ANY_TYPE.equals(type)) {
                handleSendUri(intent);
            }
        } else if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null) {
            if (type.startsWith(TEXT_PLAIN_TYPE)) {
                handleSendMultipleTexts(intent); // Handle multiple images being sent
            }
        } else if (Intent.ACTION_VIEW.equals(action) && type != null) {
            if (type.startsWith(TEXT_PLAIN_TYPE)) {
                handleOpenTextFile(intent); // Handle text being sent
            }
        }
        intent.setAction(null);
    }

    @Override
    protected void onPause() {
        super.onPause();
        dataSourceAdapter.unSubscribe();
    }

    @Override
    protected void onResume() {
        super.onResume();
        dataSourceAdapter.subscribe();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void handleSendMultipleTexts(@NonNull Intent intent) {
        FileLog.v(LOG_TAG, "handle multiple texts %s", Utils.bundleToString(intent.getExtras()));
        //TODO - implement me
    }

    private void handleSendText(@NonNull Intent intent) {
        FileLog.v(LOG_TAG, "handle texts %s", Utils.bundleToString(intent.getExtras()));
        //TODO - in case of chrome, we can extract page preview using param 'share_screenshot_as_stream'
        String text = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (TextUtils.isEmpty(text)) {
            return;
        }
        VoxReaderApp.getManager().add(text);
        intent.removeExtra(Intent.EXTRA_TEXT);
    }

    private void handleSendUri(@NonNull Intent intent) {
        FileLog.v(LOG_TAG, "handle url %s", Utils.bundleToString(intent.getExtras()));
        Bundle bundle = intent.getExtras();
        if (bundle == null) {
            return;
        }
        Object uriObject = bundle.get(Intent.EXTRA_STREAM);
        if (uriObject == null || !(uriObject instanceof Uri)) {
            return;
        }
        Uri resourceUri = (Uri)uriObject;
        TimeLineFactory.getInstance(this).add(resourceUri);
        intent.removeExtra(Intent.EXTRA_STREAM);
    }

    private void handleOpenTextFile(@NonNull Intent intent) {
        FileLog.v(LOG_TAG, "handle text file %s", intent.getData());
        Uri uri = intent.getData();
        if (uri == null) {
            return;
        }
        TimeLineFactory.getInstance(this).add(uri);
        intent.setData(null);
    }

    public static class EnterUriDialogFragment extends DialogFragment {
        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();
            final View dialogView = inflater.inflate(R.layout.enter_data_source_dialog, null);
            final EditText editText = (EditText) dialogView.findViewById(R.id.data_source_uri);
            editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        UiUtils.showKeyboard(getActivity(), v);
                    }
                }
            });
            builder.setView(dialogView)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            String uri = editText.getText().toString();
                            FileLog.v(LOG_TAG, "selected resource %s", uri);
                            Toast.makeText(getActivity(), uri, Toast.LENGTH_SHORT).show();
                            Uri resourceUri = Uri.parse(uri);
                            VoxReaderApp.getManager().add(resourceUri);
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });
            return builder.create();
        }
    }
}
