package com.texnedo.voxreader.core.api;

import android.support.annotation.NonNull;
import android.test.ActivityTestCase;
import android.text.TextUtils;
import android.util.Log;

import com.texnedo.voxreader.R;
import com.texnedo.voxreader.core.timeline.BaseTimeLineItem;
import com.texnedo.voxreader.core.timeline.SilenceItem;
import com.texnedo.voxreader.core.timeline.TextItem;
import com.texnedo.voxreader.core.timeline.TimeLineItem;
import com.texnedo.voxreader.sound.TextToSpeechProviderImpl;
import com.texnedo.voxreader.storage.TimeLineStorageImpl;
import com.texnedo.voxreader.system.Config;
import com.texnedo.voxreader.system.ConfigImpl;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Thread.sleep;

public class TimeLineBuilderImplTest extends ActivityTestCase {
    public static final String SMALL_TEXT = "Well, Prince, so Genoa and Lucca are now just family estates of the Buonapartes.\n" +
            " But I warn you, if you don't tell me that this means war, if you still try to defend\n" +
            " the infamies and horrors perpetrated by that Antichrist- I really believe he is Antichrist\n" +
            "- I will have nothing more to do with you and you are no longer my friend, no longer my 'faithful\n" +
            " slave,' as you call yourself! But how do you do?";

    public static final String BIG_TEXT = "\"Well, Prince, so Genoa and Lucca are now just family estates of the\n" +
            "Buonapartes. But I warn you, if you don't tell me that this means war,\n" +
            "if you still try to defend the infamies and horrors perpetrated by\n" +
            "that Antichrist- I really believe he is Antichrist- I will have\n" +
            "nothing more to do with you and you are no longer my friend, no longer\n" +
            "my 'faithful slave,' as you call yourself! But how do you do? I see\n" +
            "I have frightened you- sit down and tell me all the news.\"\n" +
            "  It was in July, 1805, and the speaker was the well-known Anna\n" +
            "Pavlovna Scherer, maid of honor and favorite of the Empress Marya\n" +
            "Fedorovna. With these words she greeted Prince Vasili Kuragin, a man\n" +
            "of high rank and importance, who was the first to arrive at her\n" +
            "reception. Anna Pavlovna had had a cough for some days. She was, as\n" +
            "she said, suffering from la grippe; grippe being then a new word in\n" +
            "St. Petersburg, used only by the elite.\n" +
            "  All her invitations without exception, written in French, and\n" +
            "delivered by a scarlet-liveried footman that morning, ran as follows:\n" +
            "  \"If you have nothing better to do, Count [or Prince], and if the\n" +
            "prospect of spending an evening with a poor invalid is not too\n" +
            "terrible, I shall be very charmed to see you tonight between 7 and 10-\n" +
            "Annette Scherer.\"\n" +
            "  \"Heavens! what a virulent attack!\" replied the prince, not in the\n" +
            "least disconcerted by this reception. He had just entered, wearing\n" +
            "an embroidered court uniform, knee breeches, and shoes, and had\n" +
            "stars on his breast and a serene expression on his flat face. He spoke\n" +
            "in that refined French in which our grandfathers not only spoke but\n" +
            "thought, and with the gentle, patronizing intonation natural to a\n" +
            "man of importance who had grown old in society and at court. He went\n" +
            "up to Anna Pavlovna, kissed her hand, presenting to her his bald,\n" +
            "scented, and shining head, and complacently seated himself on the\n" +
            "sofa.\n" +
            "  \"First of all, dear friend, tell me how you are. Set your friend's\n" +
            "mind at rest,\" said he without altering his tone, beneath the\n" +
            "politeness and affected sympathy of which indifference and even\n" +
            "irony could be discerned.\n" +
            "  \"Can one be well while suffering morally? Can one be calm in times\n" +
            "like these if one has any feeling?\" said Anna Pavlovna. \"You are\n" +
            "staying the whole evening, I hope?\"\n" +
            "  \"And the fete at the English ambassador's? Today is Wednesday. I\n" +
            "must put in an appearance there,\" said the prince. \"My daughter is\n" +
            "coming for me to take me there.\"\n" +
            "  \"I thought today's fete had been canceled. I confess all these\n" +
            "festivities and fireworks are becoming wearisome.\"\n" +
            "  \"If they had known that you wished it, the entertainment would\n" +
            "have been put off,\" said the prince, who, like a wound-up clock, by\n" +
            "force of habit said things he did not even wish to be believed.\n" +
            "  \"Don't tease! Well, and what has been decided about Novosiltsev's\n" +
            "dispatch? You know everything.\"\n" +
            "  \"What can one say about it?\" replied the prince in a cold,\n" +
            "listless tone. \"What has been decided? They have decided that\n" +
            "Buonaparte has burnt his boats, and I believe that we are ready to\n" +
            "burn ours.\"\n" +
            "  Prince Vasili always spoke languidly, like an actor repeating a\n" +
            "stale part. Anna Pavlovna Scherer on the contrary, despite her forty\n" +
            "years, overflowed with animation and impulsiveness. To be an\n" +
            "enthusiast had become her social vocation and, sometimes even when she\n" +
            "did not feel like it, she became enthusiastic in order not to\n" +
            "disappoint the expectations of those who knew her. The subdued smile\n" +
            "which, though it did not suit her faded features, always played\n" +
            "round her lips expressed, as in a spoiled child, a continual\n" +
            "consciousness of her charming defect, which she neither wished, nor\n" +
            "could, nor considered it necessary, to correct.\n" +
            "  In the midst of a conversation on political matters Anna Pavlovna\n" +
            "burst out:\n" +
            "  \"Oh, don't speak to me of Austria. Perhaps I don't understand\n" +
            "things, but Austria never has wished, and does not wish, for war.\n" +
            "She is betraying us! Russia alone must save Europe. Our gracious\n" +
            "sovereign recognizes his high vocation and will be true to it. That is\n" +
            "the one thing I have faith in! Our good and wonderful sovereign has to\n" +
            "perform the noblest role on earth, and he is so virtuous and noble\n" +
            "that God will not forsake him. He will fulfill his vocation and\n" +
            "crush the hydra of revolution, which has become more terrible than\n" +
            "ever in the person of this murderer and villain! We alone must\n" +
            "avenge the blood of the just one.... Whom, I ask you, can we rely\n" +
            "on?... England with her commercial spirit will not and cannot\n" +
            "understand the Emperor Alexander's loftiness of soul. She has\n" +
            "refused to evacuate Malta. She wanted to find, and still seeks, some\n" +
            "secret motive in our actions. What answer did Novosiltsev get? None.\n" +
            "The English have not understood and cannot understand the\n" +
            "self-abnegation of our Emperor who wants nothing for himself, but only\n" +
            "desires the good of mankind. And what have they promised? Nothing! And\n" +
            "what little they have promised they will not perform! Prussia has\n" +
            "always declared that Buonaparte is invincible, and that all Europe\n" +
            "is powerless before him.... And I don't believe a word that Hardenburg\n" +
            "says, or Haugwitz either. This famous Prussian neutrality is just a\n" +
            "trap. I have faith only in God and the lofty destiny of our adored\n" +
            "monarch. He will save Europe!\"\n" +
            "  She suddenly paused, smiling at her own impetuosity.\n" +
            "  \"I think,\" said the prince with a smile, \"that if you had been\n" +
            "sent instead of our dear Wintzingerode you would have captured the\n" +
            "King of Prussia's consent by assault. You are so eloquent. Will you\n" +
            "give me a cup of tea?\"\n" +
            "  \"In a moment. A propos,\" she added, becoming calm again, \"I am\n" +
            "expecting two very interesting men tonight, le Vicomte de Mortemart,\n" +
            "who is connected with the Montmorencys through the Rohans, one of\n" +
            "the best French families. He is one of the genuine emigres, the good\n" +
            "ones. And also the Abbe Morio. Do you know that profound thinker? He\n" +
            "has been received by the Emperor. Had you heard?\"\n" +
            "  \"I shall be delighted to meet them,\" said the prince. \"But tell me,\"\n" +
            "he added with studied carelessness as if it had only just occurred\n" +
            "to him, though the question he was about to ask was the chief motive\n" +
            "of his visit, \"is it true that the Dowager Empress wants Baron Funke\n" +
            "to be appointed first secretary at Vienna? The baron by all accounts\n" +
            "is a poor creature.\"\n" +
            "  Prince Vasili wished to obtain this post for his son, but others\n" +
            "were trying through the Dowager Empress Marya Fedorovna to secure it\n" +
            "for the baron.\n" +
            "  Anna Pavlovna almost closed her eyes to indicate that neither she\n" +
            "nor anyone else had a right to criticize what the Empress desired or\n" +
            "was pleased with.\n" +
            "  \"Baron Funke has been recommended to the Dowager Empress by her\n" +
            "sister,\" was all she said, in a dry and mournful tone.\n" +
            "  As she named the Empress, Anna Pavlovna's face suddenly assumed an\n" +
            "expression of profound and sincere devotion and respect mingled with\n" +
            "sadness, and this occurred every time she mentioned her illustrious\n" +
            "patroness. She added that Her Majesty had deigned to show Baron\n" +
            "Funke beaucoup d'estime, and again her face clouded over with sadness.\n" +
            "  The prince was silent and looked indifferent. But, with the\n" +
            "womanly and courtierlike quickness and tact habitual to her, Anna\n" +
            "Pavlovna wished both to rebuke him (for daring to speak he had done of\n" +
            "a man recommended to the Empress) and at the same time to console him,\n" +
            "so she said:\n" +
            "  \"Now about your family. Do you know that since your daughter came\n" +
            "out everyone has been enraptured by her? They say she is amazingly\n" +
            "beautiful.\"\n" +
            "  The prince bowed to signify his respect and gratitude.\n" +
            "  \"I often think,\" she continued after a short pause, drawing nearer\n" +
            "to the prince and smiling amiably at him as if to show that\n" +
            "political and social topics were ended and the time had come for\n" +
            "intimate conversation- \"I often think how unfairly sometimes the\n" +
            "joys of life are distributed. Why has fate given you two such splendid\n" +
            "children? I don't speak of Anatole, your youngest. I don't like\n" +
            "him,\" she added in a tone admitting of no rejoinder and raising her\n" +
            "eyebrows. \"Two such charming children. And really you appreciate\n" +
            "them less than anyone, and so you don't deserve to have them.\"\n" +
            "  And she smiled her ecstatic smile.\n" +
            "  \"I can't help it,\" said the prince. \"Lavater would have said I\n" +
            "lack the bump of paternity.\"\n" +
            "  \"Don't joke; I mean to have a serious talk with you. Do you know I\n" +
            "am dissatisfied with your younger son? Between ourselves\" (and her\n" +
            "face assumed its melancholy expression), \"he was mentioned at Her\n" +
            "Majesty's and you were pitied....\"\n" +
            "  The prince answered nothing, but she looked at him significantly,\n" +
            "awaiting a reply. He frowned.\n" +
            "  \"What would you have me do?\" he said at last. \"You know I did all\n" +
            "a father could for their education, and they have both turned out\n" +
            "fools. Hippolyte is at least a quiet fool, but Anatole is an active\n" +
            "one. That is the only difference between them.\" He said this smiling\n" +
            "in a way more natural and animated than usual, so that the wrinkles\n" +
            "round his mouth very clearly revealed something unexpectedly coarse\n" +
            "and unpleasant.\n" +
            "  \"And why are children born to such men as you? If you were not a\n" +
            "father there would be nothing I could reproach you with,\" said Anna\n" +
            "Pavlovna, looking up pensively.\n" +
            "  \"I am your faithful slave and to you alone I can confess that my\n" +
            "children are the bane of my life. It is the cross I have to bear. That\n" +
            "is how I explain it to myself. It can't be helped!\"\n" +
            "  He said no more, but expressed his resignation to cruel fate by a\n" +
            "gesture. Anna Pavlovna meditated.\n" +
            "  \"Have you never thought of marrying your prodigal son Anatole?\"\n" +
            "she asked. \"They say old maids have a mania for matchmaking, and\n" +
            "though I don't feel that weakness in myself as yet,I know a little\n" +
            "person who is very unhappy with her father. She is a relation of\n" +
            "yours, Princess Mary Bolkonskaya.\"\n" +
            "  Prince Vasili did not reply, though, with the quickness of memory\n" +
            "and perception befitting a man of the world, he indicated by a\n" +
            "movement of the head that he was considering this information.\n" +
            "  \"Do you know,\" he said at last, evidently unable to check the sad\n" +
            "current of his thoughts, \"that Anatole is costing me forty thousand\n" +
            "rubles a year? And,\" he went on after a pause, \"what will it be in\n" +
            "five years, if he goes on like this?\" Presently he added: \"That's what\n" +
            "we fathers have to put up with.... Is this princess of yours rich?\"\n" +
            "  \"Her father is very rich and stingy. He lives in the country. He\n" +
            "is the well-known Prince Bolkonski who had to retire from the army\n" +
            "under the late Emperor, and was nicknamed 'the King of Prussia.' He is\n" +
            "very clever but eccentric, and a bore. The poor girl is very\n" +
            "unhappy. She has a brother; I think you know him, he married Lise\n" +
            "Meinen lately. He is an aide-de-camp of Kutuzov's and will be here\n" +
            "tonight.\"\n" +
            "  \"Listen, dear Annette,\" said the prince, suddenly taking Anna\n" +
            "Pavlovna's hand and for some reason drawing it downwards. \"Arrange\n" +
            "that affair for me and I shall always be your most devoted slave-\n" +
            "slafe wigh an f, as a village elder of mine writes in his reports. She\n" +
            "is rich and of good family and that's all I want.\"\n" +
            "  And with the familiarity and easy grace peculiar to him, he raised\n" +
            "the maid of honor's hand to his lips, kissed it, and swung it to and\n" +
            "fro as he lay back in his armchair, looking in another direction.\n" +
            "  \"Attendez,\" said Anna Pavlovna, reflecting, \"I'll speak to Lise,\n" +
            "young Bolkonski's wife, this very evening, and perhaps the thing can\n" +
            "be arranged. It shall be on your family's behalf that I'll start my\n" +
            "apprenticeship as old maid.\"\n" +
            "BK1|CH2";

    public void testBuildItems() throws Exception {
        sleep(300);
        final ArrayList<TimeLineItem> items = new ArrayList<>();
        TimeLineBuilder builder = getTimeLineBuilder(items);
        builder.appendText("Hello world13");
        assertEquals(items.size(), 0);
        builder.complete();
        assertEquals(items.size(), 1);
        builder.appendText("Hello world sdfgsdfg sdfg sdfg as d wqerqwe rsdfasdsdfg sdf61");
        assertEquals(items.size(), 1);
        builder.complete();
        assertEquals(items.size(), 2);
        String text = "adslkfjaldskf1 asdfjklhasdf2 asdwoeiruqwopieu3 asdkjfhalskjd4 asdkjfh5 aldskjf6 hlakjsdf7 " +
                "askdljfha8 sdlkfjhasdwerqwernmbq9 asdf10 asdf11 sdasdfasd12 asdfasd13 asdf14 asdf15 asdf16 asdf17  adslkfjaldskf18 asdfjklhasdf19" +
                " asdwoeiruqwopieu20 asdkjfhalskjd21 asdkjfh aldskjf hlakjsdf askdljfha sdlkfjhasdwerqwernmbq asdf asdf " +
                "sdasdfasd asdfasd asdf asdf asdf asdf";
        int iCh = 0;
        while (iCh < 65){
            builder.appendChar(text.charAt(iCh++));
        }
        assertEquals(items.size(), 2);
        while (iCh < text.length()){
            builder.appendChar(text.charAt(iCh++));
        }
        assertEquals(items.size(), 4);
        builder.complete();
        assertEquals(items.size(), 5);
    }

    public void testTestFromBigFile() throws InterruptedException, IOException {
        sleep(300);
        TimeLineBuilder builder = getTimeLineBuilder(new ArrayList<TimeLineItem>());
        InputStreamReader reader = new InputStreamReader(getInstrumentation().getContext().getResources().openRawResource(R.raw.war_peace_text));
        char buffer[] = new char[1024];
        try {
            while (true){
                int count = reader.read(buffer, 0, buffer.length);
                if (count == 0) {
                    break;
                }
                for (int i = 0; i < count; i++) {
                    builder.appendChar(buffer[i]);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            reader.close();
        }
    }

    public void testTestBigText() throws InterruptedException, IOException {
        sleep(300);
        ArrayList<TimeLineItem> items = new ArrayList<>();
        TimeLineBuilder builder = getTimeLineBuilder(items);
        for (int i = 0; i < BIG_TEXT.length(); i++) {
            builder.appendChar(BIG_TEXT.charAt(i));
        }
        builder.complete();
        StringBuilder checker = new StringBuilder();
        for (TimeLineItem item : items) {
            if (item instanceof TextItem) {
                checker.append(item.getText());
            }
        }
        assertTrue(TextUtils.equals(BIG_TEXT.replace("\n", ""), checker.toString()));
    }

    public void testSpeakBigText() throws Exception {
        sleep(300);
        final Config config = new ConfigImpl(getInstrumentation().getContext());
        TimeLineStorageImpl storage = new TimeLineStorageImpl(config, "sdf");
        final TextToSpeechProviderImpl tts = new TextToSpeechProviderImpl(config);
        final ArrayList<TimeLineItem> items = new ArrayList<>();
        final ArrayList<String> ttsResult = new ArrayList<>();
        tts.setStateListener(new TextToSpeechProvider.StateListener() {
            @Override
            public void onReady() {

            }

            @Override
            public void onUtteranceStart(@NonNull String id) {

            }

            @Override
            public void onUtteranceDone(@NonNull String id) {
                ttsResult.add(id);
            }

            @Override
            public void onUtteranceError(@NonNull String id) {

            }

            @Override
            public void onError(TextToSpeechProvider.FailReason reason) {

            }
        });
        TimeLineBuilderImpl builder = new TimeLineBuilderImpl(
                config, storage, new TimeLineBuilder.Receiver() {
            @Override
            public void onComplete(@NonNull final BaseTimeLineItem item) {
                items.add(item);
                config.getDispatcher().post(new Runnable() {
                    @Override
                    public void run() {
                        if (item instanceof TextItem) {
                            tts.playUtterance(item.getId(), item.getText());
                        } else if (item instanceof SilenceItem) {
                            tts.playSilentUtterance(item.getId(), ((SilenceItem) item).getDuration());
                        }
                    }
                });
            }
        });
        for (int i = 0; i < SMALL_TEXT.length(); i++) {
            builder.appendChar(SMALL_TEXT.charAt(i));
        }
        builder.complete();
        sleep(60000);
        assertEquals(items.size(), ttsResult.size());
    }

    private TimeLineBuilder getTimeLineBuilder(final List<TimeLineItem> items) {
        Config config = new ConfigImpl(getInstrumentation().getContext());
        TimeLineStorageImpl storage = new TimeLineStorageImpl(config, "sdf");
        return new TimeLineBuilderImpl(
                config, storage, new TimeLineBuilder.Receiver() {
            @Override
            public void onComplete(@NonNull BaseTimeLineItem item) {
                if (item instanceof TextItem) {
                    if (item.getText().length() > 150) {
                        throw new IllegalStateException(item.getText());
                    }
                    Log.v("TimeLineBuilderImplTest", item.getText());
                } else {
                    Log.v("TimeLineBuilderImplTest", item.getClass().getName());
                }
                items.add(item);
            }
        });
    }

    private void testDifferentBuilderMethods(String text) throws InterruptedException {
        sleep(300);
        ArrayList<TimeLineItem> items = new ArrayList<>();
        TimeLineBuilder builder = getTimeLineBuilder(items);
        builder.appendText(text);
        builder.complete();
        StringBuilder checker = new StringBuilder();
        for (TimeLineItem item : items) {
            if (item instanceof TextItem) {
                checker.append(item.getText());
            }
        }
        assertTrue(TextUtils.equals(text.replace("\n", ""), checker.toString()));

        ArrayList<TimeLineItem> items2 = new ArrayList<>();
        TimeLineBuilder builder2 = getTimeLineBuilder(items2);
        for (int i = 0; i < text.length(); ++i) {
            builder2.appendChar(text.charAt(i));
        }
        builder2.complete();
        StringBuilder checker2 = new StringBuilder();
        for (TimeLineItem item : items2) {
            if (item instanceof TextItem) {
                checker2.append(item.getText());
            }
        }
        assertTrue(TextUtils.equals(text.replace("\n", ""), checker2.toString()));
        assertEquals(items.size(), items2.size());
    }

    public void testNoNewLinesText() throws InterruptedException {
        testDifferentBuilderMethods(SMALL_TEXT.replace("\n", ""));
    }

    public void testNewLinesText() throws InterruptedException {
        testDifferentBuilderMethods(SMALL_TEXT);
    }
}