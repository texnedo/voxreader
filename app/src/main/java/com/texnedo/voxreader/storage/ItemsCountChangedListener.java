package com.texnedo.voxreader.storage;

public interface ItemsCountChangedListener {
    void onComplete(long count);
}
