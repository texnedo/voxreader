package com.texnedo.voxreader.net;

import android.content.Context;
import android.support.annotation.NonNull;

import com.texnedo.voxreader.utils.FileLog;
import com.texnedo.voxreader.utils.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.ProtocolException;

public class CachedHttpConnectionImpl implements HttpConnection {
    private static final String DEFAULT_CACHE_FOLDER = "httpcache";
    private static final int DEFAULT_CACHE_FILE_KEEP_TIMEOUT = 1000 * 60 * 60 * 24 * 5;
    private static final String LOG_TAG = "CachedConnection";
    private static volatile File cacheFolder;
    private final File cacheFile;
    private final String url;
    private final ConnectionBuilder builder;
    private final CachePolicy policy;
    private HttpConnection connection;

    private CachedHttpConnectionImpl(@NonNull String url,
                                     @NonNull ConnectionBuilder builder,
                                     @NonNull CachePolicy policy) {
        this.url = url;
        this.builder = builder;
        this.policy = policy;
        this.cacheFile = new File(getCacheFolder(), Utils.stringToMD5(url));
    }

    public static CachedConnectionBuilder getBuilder(@NonNull Context context, String url) throws IOException, ClientException {
        return new BuilderImpl(context, url);
    }

    @Override
    public int getResponseCode() throws IOException, ClientException {
        if (checkValidCacheFile()) {
            return HttpURLConnection.HTTP_OK;
        }
        return getConnection().getResponseCode();
    }

    @Override
    public String getResponseAsString() throws IOException, ServerException, ClientException {
        //TODO - implement logic of reading sequential chunks from the Internet and saving them into a cache file (file should have special name)
        if (checkValidCacheFile()) {
            try {
                return readFile(cacheFile);
            } catch (IOException ex) {
                FileLog.e(LOG_TAG, "failed to read cache item for url %s", url);
                dropCacheFile();
            }
        }
        String response = getConnection().getResponseAsString();
        try {
            writeFile(cacheFile, response);
        } catch (IOException ex) {
            FileLog.e(LOG_TAG, "failed to save cache item for url %s", url);
        }
        return response;
    }

    @Override
    public String getHeaderField(String s) {
        if (checkValidCacheFile()) {
            return null;
        }
        return getConnection().getHeaderField(s);
    }

    public static void clearCache(@NonNull Context context) {
        if (!getDefaultCacheFolder(context.getApplicationContext()).delete()) {
            FileLog.e(LOG_TAG, "failed to delete cache folder");
        }
    }

    private synchronized HttpConnection getConnection() {
        if (connection == null) {
            connection = builder.build();
        }
        return connection;
    }

    private void dropCacheFile() {
        if (cacheFile.exists() && !cacheFile.delete()) {
            File tmpFile = new File(getCacheFolder(), cacheFile.getName() + "-tmp");
            //noinspection ResultOfMethodCallIgnored
            cacheFile.renameTo(tmpFile);
            if (!tmpFile.delete()) {
                FileLog.e(LOG_TAG, "failed to delete cache file");
            }
        }
    }

    private boolean checkValidCacheFile() {
        //TODO - implement max cache folder size check
        if (!cacheFile.exists()) {
            return false;
        }
        long diff = System.currentTimeMillis() - cacheFile.lastModified();
        if (diff < 0 || diff > DEFAULT_CACHE_FILE_KEEP_TIMEOUT) {
            return false;
        }
        FileLog.v(
                LOG_TAG,
                "found cache file for url %s, last modified %d(s) ago, size %d(b)",
                url, diff / 1000, cacheFile.length()
        );
        return cacheFile.length() != 0;
    }

    private File getCacheFolder() {
        if (policy.getCacheFolder() != null) {
            return policy.getCacheFolder();
        }
        if (cacheFolder == null) {
            cacheFolder = getDefaultCacheFolder(policy.getContext().getApplicationContext());
        }
        return cacheFolder;
    }

    private static File getDefaultCacheFolder(@NonNull Context context) {
        File folder = new File(context.getCacheDir(), DEFAULT_CACHE_FOLDER);
        if (!folder.exists()) {
            if (!folder.mkdir()) {
                return context.getCacheDir();
            }
        }
        return folder;
    }

    private static String readFile(@NonNull File file) throws IOException {
        RandomAccessFile f = new RandomAccessFile(file, "r");
        byte[] bytes = new byte[(int) f.length()];
        f.readFully(bytes);
        f.close();
        return new String(bytes);
    }

    private static void writeFile(@NonNull File file, @NonNull String data) throws IOException {
        FileOutputStream out = new FileOutputStream(file);
        out.write(data.getBytes());
        out.close();
        //noinspection ResultOfMethodCallIgnored
        file.setLastModified(System.currentTimeMillis());
    }

    private static class CachePolicy {
        private final long maxCacheSize;
        private final long keepFileTimeout;
        private final File cacheFolder;
        private final Context context;

        private CachePolicy(long maxCacheSize,
                            long keepFileTimeout,
                            File cacheFolder,
                            @NonNull Context context) {
            this.maxCacheSize = maxCacheSize;
            this.keepFileTimeout = keepFileTimeout;
            this.cacheFolder = cacheFolder;
            this.context = context;
        }

        public long getMaxCacheSize() {
            return maxCacheSize;
        }

        public File getCacheFolder() {
            return cacheFolder;
        }

        public long getKeepFileTimeout() {
            return keepFileTimeout;
        }

        public Context getContext() {
            return context;
        }
    }

    private static class BuilderImpl implements CachedConnectionBuilder {
        private final ConnectionBuilder builder;
        private final Context context;
        private final String url;
        private long maxCacheSize;
        private File cacheFolder;
        private long keepFileTimeout;

        public BuilderImpl(@NonNull Context context, String url) throws IOException, ClientException {
            this.context = context;
            this.url = url;
            builder = HttpConnectionImpl.getBuilder(url);
        }

        @Override
        public ConnectionBuilder setMethod(Method method) throws ProtocolException {
            builder.setMethod(method);
            return this;
        }

        @Override
        public ConnectionBuilder setKeepAlive(boolean keepAlive) {
            builder.setKeepAlive(keepAlive);
            return this;
        }

        @Override
        public ConnectionBuilder allowRedirects(boolean allow) {
            builder.allowRedirects(allow);
            return this;
        }

        @Override
        public ConnectionBuilder setConnectTimeout(int ms) {
            builder.setConnectTimeout(ms);
            return this;
        }

        @Override
        public ConnectionBuilder setReadTimeout(int ms) {
            builder.setReadTimeout(ms);
            return this;
        }

        @Override
        public ConnectionBuilder addHeader(String header, String value) {
            builder.addHeader(header, value);
            return this;
        }

        @Override
        public ConnectionBuilder setCheckResponseEncoding(boolean checkEncoding) {
            builder.setCheckResponseEncoding(checkEncoding);
            return this;
        }

        @Override
        public ConnectionBuilder acceptGzip(boolean accept) {
            builder.acceptGzip(accept);
            return this;
        }

        @Override
        public CachedConnectionBuilder setMaxCacheSize(long maxCacheSize) {
            this.maxCacheSize = maxCacheSize;
            return this;
        }

        @Override
        public CachedConnectionBuilder setCacheFolder(@NonNull File cacheFolder) {
            this.cacheFolder = cacheFolder;
            return this;
        }

        @Override
        public CachedConnectionBuilder setKeepCacheItemTimeout(long timeout) {
            this.keepFileTimeout = timeout;
            return this;
        }

        @Override
        public HttpConnection build() {
            return new CachedHttpConnectionImpl(
                    url,
                    builder,
                    new CachePolicy(maxCacheSize, keepFileTimeout, cacheFolder, context)
            );
        }
    }
}
