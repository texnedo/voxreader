package com.texnedo.voxreader.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.texnedo.voxreader.BuildConfig;

public class FileLog {
    private static volatile LogReceiver sInstance = null;
    private static volatile Context sInitContext = null;
    private static volatile LogReceiver sInitReceiver;

    public static void init(@NonNull Context context, @Nullable LogReceiver logReceiver) {
        sInitContext = context;
        sInitReceiver = logReceiver;
    }

    private static boolean writeLogs() {
        if (BuildConfig.DEBUG && sInitContext != null) {
            return true;
        }
        return sInitReceiver != null;
    }

    private static LogReceiver getInstance() {
        LogReceiver localInstance = sInstance;
        if (localInstance == null) {
            synchronized (FileLog.class) {
                localInstance = sInstance;
                if (localInstance == null) {
                    if (sInitReceiver != null) {
                        sInstance = localInstance = sInitReceiver;
                    } else if (BuildConfig.DEBUG) {
                        sInstance = localInstance = new DebugFileLogImpl(sInitContext);
                    }
                }
            }
        }
        return localInstance;
    }

    public static void e(final String tag, final String message, final Throwable exception) {
        if (!writeLogs()) {
            return;
        }
        getInstance().e(tag, message, exception);
    }

    public static void e(final String tag, final String message) {
        if (!writeLogs()) {
            return;
        }
        getInstance().e(tag, message);
    }

    public static void d(final String tag, final String message, final Throwable exception) {
        if (!writeLogs()) {
            return;
        }
        getInstance().d(tag, message, exception);
    }

    public static void d(final String tag, final String message) {
        if (!writeLogs()) {
            return;
        }
        getInstance().d(tag, message);
    }

    public static void v(final String tag, final String message, final Throwable exception) {
        if (!writeLogs()) {
            return;
        }
        getInstance().v(tag, message, exception);
    }

    public static void v(final String tag, final String message) {
        if (!writeLogs()) {
            return;
        }
        getInstance().v(tag, message);
    }

    public static void e(final String tag, final Throwable exception, final String messageFormat, final Object... arguments) {
        if (!writeLogs()) {
            return;
        }
        String message = String.format(messageFormat, arguments);
        getInstance().e(tag, message, exception);
    }

    public static void e(final String tag, final String messageFormat, Object... arguments) {
        if (!writeLogs()) {
            return;
        }
        String message = String.format(messageFormat, arguments);
        getInstance().e(tag, message);
    }

    public static void d(final String tag, final Throwable exception, final String messageFormat, final Object... arguments) {
        if (!writeLogs()) {
            return;
        }
        String message = String.format(messageFormat, arguments);
        getInstance().d(tag, message, exception);
    }

    public static void d(final String tag, final String messageFormat, final Object... arguments) {
        if (!writeLogs()) {
            return;
        }
        String message = String.format(messageFormat, arguments);
        getInstance().d(tag, message);
    }

    public static void v(final String tag, final Throwable exception, final String messageFormat, final Object... arguments) {
        if (!writeLogs()) {
            return;
        }
        String message = String.format(messageFormat, arguments);
        getInstance().v(tag, message, exception);
    }

    public static void v(final String tag, final String messageFormat, final Object... arguments) {
        if (!writeLogs()) {
            return;
        }
        String message = String.format(messageFormat, arguments);
        getInstance().v(tag, message);
    }

}
