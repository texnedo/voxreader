package com.texnedo.voxreader.core.api;

public enum  DataSourceState {
    INITIAL(0),
    PREPARING(1),
    FAILED(2),
    READY(3),
    PLAYING(4),
    PAUSED(5);

    private final int value;

    DataSourceState(final int newValue) {
        value = newValue;
    }

    public int getValue() { return value; }

    public static DataSourceState from(Integer value) {
        if (value == null) {
            return INITIAL;
        }
        switch (value) {
            case 0: {
                return INITIAL;
            } case 1: {
                return PREPARING;
            } case 2: {
                return FAILED;
            } case 3: {
                return READY;
            } case 4: {
                return PLAYING;
            } case 5: {
                return PAUSED;
            } default: {
                throw  new IllegalArgumentException();
            }
        }
    }
}
