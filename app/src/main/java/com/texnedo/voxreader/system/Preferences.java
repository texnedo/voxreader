package com.texnedo.voxreader.system;

import android.support.annotation.NonNull;

public interface Preferences {
    boolean playNextAfterCompletion();

    void setPlayNextAfterCompletion(boolean value);

    boolean showTextInNotification();

    void setShowTextInNotification(boolean value);

    boolean autoDownloadEnabled();

    void setAutoDownloadEnabled(boolean value);

    void addListener(@NonNull PreferenceListener listener);

    void removeListener(@NonNull PreferenceListener listener);

    interface PreferenceListener {
        void onChanged();
    }
}
