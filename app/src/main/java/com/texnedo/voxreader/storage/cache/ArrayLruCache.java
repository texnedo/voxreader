package com.texnedo.voxreader.storage.cache;

import com.texnedo.voxreader.core.timeline.TimeLineItem;

import java.util.List;

public class ArrayLruCache {
    private final List[] items;
    private final int maxSize;
    private final int rangeSize;

    public ArrayLruCache(int maxSize, int rangeSize) {
        this.maxSize = maxSize;
        this.rangeSize = rangeSize;
        items = new List[maxSize];
    }

    public List<TimeLineItem> get(Range range) {
        int index = (int)(range.from / (range.to - range.from));
        if (items.length == 0 || index >= items.length) {
            return null;
        }
        //noinspection unchecked
        return items[index];
    }

    public List<TimeLineItem> get(long itemId) {
        int index = (int)(itemId / rangeSize);
        if (items.length == 0 || index >= items.length) {
            return null;
        }
        //noinspection unchecked
        return items[0];
    }

    public int maxSize() {
        return maxSize;
    }

    public void put(Range range, List<TimeLineItem> existing) {
        items[(int)(range.from / (range.to - range.from))] = existing;
    }

    public int size() {
        return items.length;
    }
}
