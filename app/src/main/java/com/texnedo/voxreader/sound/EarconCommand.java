package com.texnedo.voxreader.sound;

import android.support.annotation.NonNull;

import com.texnedo.voxreader.core.api.TextToSpeechProvider;

class EarconCommand extends Command {
    private static final CommandCache<EarconCommand> commandsCache = new CommandCache<>();
    private TextToSpeechProvider.EarconType type;
    private String id;

    protected EarconCommand(CommandContext context) {
        super(context);
    }

    @Override
    public boolean waitForCompletion() {
        return true;
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    protected void recycleInternal() {
        id = null;
        type = TextToSpeechProvider.EarconType.UNKNOWN;
        commandsCache.recycle(this);
    }

    protected static Command obtain(@NonNull CommandContext context,
                                    @NonNull String id,
                                    TextToSpeechProvider.EarconType type) {
        EarconCommand command = commandsCache.obtain();
        if (command == null) {
            command = new EarconCommand(context);
        }
        command.id = id;
        command.type = type;
        return command;
    }

    @Override
    public void run() {
        context.playEarcon(id, type);
    }
}
