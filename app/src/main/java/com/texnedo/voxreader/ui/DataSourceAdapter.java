package com.texnedo.voxreader.ui;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.texnedo.voxreader.R;
import com.texnedo.voxreader.core.api.DataSource;
import com.texnedo.voxreader.core.api.TimeLineAccess;
import com.texnedo.voxreader.core.api.TimeLineManager;
import com.texnedo.voxreader.utils.UiThread;

import java.util.List;

public class DataSourceAdapter extends BaseAdapter {
    private final TimeLineManager manager;
    private final TimeLineManager.DataSourceChangeListener listener =
            new TimeLineManager.DataSourceChangeListener() {
                @Override
                public void onItemsChanged() {
                    UiThread.run(new Runnable() {
                        @Override
                        public void run() {
                            dataSources = null;
                            notifyDataSetChanged();
                        }
                    });
                }

                @Override
                public void onStateChanged(@NonNull TimeLineAccess dataSource) {
                    UiThread.run(new Runnable() {
                        @Override
                        public void run() {
                            notifyDataSetChanged();
                        }
                    });
                }

                @Override
                public void onFailure() {
                    //TODO - show notification
                }
            };
    private List<TimeLineAccess> dataSources;
    private final LayoutInflater inflater;

    public DataSourceAdapter(@NonNull TimeLineManager manager) {
        this.manager = manager;
        this.inflater = LayoutInflater.from(manager.getContext());
    }

    public void subscribe() {
        manager.addListener(listener);
        dataSources = null;
        notifyDataSetInvalidated();
    }

    public void unSubscribe() {
        manager.removeListener(listener);
    }

    @Override
    public int getCount() {
        if (dataSources == null) {
            manager.requestDataSources(new TimeLineManager.DataSourceRequestListener() {
                @Override
                public void onComplete(@NonNull final List<TimeLineAccess> items) {
                    UiThread.run(new Runnable() {
                        @Override
                        public void run() {
                            dataSources = items;
                            notifyDataSetChanged();
                        }
                    });
                }

                @Override
                public void onFailure() {
                    //TODO - show notification
                }
            });
            return 0;
        }
        return dataSources.size();
    }

    @Override
    public Object getItem(int position) {
        return dataSources.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (dataSources == null) {
            return null;
        }
        View view;
        if (convertView == null) {
            view = inflater.inflate(R.layout.datasource_item, parent, false);
        } else {
            view = convertView;
        }
        TimeLineAccess dataSource = dataSources.get(position);
        TextView name = (TextView) view.findViewById(R.id.name);
        name.setText(dataSource.getName());
        if (dataSource.isFailed()) {
            name.setTextColor(Color.RED);
        } else {
            name.setTextColor(Color.BLUE);
        }
        TextView description = (TextView) view.findViewById(R.id.description);
        description.setText(dataSource.getDescription());
        ImageView bookmark = (ImageView) view.findViewById(R.id.bookmark);
        bookmark.setImageResource(R.drawable.ic_bookmark_border_black_36dp);
        TextView language = (TextView) view.findViewById(R.id.language);
        language.setText(dataSource.getLanguage());
        return view;
    }
}
