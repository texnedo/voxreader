package com.texnedo.voxreader.sound;

import android.support.annotation.NonNull;

class PitchCommand extends Command {
    private float pitch;

    @Override
    public String id() {
        return null;
    }

    @Override
    public boolean waitForCompletion() {
        return false;
    }

    protected PitchCommand(CommandContext context) {
        super(context);
    }

    @Override
    protected void recycleInternal() {
        pitch = 0;
    }

    @Override
    public void run() {
        context.setPitch(pitch);
    }

    protected static Command obtain(@NonNull CommandContext context,
                                    float pitch) {
        PitchCommand command = new PitchCommand(context);
        command.pitch = pitch;
        return command;
    }
}
