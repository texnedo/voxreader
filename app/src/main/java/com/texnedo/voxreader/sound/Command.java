package com.texnedo.voxreader.sound;

import android.net.Uri;
import android.support.annotation.NonNull;

import com.texnedo.voxreader.core.api.TextToSpeechProvider;

import java.util.Locale;

abstract class Command implements Runnable {
    private boolean isInUse = false;
    protected final CommandContext context;

    public void recycle() {
        isInUse = false;
        recycleInternal();
    }

    public abstract boolean waitForCompletion();

    public abstract String id();

    public static Command obtainPlayCommand(@NonNull CommandContext context,
                                            @NonNull String id,
                                            @NonNull String utterance,
                                            TextToSpeechProvider.SpeechVolume volume) {
        Command command = PlayCommand.obtain(context, id, utterance, volume);
        command.isInUse = true;
        return command;
    }

    public static Command obtainPlayCommand(@NonNull CommandContext context,
                                            @NonNull String id,
                                            @NonNull String utterance) {
        return obtainPlayCommand(context, id, utterance, null);
    }

    public static Command obtainSilentCommand(@NonNull CommandContext context,
                                            @NonNull String id,
                                            long duration) {
        return SilentCommand.obtain(context, id, duration);
    }

    public static Command obtainSpeechRateCommand(@NonNull CommandContext context,
                                              float rate) {
        return SpeechRateCommand.obtain(context, rate);
    }

    public static Command obtainPitchCommand(@NonNull CommandContext context,
                                                  float pitch) {
        return PitchCommand.obtain(context, pitch);
    }

    public static Command obtainLanguageCommand(@NonNull CommandContext context,
                                             @NonNull Locale locale) {
        return LanguageCommand.obtain(context, locale);
    }

    public static Command obtainEarconCommand(@NonNull CommandContext context,
                                              @NonNull String id,
                                              TextToSpeechProvider.EarconType type) {
        return EarconCommand.obtain(context, id, type);
    }

    public static Command obtainMusicCommand(@NonNull CommandContext context,
                                              @NonNull String id,
                                              @NonNull Uri resource,
                                              boolean loop) {
        return PlayMusicCommand.obtain(context, id, resource, loop);
    }

    protected Command(CommandContext context) {
        this.context = context;
    }

    protected abstract void recycleInternal();

    protected boolean isInUse() {
        return isInUse;
    }
}
