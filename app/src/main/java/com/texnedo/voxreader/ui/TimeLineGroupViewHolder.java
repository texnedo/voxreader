package com.texnedo.voxreader.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.LongSparseArray;
import android.text.Layout;
import android.text.ParcelableSpan;
import android.text.Spannable;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.BackgroundColorSpan;
import android.text.style.CharacterStyle;
import android.text.style.ClickableSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.LruCache;
import android.view.View;
import android.view.WindowManager;

import com.texnedo.voxreader.R;
import com.texnedo.voxreader.core.api.TimeLineAccess;
import com.texnedo.voxreader.core.timeline.BaseTimeLineItem;
import com.texnedo.voxreader.core.timeline.TimeLineItem;
import com.texnedo.voxreader.storage.ItemsReceiver;
import com.texnedo.voxreader.system.Config;
import com.texnedo.voxreader.utils.FileLog;
import com.texnedo.voxreader.utils.UiThread;
import com.texnedo.voxreader.utils.UiUtils;

import java.util.List;
import java.util.Locale;

class TimeLineGroupViewHolder {
    private static final String LOG_TAG = "GroupViewHolder";
    //TODO - move this to config
    private static final int MAX_BITMAP_INSTANCES_IN_CACHE =
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT ? 5 : 3;
    private static final int MAX_AVAILABLE_BITMAP_INSTANCES = 10;
    private static final int MAX_LAYOUT_INSTANCES = 200;
    private static final int MAX_SPANNABLE_INSTANCES = 200;
    private static final char[] MAX_CHAR_FOR_MEASURE = new char[] { 'W' };
    private static final int SCREEN_HEIGHT_DIVIDER = 5;
    private static final int RIGHT_TEXT_BORDER = 15;
    private static final int INITIAL_ACTIVATION_DELAY = 200;
    //
    private static final LruCache<TimeLineGroupViewHolder, StaticLayout> layoutCache
            = new LruCache<>(MAX_LAYOUT_INSTANCES);
    private static final LruCache<TimeLineGroupViewHolder, SpannableBuilder> spannableCache
            = new LruCache<>(MAX_SPANNABLE_INSTANCES);
    private static BitmapCache<TimeLineBitmapSourceImpl> bitmapCache =
            new BitmapCache<>(MAX_BITMAP_INSTANCES_IN_CACHE, MAX_AVAILABLE_BITMAP_INSTANCES);
    private static final Canvas canvas = new Canvas();
    private static volatile ParcelableSpan currentReadingSpan;
    private static volatile TextPaint textPaint;
    private static volatile Rect maxCharRect;
    private final LongSparseArray<TimeLinePosition> positions;
    private final Config config;
    private final String id;
    private final int position;
    private final long from;
    private final long to;
    private final TimeLineAccess access;
    private final TimeLineGroupListener groupListener;
    private TimeLineBitmapSourceImpl delegate;
    private volatile String playingItemId;

    TimeLineGroupViewHolder(@NonNull Config config,
                            long from,
                            long to,
                            @NonNull TimeLineAccess access,
                            int position,
                            @Nullable TimeLineGroupListener listener) {
        this.config = config;
        this.from = from;
        this.to = to;
        this.access = access;
        this.groupListener = listener;
        this.position = position;
        this.positions = new LongSparseArray<>((int)(to - from + 1));
        this.id = String.format(Locale.US, "%d - %d", from, to);
        getBuilder();
        Log.v(LOG_TAG, "created group with id " + id);
    }

    public static void onTrimMemory() {
        Log.d(LOG_TAG, "resources cleanup started");
        layoutCache.evictAll();
        bitmapCache.evictAll();
        spannableCache.evictAll();
        Log.d(LOG_TAG, "resources cleanup completed");
    }

    public TimeLineBitmapSourceImpl getTimeLineLayoutDelegate() {
        if (delegate == null) {
            delegate = new TimeLineBitmapSourceImpl();
        }
        return delegate;
    }

    public int getPosition() {
        return position;
    }

    private void processItems(@NonNull List<TimeLineItem> items,
                              @NonNull SpannableBuilder builder) {
        ItemListener itemListener = new ItemListener();
//        if (BuildConfig.DEBUG) {
//            builder.append(id).append(" ");
//        }
        for (TimeLineItem item : items) {
            String itemText = item.getText();
            if (!TextUtils.isEmpty(itemText)) {
//                if (BuildConfig.DEBUG) {
//                    builder.append(item.getId()).append(" ");
//                }
                int currentLength = builder.length();
                positions.put(
                        item.getNumber(),
                        new TimeLinePosition(currentLength, currentLength + itemText.length())
                );
                builder.append(itemText);
                item.setListener(itemListener);
                if (item.isPlaying()) {
                    applyStyleSpanToItem(item, builder);
                    notifyItemActivationWithOffsetDelayed(item);
                    playingItemId = item.getId();
                }
                if (item.canBeActivated()) {
                    applyActivationSpanToItem(item, builder);
                }
                builder.append(" ");
            }
        }
        Log.v(LOG_TAG, "items processed for group with id " + id);
    }

    @Nullable
    private SpannableBuilder getBuilder() {
        SpannableBuilder builder = spannableCache.get(this);
        if (builder != null) {
            return builder;
        }
        List<TimeLineItem> items = access.getItems(from, to);
        if (items != null) {
            return createBuilder(items);
        }
        access.loadItems(from, to, new ItemsReceiver() {
            @Override
            public void onComplete(@NonNull final List<TimeLineItem> items) {
                UiThread.run(new Runnable() {
                    @Override
                    public void run() {
                        createBuilder(items);
                        getTimeLineLayoutDelegate().notifyLayoutChanged();
                    }
                });
            }

            @Override
            public void onFailure(@NonNull Exception ex) {
                FileLog.e(LOG_TAG, "failed to load items", ex);
            }
        });
        return null;
    }

    private SpannableBuilder createBuilder(List<TimeLineItem> items) {
        SpannableBuilder builder = new CustomSpannableBuilder();
        processItems(items, builder);
        spannableCache.put(this, builder);
        return builder;
    }

    private void applyActivationSpanToItem(@NonNull TimeLineItem item,
                                           @NonNull SpannableBuilder builder) {
        TimeLinePosition position = positions.get(item.getNumber());
        if (position == null) {
            throw new IllegalStateException("Item must be added to the positions map");
        }
        builder.setSpan(
                new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        //TODO - implement link click support by calculation of a touch position
                    }
                },
                position.getStart(),
                position.getEnd(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        );
    }

    private void applyStyleSpanToItem(@NonNull TimeLineItem item,
                                      @NonNull SpannableBuilder builder) {
        TimeLinePosition position = positions.get(item.getNumber());
        if (position == null) {
            throw new IllegalStateException("Item must be added to the positions map");
        }
        Log.v(LOG_TAG, "apply selection style to " + position);
        builder.setSpan(
                getCurrentReadingSpan(config.getContext()),
                position.getStart(),
                position.getEnd(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        );
    }

    private void clearStyleSpan(@NonNull SpannableBuilder builder) {
        builder.removeSpan(getCurrentReadingSpan(config.getContext()));
    }

    private StaticLayout getLayout(int width) {
        StaticLayout layout = layoutCache.get(this);
        if (layout == null || layout.getWidth() != width) {
            return null;
        }
        return layout;
    }

    private void clearLayout() {
        layoutCache.remove(this);
    }

    @Nullable
    private Bitmap createBitmap(int width) {
        StaticLayout created = createLayout(width);
        if (created == null) {
            return null;
        }
        Bitmap bitmap = bitmapCache.create(
                created.getWidth(),
                created.getHeight(),
                TextUtils.isEmpty(playingItemId) ? Bitmap.Config.ALPHA_8 : Bitmap.Config.ARGB_8888
        );
        canvas.setBitmap(bitmap);
        created.draw(canvas);
        return bitmap;
    }

    private StaticLayout createLayout(int width) {
        SpannableBuilder builder = getBuilder();
        if (builder == null) {
            return null;
        }
        StaticLayout existing = getLayout(width);
        if (existing == null) {
            existing = new StaticLayout(
                    builder.build(getTextPaint(config.getContext()), width - RIGHT_TEXT_BORDER),
                    getTextPaint(config.getContext()),
                    width,
                    Layout.Alignment.ALIGN_NORMAL,
                    1.0f,
                    0.0f,
                    false
            );
            layoutCache.put(this, existing);
        }
        return existing;
    }

    private static ParcelableSpan getCurrentReadingSpan(@NonNull Context context) {
        if (currentReadingSpan == null) {
            synchronized (TimeLineGroupViewHolder.class) {
                if (currentReadingSpan == null) {
                    currentReadingSpan = new BackgroundColorSpan(
                            context.getResources().getColor(R.color.readingHighlight)
                    );
                }
            }
        }
        return currentReadingSpan;
    }

    private static Rect getMaxCharRect(@NonNull Context context) {
        getTextPaint(context);
        return maxCharRect;
    }

    private static TextPaint getTextPaint(@NonNull Context context) {
        if (textPaint == null) {
            synchronized (TimeLineGroupViewHolder.class) {
                if (textPaint == null) {
                    textPaint = new TextPaint();
                    textPaint.setTypeface(Typeface.SANS_SERIF);
                    DisplayMetrics metrics = new DisplayMetrics();
                    WindowManager windowManager = (WindowManager) context
                            .getSystemService(Context.WINDOW_SERVICE);
                    windowManager.getDefaultDisplay().getMetrics(metrics);
                    textPaint.setTextSize(20 * metrics.scaledDensity);
                    textPaint.setAntiAlias(true);
                    textPaint.linkColor = Color.BLUE;
                    maxCharRect = new Rect();
                    textPaint.getTextBounds(MAX_CHAR_FOR_MEASURE, 0, 1, maxCharRect);
                }
            }
        }
        return textPaint;
    }

    private void notifyItemLayoutChange(@NonNull TimeLineItem item) {
        SpannableBuilder builder = getBuilder();
        if (builder == null) {
            return;
        }
        clearStyleSpan(builder);
        if (item.isPlaying()) {
            playingItemId = item.getId();
            applyStyleSpanToItem(item, builder);
        } else {
            playingItemId = null;
        }
        clearLayout();
        UiThread.run(new Runnable() {
            @Override
            public void run() {
                getTimeLineLayoutDelegate().notifyLayoutChanged();
            }
        });
    }

    private void notifyItemActivationWithOffsetDelayed(@NonNull final TimeLineItem item) {
        UiThread.runLater(new Runnable() {
            @Override
            public void run() {
                notifyItemActivationWithOffset(item);
            }
        }, INITIAL_ACTIVATION_DELAY);
    }

    private void notifyItemActivationWithOffset(@NonNull TimeLineItem item) {
        TimeLinePosition position = positions.get(item.getNumber());
        Rect charRect = getMaxCharRect(config.getContext());
        if (position != null && charRect != null) {
            int width = UiUtils.getScreenWidth();
            int maxCharsOnRow = width / charRect.width();
            final int offset = UiUtils.getScreenHeight() / SCREEN_HEIGHT_DIVIDER -
                    position.getStart() / maxCharsOnRow * charRect.height();
            Log.v(LOG_TAG, "playing item offset " + offset);
            UiThread.run(new Runnable() {
                @Override
                public void run() {
                    getTimeLineLayoutDelegate().notifyLayoutActivated(offset);
                }
            });
        }
    }

    private class TimeLineBitmapSourceImpl implements TimeLineBitmapSource {
        private volatile TimeLineBitmapListener layoutListener;

        @Override
        public void requestBitmap(int width) {
            requestBitmapInternal(width);
        }

        @Override
        public void setLayoutListener(TimeLineBitmapListener listener) {
            layoutListener = listener;
        }

        @Override
        public Bitmap getCachedBitmap(int width) {
            return bitmapCache.get(this, width);
        }

        @Override
        public String getId() {
            return id;
        }

        private void notifyLayoutChanged() {
            if (layoutListener != null) {
                layoutListener.onLayoutChanged();
            }
        }

        private void notifyLayoutActivated(int offset) {
            if (groupListener != null) {
                groupListener.onActivated(TimeLineGroupViewHolder.this, offset);
            }
        }

        private void requestBitmapInternal(final int width) {
            config.getLayoutRenderExecutor().submit(new Runnable() {
                @Override
                public void run() {
                    if (layoutListener == null) {
                        return;
                    }
                    final Bitmap createdBitmap = createBitmap(width);
                    if (createdBitmap == null) {
                        return;
                    }
                    UiThread.run(new Runnable() {
                        @Override
                        public void run() {
                            bitmapCache.put(TimeLineBitmapSourceImpl.this, createdBitmap);
                            if (layoutListener != null) {
                                layoutListener.onCompleted(createdBitmap);
                            }
                            Log.v(LOG_TAG, String.format("layout created for group %s", id));
                        }
                    });
                }
            });
        }
    }

    private class ItemListener implements BaseTimeLineItem.Listener {
        @Override
        public void onChanged(@NonNull final TimeLineItem item) {
            config.getLayoutRenderExecutor().submit(new Runnable() {
                @Override
                public void run() {
                    Log.v(
                            LOG_TAG,
                            String.format(
                                    "item %d changed in group %s, isPlaying %s",
                                    item.getNumber(),
                                    id,
                                    item.isPlaying()
                            )
                    );
                    notifyItemActivationWithOffset(item);
                    notifyItemLayoutChange(item);
                }
            });
        }
    }

    @Override
    public String toString() {
        return "TimeLineGroupViewHolder{" +
                "id='" + id + '\'' +
                ", position=" + position +
                ", from=" + from +
                ", to=" + to +
                '}';
    }
}
