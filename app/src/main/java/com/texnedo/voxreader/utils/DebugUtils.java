package com.texnedo.voxreader.utils;

import android.content.Intent;
import android.os.Bundle;

import com.texnedo.voxreader.BuildConfig;

import java.util.Iterator;
import java.util.Set;

public class DebugUtils {
    private static final String LOG_TAG = "Misc";

    @SuppressWarnings("serial")
    private static class SmartException extends RuntimeException {
        public SmartException(Throwable throwable) {
            super(throwable);
        }
    }

    public static void safeThrow(final Throwable t) {
        FileLog.e(LOG_TAG, "FATAL ERROR", t);
        if (BuildConfig.DEBUG) {
            throw new SmartException(t);
        } else {
            reportCrash(t);
        }
    }

    public static void printIntent(Intent i){
        if (BuildConfig.DEBUG) {
            Bundle bundle = i.getExtras();
            if (bundle != null) {
                Set<String> keys = bundle.keySet();
                Iterator<String> it = keys.iterator();
                FileLog.e(LOG_TAG, "Dumping Intent start");
                while (it.hasNext()) {
                    String key = it.next();
                    FileLog.e(LOG_TAG, "[" + key + "=" + bundle.get(key)+"]");
                }
                FileLog.e(LOG_TAG, "Dumping Intent end");
            }
        }
    }

    private static void reportCrash(Throwable t) {
        //TODO - implement me
    }
}
