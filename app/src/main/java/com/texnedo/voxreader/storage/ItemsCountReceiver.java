package com.texnedo.voxreader.storage;

public interface ItemsCountReceiver {
    void onComplete(long count);

    void onFailure(Exception ex);
}
