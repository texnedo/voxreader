package com.texnedo.voxreader;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import org.androidannotations.api.builder.ActivityIntentBuilder;

public class Navigate {
    public static final String DATA_SOURCE_ID_EXTRA = "data_source_id";

    public static void goToDataSources(@NonNull Context context) {
        DataSourceActivity_
                .intent(context)
                .flags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .start();
    }

    public static void goToTimeLine(@NonNull Context context,
                                    @NonNull String id) {
        getTimeLineBuilder(context, id).start();
    }

    public static Intent getTimeLineIntent(@NonNull Context context,
                                           @NonNull String id) {
        return getTimeLineBuilder(context, id).get();
    }

    public static void goToSettings(@NonNull Context context) {
        SettingsActivity_
                .intent(context)
                .flags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .start();
    }

    private static ActivityIntentBuilder getTimeLineBuilder(@NonNull Context context,
                                                            @NonNull String id) {
        return TimeLineActivity_
                .intent(context)
                .flags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .extra(DATA_SOURCE_ID_EXTRA, id);
    }
}
