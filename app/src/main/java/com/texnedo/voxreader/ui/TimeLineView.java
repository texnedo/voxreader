package com.texnedo.voxreader.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

class TimeLineView extends View {
    private static final String LOG_TAG = "TimeLineView";
    private static final Paint paint = new Paint();
    private final TimeLineLayoutListener listener = new TimeLineLayoutListener();
    private TimeLineBitmapSource source;
    private int renderWidth;

    public TimeLineView(Context context) {
        super(context);
    }

    public TimeLineView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setStaticLayoutDelegate(int width, @NonNull final TimeLineBitmapSource layoutSource) {
        renderWidth = width;
        if (source != null && source != layoutSource) {
            source.setLayoutListener(null);
            Log.v(LOG_TAG, "setStaticLayoutDelegate reset " + hashCode() + " " + source.getId());
        }
        source = layoutSource;
        Bitmap existing = getCachedBitmap();
        if (existing == null) {
            source.setLayoutListener(listener);
            source.requestBitmap(getDesiredBitmapWidth());
        } else {
            ViewGroup.LayoutParams params = getLayoutParams();
            params.height = existing.getHeight();
        }
        Log.v(LOG_TAG, "setStaticLayoutDelegate " + hashCode() + " " + source.getId());
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.v(LOG_TAG, "on touched " + event);
        super.onTouchEvent(event);
        return true;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Bitmap bitmap = getCachedBitmap();
        if (bitmap == null) {
            if (source != null) {
                source.requestBitmap(getDesiredBitmapWidth());
            }
            return;
        }
        canvas.save();
        canvas.drawBitmap(bitmap, 0, 0, paint);
        canvas.restore();
        Log.v(LOG_TAG, "onDraw " + hashCode() + " " + source.getId() + " h = " + bitmap.getHeight() + " hs = " + getHeight() + " lh = " + getLayoutParams().height);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private Bitmap getCachedBitmap() {
        if (source == null) {
            return null;
        }
        return source.getCachedBitmap(getDesiredBitmapWidth());
    }

    private int getDesiredBitmapWidth() {
        return getWidth() == 0 ? renderWidth : getWidth();
    }

    private class TimeLineLayoutListener implements TimeLineBitmapListener {
        @Override
        public void onCompleted(@NonNull Bitmap bitmap) {
            if (source == null) {
                return;
            }
            ViewGroup.LayoutParams params = getLayoutParams();
            params.height = bitmap.getHeight();
            requestLayout();
            Log.v(LOG_TAG, "onCompleted " + hashCode() + " " + source.getId() + " h = " + bitmap.getHeight());
        }

        @Override
        public void onLayoutChanged() {
            if (source == null) {
                return;
            }
            source.requestBitmap(getDesiredBitmapWidth());
            Log.v(LOG_TAG, "onLayoutChanged " + hashCode() + " " + source.getId());
        }
    }
}
