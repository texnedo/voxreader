package com.texnedo.voxreader.core.api;

public interface TimeLineAccess extends TimeLineItemsProvider {
    String getId();

    String getName();

    String getDescription();

    String getLanguage();

    String getPlayingText();

    Long getPlayingId();

    boolean isProcessing();

    boolean isFailed();

    boolean isPlaying();

    boolean isPaused();
}
