package com.texnedo.voxreader.storage.cache;

import android.support.annotation.NonNull;

import com.texnedo.voxreader.core.timeline.TimeLineItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public final class TimeLineCache {
    private static final String LOG_TAG = "TimeLineCache";
    private final RangeLruCache cache;
    private final int rangeSize;

    public TimeLineCache(int maxMemoryCacheSize, int rangeSize) {
        this.rangeSize = rangeSize;
        this.cache = new RangeLruCache(maxMemoryCacheSize / rangeSize);
    }

    public void evictAll() {
        cache.evictAll();
    }

    public void putAll(@NonNull List<TimeLineItem> items, boolean evict) {
        if (items.size() == 0) {
            throw new IllegalArgumentException("items must be non empty");
        }
        Range from = getByItem(items.get(0));
        Range to = getByItem(items.get(items.size() - 1));
        if (from.equals(to)) {
            putToRange(items, from, evict);
        } else {
            for (TimeLineItem item : items) {
                put(item, evict);
            }
        }
    }

    public void put(@NonNull TimeLineItem item, boolean evict) {
        Range range = getByItem(item);
        List<TimeLineItem> existing = cache.get(range);
        if (existing != null) {
            if (existing.get(existing.size() - 1).getNumber() + 1 != item.getNumber()) {
                throw new IllegalArgumentException(
                        "all sequential calls of this method must provide sequential item ranges"
                );
            }
            existing.add(item);
        } else if (canOverride(evict)) {
            existing = new ArrayList<>(rangeSize);
            existing.add(item);
            cache.put(range, existing);
        }
    }

    private boolean canOverride(boolean evict) {
        if (!evict && cache.maxSize() == cache.size()) {
            //items cache is full with saved items;
            //so do nothing, because we will request those cached items firstly
            return false;
        }
        return true;
    }

    public List<TimeLineItem> getAll(long from, long to) {
        Range range = getByBounds(from, to);
        return cache.get(range);
    }

    //TODO - check thread safety
    public TimeLineItem get(long itemId) {
        Range range = getById(itemId);
        List<TimeLineItem> existing = cache.get(range);
        if (existing == null) {
            return null;
        }
        int offset = (int)(itemId - range.from);
        if (offset >= existing.size()) {
            return null;
        }
        return existing.get(offset);
    }

    private void putToRange(@NonNull List<TimeLineItem> items, @NonNull Range range, boolean evict) {
        List<TimeLineItem> existing = cache.get(range);
        if (existing != null) {
            if (existing.get(existing.size() - 1).getNumber() + 1 != items.get(0).getNumber()) {
                throw new IllegalArgumentException(
                        "all sequential calls of this method must provide sequential item ranges "
                                + range
                );
            }
            existing.addAll(items);
        } else if (canOverride(evict)) {
            cache.put(range, items);
        }
    }

    private Range getByItem(@NonNull TimeLineItem item) {
        return getById(item.getNumber());
    }

    private Range getById(long itemId) {
        long from = itemId / rangeSize * rangeSize;
        return Range.getRange(from, from + rangeSize - 1);
    }

    private Range getByBounds(long from, long to) {
        if (to - from + 1 > rangeSize) {
            throw new IllegalArgumentException(
                    String.format(
                            Locale.US,
                            "bounds (%d - %d) must not exceed range size %d",
                            from,
                            to,
                            rangeSize
                    )
            );
        }
        if (from % rangeSize != 0) {
            throw new IllegalArgumentException(
                    String.format(
                            Locale.US,
                            "provided range (%d - %d) must have range size %d step",
                            from,
                            to,
                            rangeSize
                    )
            );
        }
        return Range.getRange(from, from + rangeSize - 1);
    }

}
