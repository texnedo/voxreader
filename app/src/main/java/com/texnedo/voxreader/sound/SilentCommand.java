package com.texnedo.voxreader.sound;

import android.support.annotation.NonNull;

class SilentCommand extends Command {
    private static final CommandCache<SilentCommand> commandsCache = new CommandCache<>();
    private String id;
    private long duration;

    @Override
    public String id() {
        return id;
    }

    @Override
    public boolean waitForCompletion() {
        return true;
    }

    protected SilentCommand(CommandContext context) {
        super(context);
    }

    @Override
    protected void recycleInternal() {
        id = null;
        duration = 0;
        commandsCache.recycle(this);
    }

    protected static Command obtain(@NonNull CommandContext context,
                                        @NonNull String id,
                                        long duration) {

        SilentCommand command = commandsCache.obtain();
        if (command == null) {
            command = new SilentCommand(context);
        }
        command.id = id;
        command.duration = duration;
        return command;
    }

    @Override
    public void run() {
        context.playSilentUtterance(id, duration);
    }
}
