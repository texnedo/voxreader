package com.texnedo.voxreader.net;

import java.net.MalformedURLException;

public class ClientException extends Exception {
    public ClientException(String s) {
        super(s);
    }

    public ClientException(MalformedURLException e) {
        super(e);
    }
}
