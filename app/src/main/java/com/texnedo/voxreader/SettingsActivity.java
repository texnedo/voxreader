package com.texnedo.voxreader;

import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;

import com.texnedo.voxreader.system.Preferences;

import org.androidannotations.annotations.EActivity;


@EActivity
public class SettingsActivity extends PreferenceActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager()
                .beginTransaction()
                .replace(android.R.id.content, new SettingsFragment()).commit();
    }

    public static class SettingsFragment extends PreferenceFragment {
        @Override
        public void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.settings);
            final Preferences settings = VoxReaderApp.getManager().getConfig().getSettings();
            CheckBoxPreference showNotification =
                    (CheckBoxPreference) findPreference(getActivity().getString(R.string.show_current_text_key));
            showNotification.setChecked(settings.showTextInNotification());
            showNotification.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    settings.setShowTextInNotification((Boolean)newValue);
                    return true;
                }
            });

            CheckBoxPreference autoDownload =
                    (CheckBoxPreference) findPreference(getActivity().getString(R.string.auto_download_key));
            autoDownload.setChecked(settings.autoDownloadEnabled());
            autoDownload.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    settings.setAutoDownloadEnabled((Boolean)newValue);
                    return true;
                }
            });

            CheckBoxPreference playNext =
                    (CheckBoxPreference) findPreference(getActivity().getString(R.string.auto_play_next_key));
            playNext.setChecked(settings.playNextAfterCompletion());
            playNext.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    settings.setPlayNextAfterCompletion((Boolean)newValue);
                    return true;
                }
            });
        }
    }
}
