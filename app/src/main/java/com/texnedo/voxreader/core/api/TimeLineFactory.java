package com.texnedo.voxreader.core.api;

import android.content.Context;
import android.support.annotation.NonNull;

import com.texnedo.voxreader.system.ConfigImpl;

public final class TimeLineFactory {
    private static volatile TimeLineManager manager;

    public static TimeLineManager getInstance(@NonNull Context context) {
        if (manager == null) {
            synchronized (TimeLineFactory.class) {
                if (manager == null) {
                    manager = new TimeLineManagerImpl(new ConfigImpl(context));
                }
            }
        }
        return manager;
    }
}
