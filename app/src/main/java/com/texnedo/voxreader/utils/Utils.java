package com.texnedo.voxreader.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Utils {
    private static final String LOG_TAG = "Utils";

    public static Uri resourceToUri (@NonNull Context context, int resID) {
        return Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" +
                context.getResources().getResourcePackageName(resID) + '/' +
                context.getResources().getResourceTypeName(resID) + '/' +
                context.getResources().getResourceEntryName(resID) );
    }

    public static String stringToMD5(@NonNull String str){
        byte[] defaultBytes = str.getBytes();
        try {
            MessageDigest algorithm = MessageDigest.getInstance("MD5");
            algorithm.reset();
            algorithm.update(defaultBytes);
            byte messageDigest[] = algorithm.digest();
            str = bytesToHexString(messageDigest);
        } catch(NoSuchAlgorithmException ex){
            FileLog.e(LOG_TAG, "stringToMD5", ex);
        }
        return str;
    }

    public static String bytesToHexString(byte[] source) {
        StringBuilder hexString = new StringBuilder();
        for (byte aMessageDigest : source) {
            if ((0xFF & aMessageDigest) < 0x10) {
                hexString.append('0');
            }
            hexString.append(Integer.toHexString(0xFF & aMessageDigest));
        }
        return hexString + "";
    }

    public static String getHexString(String source) {
        String result = "";
        try {
            for (int i = 0; i < source.length(); i++) {
                result += Integer.toHexString(source.charAt(i));
            }
        } catch (Exception ignored) {
        }
        return result;
    }

    public static String bundleToString(Bundle bundle) {
        if (bundle == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        for (String key : bundle.keySet()) {
            if (key != null && bundle.get(key) != null) {
                sb.append(key).append("='").append(bundle.get(key).toString()).append("', ");
            }
        }
        return sb.toString();
    }

    public static boolean equals(Object a, Object b) {
        return (a == null) ? (b == null) : a.equals(b);
    }

    public static String getFileExtension(File file) {
        String extension = null;
        int index = file.getName().lastIndexOf(".");
        if (index != -1 && index < file.getName().length()) {
            extension = file.getName().substring(index + 1);
        }
        return extension;
    }
}
