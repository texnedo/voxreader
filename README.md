Android app for people who want to continue reading even if they are walking right now. 

### Features: ###
* Allows to import urls, txt files or plain texts and read them using Google TTS engine.
* Parses html pages to extract main article part.
* Supports language auto detection.
* Builds single scrolling activity without a pagination.
* Analyzes commas and periods in a text to adjust pauses between words.
* Persists it's state and allows to continue reading from the last position.
* Supports custom sounds for breaking text into parts (for example to split a header from a text).  
* Shows current reading text with yellow selection.
* Shows notification for playback control and current text viewing without unlocking a device.