package com.texnedo.voxreader.storage;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import com.texnedo.voxreader.utils.FileLog;

class TimeLineOpenHelper extends SQLiteOpenHelper {
    private static final String LOG_TAG = "TimeOpenHelper";
    private static final String DATABASE_NAME_END = "-timeline";
    static final String TABLE_TIMELINE = "timeline";
    static final int DATABASE_VERSION = 1;
    private static final String DATABASE_CREATE = "CREATE TABLE "
            + TABLE_TIMELINE +
            " ( " + TimeLineModel.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + TimeLineModel.COLUMN_TYPE + " INTEGER, "
            + TimeLineModel.COLUMN_FLAGS + " INTEGER, "
            + TimeLineModel.COLUMN_DURATION + " INTEGER, "
            + TimeLineModel.COLUMN_META + " TEXT, "
            + TimeLineModel.COLUMN_TEXT + " TEXT "
            + " ) ;";
    private final String name;

    public TimeLineOpenHelper(Context context, String name) {
        super(context, getDatabaseFileName(name), null, DATABASE_VERSION, new DatabaseErrorHandler() {
            @Override
            public void onCorruption(SQLiteDatabase dbObj) {
                FileLog.e(LOG_TAG, "database corrupted %s", dbObj);
            }
        });
        this.name = name;
        FileLog.d(LOG_TAG, "database file %s opened", name);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE);
        FileLog.d(LOG_TAG, "database %s created", name);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        FileLog.d(LOG_TAG, "database upgraded %s from %d to %d", db, oldVersion, newVersion);
    }

    public static String getDatabaseFileName(@NonNull String name) {
        return name + DATABASE_NAME_END;
    }
}
