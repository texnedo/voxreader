package com.texnedo.voxreader.core.timeline;

import android.support.annotation.NonNull;

import com.texnedo.voxreader.core.api.TextToSpeechProvider;
import com.texnedo.voxreader.storage.TimeLineModel;

public class HeaderItem extends BaseTimeLineItem {
    protected HeaderItem(@NonNull TimeLineModel model) {
        super(model);
    }

    @Override
    public String getText() {
        return null;
    }

    @Override
    protected void playInternal(@NonNull TextToSpeechProvider provider) {
        provider.playEarcon(getId(), TextToSpeechProvider.EarconType.TITLE_HEADER);
    }

    @Override
    public String toString() {
        return TextToSpeechProvider.EarconType.TITLE_HEADER.name();
    }
}