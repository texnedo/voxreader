package com.texnedo.voxreader.data;

import android.support.annotation.NonNull;

import com.texnedo.voxreader.core.api.DataSource;
import com.texnedo.voxreader.core.api.DataSourceState;
import com.texnedo.voxreader.core.api.TimeLineBuilder;
import com.texnedo.voxreader.core.timeline.BaseTimeLineItem;
import com.texnedo.voxreader.storage.dao.DataSourceModel;
import com.texnedo.voxreader.system.Config;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

public class TextDataSource extends BaseDataSource {
    private final String text;

    public TextDataSource(
            @NonNull Config config, @NonNull DataSourceModel model) {
        super(config, model);
        this.text = model.getContent();
    }

    @Override
    public String getId() {
        return Integer.toString(text.hashCode());
    }

    @Override
    protected DataSourceType getType() {
        return DataSourceType.TEXT;
    }

    @Override
    protected Future<?> startLoadingInternal() {
        final FutureTask<?> task = new FutureTask<>(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                return null;
            }
        });
        config.getDispatcher().post(new Runnable() {
            @Override
            public void run() {
                if (task.isCancelled()) {
                    return;
                }
                TimeLineBuilder builder = getBuilder();
                builder.appendText(text)
                        .complete();
                getStorage().flush();
                setState(DataSourceState.READY);
                task.run();
            }
        });
        return task;
    }
}
