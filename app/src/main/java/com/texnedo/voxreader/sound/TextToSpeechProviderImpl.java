package com.texnedo.voxreader.sound;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.texnedo.voxreader.R;
import com.texnedo.voxreader.core.api.TextToSpeechProviderBase;
import com.texnedo.voxreader.system.Config;
import com.texnedo.voxreader.utils.FileLog;

import java.util.HashMap;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public class TextToSpeechProviderImpl extends TextToSpeechProviderBase {
    private static final String LOG_TAG = "TextToSpeechProvider";
    private static final String WARM_UP_COMMAND_ID = "warm_up_command";
    private static final int PRE_JELLY_BEAN_MAX_UTTERANCE_LENGTH = 4000;
    private static final int CLEAR_UTTERANCE_DURATION = 1;
    private static HashMap<String, Locale> localeHashMap;
    private static Set<Locale> locales;
    private final ConcurrentLinkedQueue<Command> pendingCommands = new ConcurrentLinkedQueue<>();
    private final ConcurrentHashMap<String, Command> lookupCommands = new ConcurrentHashMap<>();
    private final CommandContext commandContext = new CommandContextImpl();
    private final ProcessRunnable processRunnable = new ProcessRunnable();
    private final Config config;
    private volatile boolean engineInitialized = false;
    private volatile boolean engineReady = false;
    private volatile String lastPlayingUtteranceId = null;
    private volatile TextToSpeech engine;
    private MediaPlayer musicPlayer;

    public TextToSpeechProviderImpl(Config config) {
        this.config = config;
        startCommand(Command.obtainSilentCommand(commandContext, WARM_UP_COMMAND_ID, 0));
    }

    @Override
    public void playUtterance(final @NonNull String id, final @NonNull String utterance) {
        startCommand(Command.obtainPlayCommand(commandContext, id, utterance));
    }

    @Override
    public void playUtterance(final @NonNull String id, final @NonNull String utterance, final SpeechVolume volume) {
        startCommand(Command.obtainPlayCommand(commandContext, id, utterance, volume));
    }

    @Override
    public void playSilentUtterance(final @NonNull String id, final long duration) {
        startCommand(Command.obtainSilentCommand(commandContext, id, duration));
    }

    @Override
    public void playMusicQueued(@NonNull String id, @NonNull Uri resource, boolean loop) {
        startCommand(Command.obtainMusicCommand(commandContext, id, resource, loop));
    }

    @Override
    public void playMusic(@NonNull Uri resource, boolean loop) {
        playMusicInternal(null, resource, loop);
    }

    @Override
    public boolean isSpeaking() {
        return isReady() && (getEngine().isSpeaking() || !pendingCommands.isEmpty());
    }

    @Override
    public boolean isReady() {
        return engineReady && engineInitialized;
    }

    @Override
    public void setSpeechRate(final SpeechRate rate) {
        startCommand(Command.obtainSpeechRateCommand(commandContext, getRate(rate)));
    }

    @Override
    public void setPitch(final SpeechPitch pitch) {
        startCommand(Command.obtainPitchCommand(commandContext, getPitch(pitch)));
    }

    @Override
    public void playEarcon(@NonNull String id, EarconType type) {
        startCommand(Command.obtainEarconCommand(commandContext, id, type));
    }

    @Override
    public void shutdown() {
        FileLog.v(LOG_TAG, "tts engine %s shutdown requested", engine);
        pendingCommands.clear();
        lookupCommands.clear();
        resetEngine();
        FileLog.v(LOG_TAG, "tts engine shutdown completed");
    }

    @Override
    public void clear() {
        FileLog.v(
                LOG_TAG,
                "clear tts engine %s commands queue (size = %s) requested",
                engine,
                pendingCommands.size()
        );
        pendingCommands.clear();
        lookupCommands.clear();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getEngine().playSilentUtterance(CLEAR_UTTERANCE_DURATION, TextToSpeech.QUEUE_FLUSH, null);
        } else {
            //noinspection deprecation
            getEngine().playSilence(CLEAR_UTTERANCE_DURATION, TextToSpeech.QUEUE_FLUSH, null);
        }
    }

    @Override
    public void stopMusic() {
        FileLog.v(LOG_TAG, "stop music requested (playing %s)", musicPlayer != null);
        if (musicPlayer != null) {
            musicPlayer.stop();
            musicPlayer = null;
        }
    }

    @Override
    public int getMaxSpeechInputLength() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            return TextToSpeech.getMaxSpeechInputLength();
        } else {
            return PRE_JELLY_BEAN_MAX_UTTERANCE_LENGTH;
        }
    }

    @Override
    public void setLanguage(@NonNull Locale locale) {
        startCommand(Command.obtainLanguageCommand(commandContext, locale));
    }

    @Override
    public String getCurrentUtteranceId() {
        return lastPlayingUtteranceId;
    }

    private void startCommand(final Command command) {
        queueCommand(command);
        processCommands();
    }

    private void initEarcon(EarconType type) {
        switch (type) {
            case TITLE_HEADER: {
                getEngine().addEarcon(type.name(), "com.texnedo.voxreader", R.raw.typewriter_bell);
                break;
            }
            case TEXT_END: {
                getEngine().addEarcon(type.name(), "com.texnedo.voxreader", R.raw.typewriter_text_end);
                break;
            }
            default:{
                throw new IllegalArgumentException(
                        String.format("Unknown earcon type %s detected", type)
                );
            }
        }
    }

    private void playUtteranceInternal(@NonNull String id, @NonNull String utterance, @Nullable SpeechVolume volume) {
        FileLog.v(LOG_TAG, "play utterance id %s, text %s, volume %s", id, utterance, volume);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getEngine().speak(utterance, TextToSpeech.QUEUE_ADD, volume == null ? null : getParamsBundle(volume), id);
        } else {
            //noinspection deprecation
            getEngine().speak(utterance, TextToSpeech.QUEUE_ADD, volume == null ? getParamsMap(id) : getParamsMap(id, volume));
        }
    }

    private void playSilentUtteranceInternal(@NonNull String id, long duration) {
        FileLog.v(LOG_TAG, "play silent utterance id %s, duration %d", id, duration);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getEngine().playSilentUtterance(duration, TextToSpeech.QUEUE_ADD, id);
        } else {
            //noinspection deprecation
            getEngine().playSilence(duration, TextToSpeech.QUEUE_ADD, getParamsMap(id));
        }
    }

    private void playEarconInternal(@NonNull String id, EarconType type) {
        FileLog.v(LOG_TAG, "play earcon id %s, type %s", id, type);
        initEarcon(type);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getEngine().playEarcon(type.name(), TextToSpeech.QUEUE_ADD, getIdBundle(id), id);
        } else {
            //noinspection deprecation
            getEngine().playEarcon(type.name(), TextToSpeech.QUEUE_ADD, getParamsMap(id));
        }
    }

    private void playMusicInternal(final @Nullable String id,
                                   final @NonNull Uri resource,
                                   boolean loop) {
        FileLog.e(LOG_TAG, "start to play music id %s uri %s", id, resource);
        if (musicPlayer != null) {
            musicPlayer.stop();
        }
        musicPlayer = MediaPlayer.create(config.getContext(), resource);
        musicPlayer.setLooping(loop);
        if (musicPlayer == null) {
            FileLog.e(LOG_TAG, "failed to play music id %s uri %s", id, resource);
            if (listener != null && id != null) {
                listener.onUtteranceError(id);
            }
            return;
        }
        musicPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                FileLog.v(LOG_TAG, "play music completed id %s, uri %s", id, resource);
                if (listener != null && id != null) {
                    listener.onUtteranceDone(id);
                }
            }
        });
        musicPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                FileLog.e(LOG_TAG, "failed to play music id %s, uri %s (what %d, extra %d)", id, resource, what, extra);
                if (listener != null && id != null) {
                    listener.onUtteranceError(id);
                }
                return false;
            }
        });
        musicPlayer.start();
    }

    private TextToSpeech getEngine() {
        if (engine == null) {
            synchronized (this) {
                if (engine == null) {
                    engine = new TextToSpeech(config.getContext(), new TextToSpeech.OnInitListener() {
                        @Override
                        public void onInit(int status) {
                            if (status == TextToSpeech.SUCCESS) {
                                FileLog.d(LOG_TAG, "engine %s initialization completed (pending commands: %d)", engine, pendingCommands.size());
                                engine.setLanguage(config.getCurrentLocale());
                                engineInitialized = true;
                                processCommands();
                            } else {
                                FileLog.e(LOG_TAG, "engine %s initialization failed", engine);
                                if (listener != null) {
                                    listener.onError(FailReason.INIT_FAILURE);
                                }
                            }
                        }
                    });
                    FileLog.d(LOG_TAG, "engine %s instance created for %s", engine, this);
                    engine.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                        @Override
                        public void onStart(String utteranceId) {
                            if (isWarmUpCommand(utteranceId)) {
                                FileLog.v(LOG_TAG, "engine %s ready (pending commands: %d)", engine, pendingCommands.size());
                                engineReady = true;
                                if (listener != null) {
                                    listener.onReady();
                                }
                            } else {
                                FileLog.v(LOG_TAG, "engine %s utterance id %s started", engine, utteranceId);
                                lastPlayingUtteranceId = utteranceId;
                                if (listener != null) {
                                    listener.onUtteranceStart(utteranceId);
                                }
                            }
                        }

                        @Override
                        public void onDone(String utteranceId) {
                            FileLog.v(LOG_TAG, "engine %s utterance id %s done", engine, utteranceId);
                            lastPlayingUtteranceId = null;
                            removeCommand(utteranceId);
                            processCommands();
                            if (listener != null && !isWarmUpCommand(utteranceId)) {
                                listener.onUtteranceDone(utteranceId);
                            }
                        }

                        @Override
                        public void onError(String utteranceId) {
                            FileLog.e(LOG_TAG, "engine %s utterance id %s error", engine, utteranceId);
                            lastPlayingUtteranceId = null;
                            removeCommand(utteranceId);
                            processCommands();
                            if (listener != null) {
                                if (isWarmUpCommand(utteranceId)) {
                                    listener.onError(FailReason.INIT_FAILURE);
                                } else {
                                    listener.onUtteranceError(utteranceId);
                                }
                            }
                        }
                    });
                }
            }
        }
        return engine;
    }

    private void resetEngine() {
        if (engine != null) {
            getEngine().setOnUtteranceProgressListener(null);
            getEngine().shutdown();
            engine = null;
        }
        lastPlayingUtteranceId = null;
        engineInitialized = false;
        engineReady = false;
    }

    private boolean isWarmUpCommand(String utteranceId) {
        return TextUtils.equals(utteranceId, WARM_UP_COMMAND_ID);
    }

    private void removeCommand(String utteranceId) {
        Command command = lookupCommands.remove(utteranceId);
        if (command == null) {
            throw new IllegalArgumentException(String.format("No such command with id %s", utteranceId));
        }
        command.recycle();
    }

    private void queueCommand(Command command) {
        pendingCommands.offer(command);
        if (command.id() != null) {
            if (lookupCommands.put(command.id(), command) != null) {
                throw new IllegalArgumentException(String.format("Command id %s duplication detected", command.id()));
            }
        }
    }

    private void processCommands() {
       config.getDispatcher().post(processRunnable);
    }

    private static HashMap<String, String> getParamsMap(@NonNull String id) {
        HashMap<String, String> params = new HashMap<>();
        params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, id);
        return params;
    }

    private static Bundle getParamsBundle(SpeechVolume volume) {
        Bundle bundle = new Bundle();
        bundle.putFloat(TextToSpeech.Engine.KEY_PARAM_VOLUME, getVolume(volume));
        return bundle;
    }

    private static Bundle getIdBundle(@NonNull String id) {
        Bundle bundle = new Bundle();
        bundle.putString(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, id);
        return bundle;
    }

    private static HashMap<String, String> getParamsMap(@NonNull String id, SpeechVolume volume) {
        HashMap<String, String> params = getParamsMap(id);
        params.put(TextToSpeech.Engine.KEY_PARAM_VOLUME, Float.toString(getVolume(volume)));
        return params;
    }

    private static float getVolume(SpeechVolume volume) {
        switch (volume) {
            case LOW:
                return 0.2f;
            case NORMAL:
                return 0.9f;
            case HIGH:
                return 1.0f;
            default:
                throw new IllegalArgumentException(String.format("Unknown volume type %s detected", volume));
        }
    }

    private static float getPitch(SpeechPitch pitch) {
        switch (pitch) {
            case LOW:
                return 0.5f;
            case NORMAL:
                return 1.0f;
            case HIGH:
                return 2.0f;
            default:
                throw new IllegalArgumentException(String.format("Unknown pitch type %s detected", pitch));
        }
    }

    private static float getRate(SpeechRate rate) {
        switch (rate) {
            case LOW:
                return 0.5f;
            case NORMAL:
                return 1.0f;
            case FAST:
                return 2.0f;
            default:
                throw new IllegalArgumentException(String.format("Unknown rate type %s detected", rate));
        }
    }

    private Locale getLocaleByLanguage(@NonNull Iterable<Locale> availableLocales,
                                       @NonNull String language) {
        if (localeHashMap == null) {
            localeHashMap = new HashMap<>();
            for (Locale item : availableLocales) {
                localeHashMap.put(item.getLanguage(), item);
            }
        }
        return localeHashMap.get(language);
    }

    private Locale getLocaleByLanguage(@NonNull Locale[] availableLocales,
                                       @NonNull String language) {
        if (localeHashMap == null) {
            localeHashMap = new HashMap<>();
            for (Locale item : availableLocales) {
                localeHashMap.put(item.getLanguage(), item);
            }
        }
        return localeHashMap.get(language);
    }

    private Locale getEngineLocale(@NonNull Locale locale) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (locales == null) {
                locales = getEngine().getAvailableLanguages();
            }
            if (locales.contains(locale)) {
                return locale;
            }
            return getLocaleByLanguage(locales, locale.getLanguage());
        } else {
            Locale systemLocale = getLocaleByLanguage(Locale.getAvailableLocales(), locale.getLanguage());
            return getEngine().isLanguageAvailable(systemLocale) != TextToSpeech.LANG_NOT_SUPPORTED ?
                    systemLocale : null;
        }
    }

    private final class CommandContextImpl implements CommandContext {
        @Override
        public void playUtterance(@NonNull String id, @NonNull String utterance, @Nullable SpeechVolume volume) {
            playUtteranceInternal(id, utterance, volume);
        }

        @Override
        public void playSilentUtterance(@NonNull String id, long duration) {
            playSilentUtteranceInternal(id, duration);
        }

        @Override
        public void setSpeechRate(float rate) {
            FileLog.v(LOG_TAG, "speech rate type %s", rate);
            getEngine().setSpeechRate(rate);
        }

        @Override
        public void setPitch(float pitch) {
            FileLog.v(LOG_TAG, "pitch type %s", pitch);
            getEngine().setPitch(pitch);
        }

        @Override
        public void playEarcon(@NonNull String id, EarconType type) {
            playEarconInternal(id, type);
        }

        @Override
        public void playMusic(@NonNull String id, @NonNull Uri resource, boolean loop) {
            playMusicInternal(id, resource, loop);
        }

        @Override
        public void setLanguage(@NonNull Locale locale) {
            Locale engineLocale = getEngineLocale(locale);
            Locale current;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                current = getEngine().getVoice().getLocale();
            } else {
                current = getEngine().getLanguage();
            }
            FileLog.d(LOG_TAG, "set locale %s, current locale %s", engineLocale, current);
            if (current != engineLocale) {
                getEngine().setLanguage(engineLocale);
            }
        }
    }

    private final class ProcessRunnable implements Runnable {
        @Override
        public void run() {
            if (!engineInitialized) {
                getEngine();
            } else {
                while (!pendingCommands.isEmpty()) {
                    Command command = pendingCommands.poll();
                    command.run();
                    if (command.waitForCompletion()) {
                        break;
                    }
                }
            }
        }
    }
}
