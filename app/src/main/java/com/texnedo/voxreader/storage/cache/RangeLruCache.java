package com.texnedo.voxreader.storage.cache;

import android.util.LruCache;

import com.texnedo.voxreader.core.timeline.TimeLineItem;

import java.util.List;

class RangeLruCache extends LruCache<Range, List<TimeLineItem>> {
    public RangeLruCache(int maxSize) {
        super(maxSize);
    }

    @Override
    protected void entryRemoved(boolean evicted,
                                Range key,
                                List<TimeLineItem> oldValue,
                                List<TimeLineItem> newValue) {
        super.entryRemoved(evicted, key, oldValue, newValue);
    }
}
