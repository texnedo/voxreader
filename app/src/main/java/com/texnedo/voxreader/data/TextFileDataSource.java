package com.texnedo.voxreader.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.texnedo.voxreader.core.api.DataSourceState;
import com.texnedo.voxreader.core.api.TimeLineBuilder;
import com.texnedo.voxreader.storage.dao.DataSourceModel;
import com.texnedo.voxreader.system.Config;
import com.texnedo.voxreader.utils.FileLog;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.Future;

public class TextFileDataSource extends BaseDataSource {
    private static final String LOG_TAG = "TextFileDataSource";

    protected TextFileDataSource(@NonNull Config config, @NonNull DataSourceModel model) {
        super(config, model);
    }

    @Override
    protected DataSourceType getType() {
        return DataSourceType.TEXT_FILE;
    }

    @Override
    protected Future<?> startLoadingInternal() {
        return config.getNetworkExecutor().submit(new Runnable() {
            @Override
            public void run() {
                InputStreamReader reader = null;
                try {
                    TimeLineBuilder builder = getBuilder();
                    ContentResolver cr = config.getContext().getContentResolver();
                    InputStream inputStream = cr.openInputStream(Uri.parse(getModel().getContent()));
                    if (inputStream == null) {
                        setState(DataSourceState.FAILED);
                        FileLog.e(LOG_TAG, "failed to open file %s", getModel().getContent());
                        return;
                    }
                    reader = new InputStreamReader(inputStream);
                    char[] buffer = new char[1024];
                    while (reader.ready()) {
                        int read = reader.read(buffer);
                        if (read != -1) {
                            builder.appendText(new String(buffer));
                        }
                    }
                    builder.complete();
                    getStorage().flush();
                    setState(DataSourceState.READY);
                } catch (Exception ex) {
                    setState(DataSourceState.FAILED);
                    FileLog.e(LOG_TAG, ex, "failed to extract text from file %s", getModel().getContent());
                } finally {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (IOException ex) {
                            FileLog.e(LOG_TAG, ex, "failed to close file file %s", getModel().getContent());
                        }
                    }
                }
            }
        });
    }

    @Override
    public String getId() {
        return Integer.toString(getModel().getContent().hashCode());
    }
}
