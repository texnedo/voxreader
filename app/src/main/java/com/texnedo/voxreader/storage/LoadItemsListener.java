package com.texnedo.voxreader.storage;

import com.texnedo.voxreader.storage.dao.DataSourceModel;

import java.util.List;

public interface LoadItemsListener {
    void onComplete(List<DataSourceModel> list);

    void onFailure();
}
