package com.texnedo.voxreader.core.api;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.texnedo.voxreader.core.timeline.BaseTimeLineItem;
import com.texnedo.voxreader.storage.TimeLineModelFactory;
import com.texnedo.voxreader.system.Config;
import com.texnedo.voxreader.utils.FileLog;

import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;

import java.util.HashSet;
import java.util.Set;


public class TimeLineBuilderImpl implements TimeLineBuilder {
    private static final String LOG_TAG = "TimeLineBuilder";
    private static final int MAX_ITEM_LENGTH = 150;
    private static final char PERIOD = '.';
    private static final char QUESTION_MARK = '?';
    private static final char EXCLAMATION_MARK = '!';
    private static final char COMMA = ',';
    private static final char COLON = ':';
    private static final char SEMICOLON = ';';
    private static final char NEWLINE = '\n';
    private static final String NEWLINE_STR = "\n";
    private static final char SPACE = ' ';
    private static final Set<Character> SEPARATORS;
    static {
        //TODO - use an array instated of a hash map
        SEPARATORS = new HashSet<>();
        SEPARATORS.add(PERIOD);
        SEPARATORS.add(COMMA);
        SEPARATORS.add(COLON);
        SEPARATORS.add(SEMICOLON);
        SEPARATORS.add(NEWLINE);
        SEPARATORS.add(QUESTION_MARK);
        SEPARATORS.add(EXCLAMATION_MARK);
    }
    private final Config config;
    private final TimeLineModelFactory factory;
    private final Receiver receiver;
    private final State state = new State();
    private final StringBuilder builder = new StringBuilder(MAX_ITEM_LENGTH);

    public TimeLineBuilderImpl(@NonNull Config config,
                               @NonNull TimeLineModelFactory factory,
                               @NonNull Receiver receiver) {
        this.config = config;
        this.factory = factory;
        this.receiver = receiver;
    }

    @Override
    public TimeLineBuilder appendChar(char c) {
        appendText(Character.toString(c));
        return this;
    }

    @Override
    public TimeLineBuilder appendText(@NonNull String text) {
        int lastItemStart = 0;
        int lastItemLength = builder.length();
        builder.append(text);
        for (int i = lastItemLength; i < builder.length(); ++i, ++lastItemLength) {
            char current = builder.charAt(i);
            if (current == NEWLINE) {
                state.hasNewLine = true;
            }
            if ((lastItemLength > MAX_ITEM_LENGTH && current == SPACE)
                    || SEPARATORS.contains(current)) {
                String item = builder.substring(lastItemStart, i + 1);
                if (state.hasNewLine) {
                    item = item.replace(NEWLINE_STR, "");
                    state.hasNewLine = false;
                }
                notifyCompleted(BaseTimeLineItem.createSimpleTextItem(factory, item));
                lastItemLength = 0;
                lastItemStart = i + 1;
            }
            processSeparator(current);
        }
        builder.replace(0, lastItemStart, "");
        return this;
    }

    @Override
    public TimeLineBuilder appendText(@NonNull String text, TextImportance importance) {
        //TODO - support param
        return appendText(text);
    }

    @Override
    public TimeLineBuilder appendParagraph() {
        completeLastText();
        notifyCompleted(BaseTimeLineItem.createParagraphItem(factory));
        return this;
    }

    @Override
    public TimeLineBuilder appendHeader() {
        completeLastText();
        notifyCompleted(BaseTimeLineItem.createHeaderItem(factory));
        return this;
    }

    @Override
    public TimeLineBuilder appendImage(@NonNull String url, @Nullable String description) {
        notifyCompleted(BaseTimeLineItem.createImageItem(factory, url, description));
        return this;
    }

    @Override
    public TimeLineBuilder appendLink(@NonNull String url, @Nullable String description) {
        notifyCompleted(BaseTimeLineItem.createLinkItem(factory, url, description));
        return this;
    }

    @Override
    public TimeLineBuilder appendNode(@NonNull Node node) {
        if (node instanceof TextNode) {
            appendText(((TextNode) node).text());
        } else {
            completeLastText();
            switch (node.nodeName()) {
                case "p": {
                    appendParagraph();
                    break;
                }
                case "a": {
                    String link = node.attr("href");
                    StringBuilder builder = new StringBuilder();
                    for (Node child : node.childNodes()) {
                        if (child instanceof TextNode) {
                            builder.append(((TextNode)child).text());
                        }
                    }
                    appendLink(link, builder.toString());
                    break;
                }
                case "img": {
                    String link = node.attr("src");
                    String description = node.attr("alt");
                    appendImage(link, description);
                    break;
                }
                case "h1":
                case "h2":
                case "h3": {
                    appendHeader();
                    break;
                }
                default: {
                    break;
                }
            }
        }
        return null;
    }

    @Override
    public void complete() {
        completeLastText();
        appendTextEnd();
        receiver.onCompleted();
    }

    private static class State {
        boolean hasNewLine = false;
    }

    private void processSeparator(char separator) {
        if (separator == PERIOD || separator == QUESTION_MARK || separator == EXCLAMATION_MARK) {
            notifyCompleted(BaseTimeLineItem.createPeriodItem(factory));
        } else if (separator == COMMA || separator == SEMICOLON || separator == COLON) {
            notifyCompleted(BaseTimeLineItem.createCommaItem(factory));
        } else if (separator == NEWLINE) {
            notifyCompleted(BaseTimeLineItem.createParagraphItem(factory));
        }
    }

    private void completeLastText() {
        if (builder.length() != 0) {
            String item = builder.toString();
            if (state.hasNewLine) {
                item = item.replace(NEWLINE_STR, "");
                state.hasNewLine = false;
            }
            notifyCompleted(BaseTimeLineItem.createSimpleTextItem(factory, item));
            builder.setLength(0);
        }
    }

    private void notifyCompleted(@NonNull BaseTimeLineItem item) {
        FileLog.v(LOG_TAG, "extracted new timeline item %d", item.getNumber());
        receiver.onComplete(item);
    }

    private void appendTextEnd() {
        completeLastText();
        notifyCompleted(BaseTimeLineItem.createTextEndItem(factory));
    }
}
