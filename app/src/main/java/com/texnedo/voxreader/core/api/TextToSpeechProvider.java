package com.texnedo.voxreader.core.api;

import android.net.Uri;
import android.support.annotation.NonNull;

import java.util.Locale;

public interface TextToSpeechProvider {
    void playUtterance(@NonNull String id, @NonNull String utterance);

    void playUtterance(@NonNull String id, @NonNull String utterance, SpeechVolume volume);

    void playSilentUtterance(@NonNull String id, long duration);

    void playMusicQueued(@NonNull String id, @NonNull Uri resource, boolean loop);

    void playMusic(@NonNull Uri resource, boolean loop);

    boolean isSpeaking();

    boolean isReady();

    void setSpeechRate(SpeechRate rate);

    void setPitch(SpeechPitch pitch);

    void playEarcon(@NonNull String id, EarconType type);

    void shutdown();

    void clear();

    void stopMusic();

    int getMaxSpeechInputLength();

    void setStateListener(StateListener listener);

    void setLanguage(@NonNull Locale locale);

    String getCurrentUtteranceId();

    enum SpeechRate {
        LOW,
        NORMAL,
        FAST
    }

    enum SpeechPitch {
        LOW,
        NORMAL,
        HIGH
    }

    enum SpeechVolume {
        LOW,
        NORMAL,
        HIGH
    }

    enum EarconType {
        TITLE_HEADER,
        TEXT_END,
        UNKNOWN
    }

    enum FailReason {
        OK,
        INIT_FAILURE,
        GENERAL
    }

    interface StateListener {
        void onReady();

        void onUtteranceStart(@NonNull String id);

        void onUtteranceDone(@NonNull String id);

        void onUtteranceError(@NonNull String id);

        void onError(FailReason reason);
    }
}
