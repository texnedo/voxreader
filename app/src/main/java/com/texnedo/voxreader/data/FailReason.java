package com.texnedo.voxreader.data;

enum FailReason {
    NETWORK_ERROR,
    SERVER_ERROR,
    GENERAL_ERROR
}
