package com.texnedo.voxreader.ui.notifications;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;

import com.texnedo.voxreader.R;
import com.texnedo.voxreader.core.api.TimeLineAccess;
import com.texnedo.voxreader.system.Preferences;

public class PlaybackNotification extends NotificationBase {
    private final Preferences preferences;
    private final TimeLineAccess dataSource;
    private NotificationCompat.Builder builder;

    public PlaybackNotification(@NonNull Context context,
                                @NonNull Preferences preferences,
                                @NonNull TimeLineAccess dataSource) {
        super(context);
        this.preferences = preferences;
        this.dataSource = dataSource;
    }

    @Override
    public NotificationId getId() {
        return NotificationId.PLAYBACK;
    }

    @Override
    public String getTag() {
        return dataSource.getId();
    }

    @Override
    protected NotificationCompat.Builder getBuilder() {
        if (builder == null){
            builder = new NotificationCompat.Builder(context);
            fillBuilder(builder);
        }
        return builder;
    }

    @Override
    protected void fillBuilder(NotificationCompat.Builder builder) {
        super.fillBuilder(builder);
        builder.setSmallIcon(R.drawable.ic_volume_down_white_36dp);
        builder.setAutoCancel(false);
        builder.setContentTitle(dataSource.getName());
        if (preferences.showTextInNotification() && dataSource.isPlaying()) {
            builder.setContentText(dataSource.getPlayingText());
        } else {
            builder.setContentText(dataSource.getDescription());
        }
        builder.setTicker(dataSource.getName());
        if (dataSource.isPlaying()) {
            builder.addAction(
                    R.drawable.ic_pause_white_36dp,
                    context.getString(R.string.pause_playback_text),
                    getPauseIntent(context, dataSource)
            );
            builder.addAction(
                    R.drawable.ic_skip_next_white_36dp,
                    context.getString(R.string.skip_playback_text),
                    getSkipIntent(context, dataSource)
            );
        } else {
            builder.addAction(
                    R.drawable.ic_play_arrow_white_36dp,
                    context.getString(R.string.speak_playback_text),
                    getPlayIntent(context, dataSource)
            );
            builder.addAction(
                    R.drawable.ic_stop_white_36dp,
                    context.getString(R.string.stop_playback_text),
                    getStopIntent(context, dataSource)
            );
        }

        builder.addAction(
                R.drawable.ic_queue_play_next_white_36dp,
                context.getString(R.string.next_source_playback_text),
                getNextIntent(context, dataSource)
        );
        builder.setDeleteIntent(getDeleteIntent(context, dataSource));
        builder.setContentIntent(getTimeLineIntent(context, dataSource));
        if (preferences.showTextInNotification() && dataSource.isPlaying()) {
            builder.setStyle(
                    new NotificationCompat.BigTextStyle().bigText(dataSource.getPlayingText())
            );
        } else {
            builder.setStyle(
                    new NotificationCompat.BigTextStyle().bigText(dataSource.getDescription())
            );
        }
    }

    @Override
    public boolean isSilent() {
        return true;
    }


    @Override
    public boolean shouldReplacePrevious() {
        return false;
    }
}
