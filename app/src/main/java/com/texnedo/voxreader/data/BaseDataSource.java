package com.texnedo.voxreader.data;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.texnedo.voxreader.core.api.DataSource;
import com.texnedo.voxreader.core.api.DataSourceState;
import com.texnedo.voxreader.core.api.LanguageDetector;
import com.texnedo.voxreader.core.api.TextToSpeechProvider;
import com.texnedo.voxreader.core.api.TimeLineBuilder;
import com.texnedo.voxreader.core.timeline.BaseTimeLineItem;
import com.texnedo.voxreader.core.timeline.TimeLineItem;
import com.texnedo.voxreader.storage.ItemFactory;
import com.texnedo.voxreader.storage.ItemsCountChangedListener;
import com.texnedo.voxreader.storage.ItemsCountReceiver;
import com.texnedo.voxreader.storage.ItemsReceiver;
import com.texnedo.voxreader.storage.TimeLineModel;
import com.texnedo.voxreader.storage.TimeLineStorage;
import com.texnedo.voxreader.storage.dao.DataSourceModel;
import com.texnedo.voxreader.system.Config;
import com.texnedo.voxreader.utils.DebugUtils;
import com.texnedo.voxreader.utils.FileLog;
import com.texnedo.voxreader.utils.Utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.Future;

public abstract class BaseDataSource implements DataSource {
    private static final int MAX_SCREEN_NAME_LENGTH = 100;
    private static final int NOTIFY_STATE_CHANGE_TIMEOUT = 200;
    private static final String LOG_TAG = "DataSource";
    private final ItemFactory itemFactory = new ItemFactoryImpl();
    protected final Config config;
    private final DataSourceModel model;
    private final HashSet<StateChangedListener> listeners = new HashSet<>();
    private final Runnable notifyStateChange = new NotifyChangeRunnable();
    private volatile TimeLineStorage storage;
    private volatile Future<?> loading;
    private volatile TextToSpeechProvider tts;
    private volatile DataSourceState state;
    private volatile long lastTtsReadId;

    protected BaseDataSource(@NonNull Config config, @NonNull DataSourceModel model) {
        this.config = config;
        this.model = model;
        this.state = DataSourceState.from(model.getState());
    }

    public static BaseDataSource create(@NonNull Config config, @NonNull DataSourceModel model) {
        if (model.getType() == DataSourceType.HTML.getTypeId()) {
            return new HtmlDataSource(config, model);
        } else if (model.getType() == DataSourceType.TEXT.getTypeId()) {
            return new TextDataSource(config, model);
        } else if (model.getType() == DataSourceType.TEXT_FILE.getTypeId()) {
            return new TextFileDataSource(config, model);
        } else {
            throw new IllegalArgumentException();
        }
    }

    public static BaseDataSource createTextDataSource(@NonNull Config config,
                                                        @NonNull String text) {
        DataSourceModel model = new DataSourceModel();
        model.setType(DataSourceType.TEXT.getTypeId());
        model.setContent(text);
        model.setName(getName(text));
        return new TextDataSource(config, model);
    }

    public static BaseDataSource createHtmlDataSource(@NonNull Config config,
                                                        @NonNull Uri uri) {
        DataSourceModel model = new DataSourceModel();
        model.setType(DataSourceType.HTML.getTypeId());
        model.setContent(uri.toString());
        model.setName(uri.toString());
        return new HtmlDataSource(config, model);
    }

    public static BaseDataSource createTextFileDataSource(@NonNull Config config,
                                                      @NonNull Uri uri) {
        DataSourceModel model = new DataSourceModel();
        model.setType(DataSourceType.TEXT_FILE.getTypeId());
        model.setContent(uri.toString());
        model.setName(uri.toString());
        return new TextFileDataSource(config, model);
    }

    private static String getName(@NonNull String text) {
        return text.length() > MAX_SCREEN_NAME_LENGTH ?
                text.substring(0, MAX_SCREEN_NAME_LENGTH) :
                text;
    }

    protected abstract DataSourceType getType();

    protected abstract Future<?> startLoadingInternal();

    private void checkStorage() {
        config.getDispatcher().post(new Runnable() {
            @Override
            public void run() {
                switch (state) {
                    case PREPARING:
                    case INITIAL: {
                        if (loading == null) {
                            synchronized (this) {
                                if (loading == null) {
                                    if (!getStorage().exists()) {
                                        setState(DataSourceState.PREPARING);
                                        loading = startLoadingInternal();
                                    } else {
                                        //TODO - handle faults during the preparation process
                                        setState(DataSourceState.READY);
                                    }
                                }
                            }
                        }
                        break;
                    }
                    case FAILED: {
                        //TODO - show notification
                        break;
                    }
                    case READY: {
                        break;
                    }
                    case PLAYING:
                    case PAUSED: {
                        if (!getStorage().exists()) {
                            FileLog.e(LOG_TAG, "storage not found on state %s", state);
                            setState(DataSourceState.FAILED);
                            return;
                        }
                        final long startFrom = getModel().getLastReadId() == null ? 0 : getModel().getLastReadId();
                        if (getStorage().getItem(startFrom) == null) {
                            getStorage().loadItemsRange(startFrom, new ItemsReceiver() {
                                @Override
                                public void onComplete(@NonNull final List<TimeLineItem> items) {
                                    config.getDispatcher().post(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (items.isEmpty()) {
                                                FileLog.e(LOG_TAG, "no such time line item with id %d in storage", startFrom);
                                                setState(DataSourceState.FAILED);
                                                return;
                                            }
                                            int diff = (int) (startFrom - items.get(0).getNumber());
                                            FileLog.d(LOG_TAG, "last playing item %s checked", items.get(diff));
                                        }
                                    });
                                }

                                @Override
                                public void onFailure(@NonNull final Exception ex) {
                                    config.getDispatcher().post(new Runnable() {
                                        @Override
                                        public void run() {
                                            FileLog.e(LOG_TAG, "error during storage request", ex);
                                            setState(DataSourceState.FAILED);
                                        }
                                    });
                                }
                            });
                        }
                        break;
                    }
                    default: {
                        throw new IllegalArgumentException();
                    }
                }
            }
        });
    }

    @Override
    public String getName() {
        return getModel().getName();
    }

    @Override
    public String getDescription() {
        return getModel().getDescription();
    }

    @Override
    public boolean isProcessing() {
        DataSourceState current = state;
        return current == DataSourceState.INITIAL || current == DataSourceState.PREPARING;
    }

    @Override
    public boolean isFailed() {
        DataSourceState current = state;
        return current == DataSourceState.FAILED;
    }

    @Override
    public boolean isPlaying() {
        DataSourceState current = state;
        return current == DataSourceState.PLAYING;
    }

    @Override
    public boolean isPaused() {
        DataSourceState current = state;
        return current == DataSourceState.PAUSED;
    }

    @Override
    public DataSourceState getState() {
        return state;
    }

    @Override
    public final DataSourceModel getModel() {
        return model;
    }

    @Override
    public void loadAllItems(@NonNull ItemsReceiver receiver) {
        checkStorage();
        getStorage().loadAllItems(receiver);
    }

    @Override
    public void loadItems(long from, long to, @NonNull ItemsReceiver receiver) {
        checkStorage();
        getStorage().loadItems(from, to, receiver);
    }

    @Override
    public void loadItem(long id, @NonNull ItemsReceiver receiver) {
        checkStorage();
        getStorage().loadItem(id, receiver);
    }

    @Override
    public void loadCount(@NonNull ItemsCountReceiver receiver) {
        checkStorage();
        getStorage().loadCount(receiver);
    }

    @Nullable
    @Override
    public List<TimeLineItem> getItems(long from, long to) {
        checkStorage();
        return getStorage().getItems(from, to);
    }

    @Nullable
    @Override
    public TimeLineItem getItem(long id) {
        checkStorage();
        return getStorage().getItem(id);
    }

    @Override
    public int getRangeSize() {
        return getStorage().getRangeSize();
    }

    @Override
    public void addItemsCountChangedListener(@NonNull ItemsCountChangedListener listener) {
        getStorage().addItemsCountChangedListener(listener);
    }

    @Override
    public void removeItemsCountChangedListener(@NonNull ItemsCountChangedListener listener) {
        getStorage().removeItemsCountChangedListener(listener);
    }

    @Override
    public synchronized void addStateChangedListener(@NonNull StateChangedListener listener) {
        listeners.add(listener);
    }

    @Override
    public synchronized void removeStateChangedListener(@NonNull StateChangedListener listener) {
        listeners.remove(listener);
    }

    @Override
    public void play() {
        playInternal(0, true);
    }

    @Override
    public void play(long from) {
        playInternal(from, false);
    }

    @Override
    public void pause() {
        config.getDispatcher().post(new Runnable() {
            @Override
            public void run() {
                FileLog.d(LOG_TAG, "pause datasource %s (%s)", getName(), getId());
                getTTS().clear();
                setState(DataSourceState.PAUSED);
            }
        });
    }

    @Override
    public void stop() {
        config.getDispatcher().post(new Runnable() {
            @Override
            public void run() {
                FileLog.d(LOG_TAG, "stop datasource %s (%s)", getName(), getId());
                getTTS().shutdown();
                resetLastReadId();
                setState(DataSourceState.READY);
            }
        });
    }

    @Override
    public void clear() {
        config.getDispatcher().post(new Runnable() {
            @Override
            public void run() {
                FileLog.d(LOG_TAG, "clear datasource %s (%s)", getName(), getId());
                resetLastReadId();
                setState(DataSourceState.INITIAL);
                getTTS().clear();
                cancelLoading();
                getStorage().delete();
            }
        });
    }

    @Override
    public void next() {
        config.getDispatcher().post(new Runnable() {
            @Override
            public void run() {
                getTTS().clear();
                switch (state) {
                    case INITIAL:
                    case PREPARING:
                    case FAILED: {
                        //TODO - fire event and show notification
                        break;
                    }
                    case READY: {
                        playInternal(0, true);
                        break;
                    }
                    case PAUSED:
                    case PLAYING: {
                        long nextId = getModel().getLastReadId() == null ? 0 : getModel().getLastReadId() + 1;
                        playInternal(nextId, false);
                        break;
                    }
                    default: {
                        throw new IllegalArgumentException();
                    }
                }
            }
        });
    }

    @Override
    public void previous() {
        config.getDispatcher().post(new Runnable() {
            @Override
            public void run() {
                getTTS().clear();
                switch (state) {
                    case INITIAL:
                    case PREPARING:
                    case FAILED: {
                        //TODO - fire event and show notification
                        break;
                    }
                    case READY: {
                        break;
                    }
                    case PAUSED:
                    case PLAYING: {
                        long currentId = getModel().getLastReadId() == null ? 0 : getModel().getLastReadId();
                        if (currentId >= 1) {
                            playInternal(--currentId, false);
                        }
                        break;
                    }
                    default: {
                        throw new IllegalArgumentException();
                    }
                }
            }
        });
    }

    @Override
    public String getLanguage() {
        return getModel().getLanguage();
    }

    @Override
    public Long getPlayingId() {
        return getModel().getLastReadId();
    }

    @Override
    public String getPlayingText() {
        Long lastReadId = getModel().getLastReadId();
        if (lastReadId == null) {
            return null;
        }
        TimeLineItem item = getStorage().getItem(lastReadId);
        if (item == null) {
            return null;
        } else {
            return item.getText();
        }
    }

    protected TimeLineStorage getStorage() {
        if (storage == null) {
            synchronized (this) {
                if (storage == null) {
                    storage = config.getStorage(this, itemFactory);
                }
            }
        }
        return storage;
    }

    protected TextToSpeechProvider getTTS() {
        if (tts == null) {
            synchronized (this) {
                if (tts == null) {
                    tts = config.getTTS();
                    tts.setStateListener(new TextToSpeechProvider.StateListener() {
                        @Override
                        public void onReady() {

                        }

                        @Override
                        public void onUtteranceStart(@NonNull final String id) {
                            config.getDispatcher().post(new Runnable() {
                                @Override
                                public void run() {
                                    //TODO - fix this hack
                                    long itemId = Long.parseLong(id);
                                    BaseTimeLineItem item =
                                            (BaseTimeLineItem)getStorage().getItem(itemId);
                                    if (item != null) {
                                        item.setIsPlaying(true);
                                    }
                                    setLastReadId(itemId);
                                }
                            });
                        }

                        @Override
                        public void onUtteranceDone(@NonNull final String id) {
                            config.getDispatcher().post(new Runnable() {
                                @Override
                                public void run() {
                                    //TODO - remove useless long parsings
                                    long itemId = Long.parseLong(id);
                                    BaseTimeLineItem item =
                                            (BaseTimeLineItem)getStorage().getItem(itemId);
                                    if (item != null) {
                                        item.setIsPlaying(false);
                                    }
                                    if (itemId == lastTtsReadId) {
                                        playNextItems(itemId + 1);
                                    }
                                }
                            });
                        }

                        @Override
                        public void onUtteranceError(@NonNull String id) {

                        }

                        @Override
                        public void onError(TextToSpeechProvider.FailReason reason) {
                            config.getDispatcher().post(new Runnable() {
                                @Override
                                public void run() {
                                    FileLog.e(LOG_TAG, "error %s received during playing");
                                    setState(DataSourceState.FAILED);
                                }
                            });
                        }
                    });
                    FileLog.d(LOG_TAG, "tts provider %s created from %s", tts, getId());
                }
            }
        }
        return tts;
    }

    protected final void setState(DataSourceState newState) {
        if (!checkTransition(state, newState)) {
            FileLog.d(LOG_TAG, "no way to switch from %s -> %s", state, newState);
            return;
        }
        if (newState != state) {
            synchronized (this) {
                DataSourceState previousState = state;
                state = newState;
                getModel().setState(state.getValue());
                FileLog.d(LOG_TAG, "state changed (%s -> %s)", previousState, state);
            }
            notifyStateChanged(true);
        }
    }

    protected void setDescription(@NonNull String description) {
        if (!TextUtils.equals(description, getModel().getDescription())) {
            getModel().setDescription(description);
            notifyStateChanged(true);
        }
    }

    private void setLastReadId(Long id) {
        if (getModel().getLastReadId() == null || !Utils.equals(id, getModel().getLastReadId())) {
            getModel().setLastReadId(id);
            notifyStateChanged(false);
        }
    }

    private void resetLastReadId() {
        Long lastReadId = getModel().getLastReadId();
        if (lastReadId == null) {
            return;
        }
        TimeLineItem item = getStorage().getItem(lastReadId);
        if (item != null) {
            item.reset();
        }
        setLastReadId(null);
    }

    private void setLanguage(String language) {
        if (!TextUtils.equals(getModel().getLanguage(), language)) {
            getModel().setLanguage(language);
            notifyStateChanged(false);
        }
    }

    private void notifyStateChanged(boolean force) {
        config.getDispatcher().removeCallbacks(notifyStateChange);
        if (force) {
            notifyStateChange.run();
        } else {
            config.getDispatcher().postDelayed(notifyStateChange, NOTIFY_STATE_CHANGE_TIMEOUT);
        }
    }

    private void playInternal(final long from, final boolean restore) {
        config.getDispatcher().post(new Runnable() {
            @Override
            public void run() {
                FileLog.d(LOG_TAG, "play datasource %s (%s)", getName(), getId());
                if (!getStorage().exists()) {
                    return;
                }
                Long lastReadId = getModel().getLastReadId();
                long playFrom = (lastReadId == null || !restore) ? from : lastReadId;
                switch (state) {
                    case FAILED:
                    case PREPARING:
                    case INITIAL: {
                        //TODO - fire event and show some notification
                        break;
                    }
                    case PAUSED:
                    case READY: {
                        playNextItems(playFrom);
                        break;
                    }
                    case PLAYING: {
                        getTTS().clear();
                        playNextItems(playFrom);
                        break;
                    }
                    default: {
                        throw new IllegalArgumentException();
                    }
                }
            }
        });
    }

    private void playNextItems(long id) {
        FileLog.v(LOG_TAG, "play next items from %d", id);
        if (!TextUtils.isEmpty(getModel().getLanguage())) {
            FileLog.v(LOG_TAG, "set locale %s", getModel().getLanguage());
            getTTS().setLanguage(new Locale(getModel().getLanguage()));
        }
        getStorage().loadItemsRange(id, new ItemsReceiver() {
            @Override
            public void onComplete(@NonNull final List<TimeLineItem> items) {
                config.getDispatcher().post(new Runnable() {
                    @Override
                    public void run() {
                        if (items.isEmpty()) {
                            setLastReadId(null);
                            setState(DataSourceState.READY);
                        } else {
                            lastTtsReadId = items.get(items.size() - 1).getNumber();
                            setState(DataSourceState.PLAYING);
                            for (TimeLineItem item : items) {
                                item.play(getTTS());
                            }
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NonNull Exception ex) {
                FileLog.e(LOG_TAG, "failed to load items for playing", ex);
            }
        });
    }

    protected final void cancelLoading() {
        if (loading != null) {
            synchronized (this) {
                if (loading != null) {
                    loading.cancel(true);
                    loading = null;
                    setState(DataSourceState.INITIAL);
                }
            }
        }
    }

    //TODO - think how to do it more useful
    protected enum DataSourceType {
        HTML(0),
        TEXT(1),
        TEXT_FILE(2);

        private final int value;

        DataSourceType(int value) {
            this.value = value;
        }

        int getTypeId() {
            return value;
        }
    }

    protected TimeLineBuilder getBuilder() {
        return config.getTimelineBuilder(getStorage(), new TimeLineBuilder.Receiver() {
            LanguageDetector detector;
            Locale detected;

            @Override
            public void onComplete(@NonNull BaseTimeLineItem item) {
                getStorage().saveItem(item);
                if (detector == null) {
                    detector = config.getLanguageDetector();
                }
                detector.process(item.getText());
                checkDetectedLocale();
            }

            @Override
            public void onCompleted() {
                if (detector != null && detected == null) {
                    detector.complete();
                    checkDetectedLocale();
                }
            }

            private void checkDetectedLocale() {
                final Locale locale = detector.getDetectedLocale();
                if (locale != null && detected == null) {
                    detected = locale;
                    config.getDispatcher().post(new Runnable() {
                        @Override
                        public void run() {
                            setLanguage(locale.getLanguage());
                        }
                    });
                }
            }
        });
    }

    private static boolean checkTransition(DataSourceState state, DataSourceState newState) {
        if (state == newState) {
            return true;
        }
        switch (newState) {
            case FAILED:
            case INITIAL:
                break;
            case PREPARING: {
                if (state != DataSourceState.INITIAL) {
                    return false;
                }
                break;
            }
            case READY: {
                if (state == DataSourceState.FAILED) {
                    return false;
                }
                break;
            }
            case PLAYING: {
                if (state != DataSourceState.READY && state != DataSourceState.PAUSED) {
                    return false;
                }
                break;
            }
            case PAUSED: {
                if (state != DataSourceState.PLAYING) {
                    return false;
                }
                break;
            }
            default: {
                throw new IllegalArgumentException();
            }
        }
        return true;
    }

    private class NotifyChangeRunnable implements Runnable {
        @Override
        public void run() {
            List<StateChangedListener> changedListeners;
            DataSourceState state = getState();
            synchronized (this) {
                changedListeners = new ArrayList<>(listeners);
            }
            for (StateChangedListener listener : changedListeners) {
                listener.onStateChanged(BaseDataSource.this, state);
            }
        }
    }

    private class ItemFactoryImpl implements ItemFactory {
        @NonNull
        @Override
        public TimeLineItem create(@NonNull TimeLineModel model) {
            BaseTimeLineItem item = BaseTimeLineItem.create(model);
            Long lastReadId = getModel().getLastReadId();
            if (lastReadId != null && lastReadId == item.getNumber()) {
                item.setIsPlaying(true);
            }
            return item;
        }
    }
}
