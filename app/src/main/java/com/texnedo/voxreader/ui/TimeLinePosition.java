package com.texnedo.voxreader.ui;

class TimeLinePosition {
    private final int start;
    private final int end;

    public TimeLinePosition(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    @Override
    public String toString() {
        return "TimeLinePosition{" +
                "start=" + start +
                ", end=" + end +
                '}';
    }
}
