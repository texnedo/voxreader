package com.texnedo.voxreader.net;

import java.net.ProtocolException;

public interface ConnectionBuilder {
    ConnectionBuilder setMethod(HttpConnectionImpl.Method method) throws ProtocolException;

    ConnectionBuilder setKeepAlive(boolean keepAlive);

    ConnectionBuilder allowRedirects(boolean allow);

    ConnectionBuilder setConnectTimeout(int ms);

    ConnectionBuilder setReadTimeout(int ms);

    ConnectionBuilder addHeader(String header, String value);

    ConnectionBuilder setCheckResponseEncoding(boolean checkEncoding);

    ConnectionBuilder acceptGzip(boolean accept);

    HttpConnection build();
}
