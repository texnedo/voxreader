package com.texnedo.voxreader;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.texnedo.voxreader.core.api.TimeLineAccess;
import com.texnedo.voxreader.core.api.TimeLineManager;
import com.texnedo.voxreader.ui.TimeLineAdapter;
import com.texnedo.voxreader.utils.UiThread;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.List;

@EActivity(R.layout.activity_time_line)
public class TimeLineActivity extends AppCompatActivity {
    private TimeLineAccess dataSource;
    private ProgressDialog progressDialog;
    private TimeLineAdapter timeLineAdapter;
    private Bundle savedInstanceState;
    private final DataSourceChangeListener listener = new DataSourceChangeListener();

    @ViewById(R.id.timeLineView)
    ListView timeLineView;

    @ViewById(R.id.toolbar)
    Toolbar toolbar;

    @AfterViews
    void setupControls() {
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        processIntent(getIntent());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_time_line, menu);
        return true;
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        this.savedInstanceState = savedInstanceState;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (timeLineAdapter != null) {
            timeLineAdapter.onSaveInstanceState(outState);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.play: {
                if (dataSource != null) {
                    VoxReaderApp.getManager().play(dataSource.getId());
                }
                break;
            }
            case R.id.stop: {
                if (dataSource != null) {
                    VoxReaderApp.getManager().stop(dataSource.getId());
                }
                break;
            }
            case R.id.pause: {
                if (dataSource != null) {
                    VoxReaderApp.getManager().pause(dataSource.getId());
                }
                break;
            }
            case R.id.next: {
                if (dataSource != null) {
                    VoxReaderApp.getManager().next(dataSource.getId());
                }
                break;
            }
            case R.id.previous: {
                if (dataSource != null) {
                    VoxReaderApp.getManager().previous(dataSource.getId());
                }
                break;
            }
            case R.id.settings: {
                Navigate.goToSettings(this);
                break;
            }
            default: {
                throw new IllegalArgumentException();
            }
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        subscribeStateChange();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unSubscribeStateChange();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        processIntent(intent);
    }

    private void processIntent(Intent intent) {
        if (intent == null) {
            finish();
            return;
        }
        String id = intent.getExtras().getString(Navigate.DATA_SOURCE_ID_EXTRA);
        if (TextUtils.isEmpty(id)) {
            finish();
            return;
        }
        VoxReaderApp.getManager().requestDataSource(
                id,
                new TimeLineManager.DataSourceRequestListener() {
                    @Override
                    public void onComplete(@NonNull final List<TimeLineAccess> dataSources) {
                        UiThread.run(new Runnable() {
                            @Override
                            public void run() {
                                if (dataSources.size() == 0) {
                                    finish();
                                    return;
                                }
                                if (dataSources.size() > 1) {
                                    throw new IllegalArgumentException();
                                }
                                dataSource = dataSources.get(0);
                                toolbar.setTitle(dataSource.getName());
                                timeLineAdapter = new TimeLineAdapter(
                                        VoxReaderApp.getManager().getConfig(),
                                        dataSource,
                                        savedInstanceState
                                );
                                timeLineView.setAdapter(timeLineAdapter);
                                timeLineView.setDivider(null);
                                updateProgressState();
                            }
                        });
                    }

                    @Override
                    public void onFailure() {
                        //TODO - shot toast notification
                    }
                }
        );
    }

    private void subscribeStateChange() {
        VoxReaderApp.getManager().addListener(listener);
    }

    private void unSubscribeStateChange() {
        VoxReaderApp.getManager().removeListener(listener);
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog.setOnCancelListener(null);
            progressDialog = null;
        }
    }

    private void updateProgressState() {
        if (dataSource == null) {
            return;
        }
        if (dataSource.isProcessing()) {
            if (progressDialog == null || !progressDialog.isShowing()) {
                progressDialog = ProgressDialog.show(
                        this,
                        getString(R.string.time_line_data_source_progress_title),
                        getString(R.string.time_line_data_source_progress_text),
                        true
                );
                progressDialog.setCancelable(true);
                progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        finish();
                        Navigate.goToDataSources(TimeLineActivity.this);
                    }
                });
            }
        } else {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }
    }

    private class DataSourceChangeListener implements TimeLineManager.DataSourceChangeListener {
        @Override
        public void onItemsChanged() {
            UiThread.run(new Runnable() {
                @Override
                public void run() {
                    updateProgressState();
                }
            });
        }

        @Override
        public void onStateChanged(@NonNull TimeLineAccess dataSource) {
            UiThread.run(new Runnable() {
                @Override
                public void run() {
                    updateProgressState();
                }
            });
        }

        @Override
        public void onFailure() {

        }
    }
}
