package com.texnedo.voxreader.system;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class PreferenceImpl implements Preferences {
    private static final String VOXREADER_SETTINGS = "voxreader_settings";
    private static final String PLAY_NEXT_AFTER_COMPLETION_KEY = "play_next";
    private static final boolean PLAY_NEXT_AFTER_COMPLETION_DEFAULT = false;
    private static final String AUTO_DOWNLOAD_ENABLED_KEY = "auto_download";
    private static final boolean AUTO_DOWNLOAD_ENABLED_DEFAULT = false;
    private static final String SHOW_TEXT_IN_NOTIFICATION_KEY = "show_notification_text";
    private static final boolean SHOW_TEXT_IN_NOTIFICATION_DEFAULT = false;
    private final SharedPreferences preferences;
    private final HashSet<PreferenceListener> listeners = new HashSet<>();

    public PreferenceImpl(@NonNull Context context) {
        preferences = context.getSharedPreferences(VOXREADER_SETTINGS, Context.MODE_PRIVATE);
        preferences.registerOnSharedPreferenceChangeListener(new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                notifyChanged();
            }
        });
    }

    @Override
    public boolean playNextAfterCompletion() {
        return preferences.getBoolean(
                PLAY_NEXT_AFTER_COMPLETION_KEY,
                PLAY_NEXT_AFTER_COMPLETION_DEFAULT
        );
    }

    @Override
    public void setPlayNextAfterCompletion(boolean value) {
        preferences
                .edit()
                .putBoolean(PLAY_NEXT_AFTER_COMPLETION_KEY, value)
                .apply();
    }

    @Override
    public boolean showTextInNotification() {
        return preferences.getBoolean(
                SHOW_TEXT_IN_NOTIFICATION_KEY,
                SHOW_TEXT_IN_NOTIFICATION_DEFAULT
        );
    }

    @Override
    public void setShowTextInNotification(boolean value) {
        preferences
                .edit()
                .putBoolean(SHOW_TEXT_IN_NOTIFICATION_KEY, value)
                .apply();
    }

    @Override
    public boolean autoDownloadEnabled() {
        return preferences.getBoolean(
                AUTO_DOWNLOAD_ENABLED_KEY,
                AUTO_DOWNLOAD_ENABLED_DEFAULT
        );
    }

    @Override
    public void setAutoDownloadEnabled(boolean value) {
        preferences
                .edit()
                .putBoolean(AUTO_DOWNLOAD_ENABLED_KEY, value)
                .apply();
    }

    @Override
    public synchronized void addListener(@NonNull PreferenceListener listener) {
        listeners.add(listener);
    }

    @Override
    public synchronized void removeListener(@NonNull PreferenceListener listener) {
        listeners.remove(listener);
    }

    private void notifyChanged() {
        //TODO - optimize this
        List<PreferenceListener> list;
        synchronized (this) {
            list = new ArrayList<>(listeners);
        }
        for (PreferenceListener listener : list) {
            listener.onChanged();
        }
    }
}
