package com.texnedo.voxreader.system;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;

import com.texnedo.voxreader.core.api.DataSource;
import com.texnedo.voxreader.core.api.LanguageDetector;
import com.texnedo.voxreader.core.api.TextToSpeechProvider;
import com.texnedo.voxreader.core.api.TimeLineBuilder;
import com.texnedo.voxreader.net.ClientException;
import com.texnedo.voxreader.net.ConnectionBuilder;
import com.texnedo.voxreader.storage.DatabaseManager;
import com.texnedo.voxreader.storage.ItemFactory;
import com.texnedo.voxreader.storage.ItemsChangeListener;
import com.texnedo.voxreader.storage.TimeLineModelFactory;
import com.texnedo.voxreader.storage.TimeLineStorage;

import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.ExecutorService;

public interface Config {
    Context getContext();

    Locale getCurrentLocale();

    Handler getDispatcher();

    ExecutorService getStorageExecutor();

    ExecutorService getNetworkExecutor();

    ExecutorService getLayoutRenderExecutor();

    ConnectionBuilder getConnectionBuilder(@NonNull String url) throws IOException, ClientException;

    TimeLineStorage getStorage(@NonNull DataSource dataSource,
                               @NonNull ItemFactory itemFactory);

    DatabaseManager getDatabaseManager(@NonNull ItemsChangeListener listener);

    TextToSpeechProvider getTTS();

    TimeLineBuilder getTimelineBuilder(@NonNull TimeLineModelFactory factory,
                                       @NonNull TimeLineBuilder.Receiver receiver);

    LanguageDetector getLanguageDetector();

    Preferences getSettings();
}
