package com.texnedo.voxreader.core.timeline;

import android.support.annotation.NonNull;

import com.texnedo.voxreader.core.api.TextToSpeechProvider;

public interface TimeLineItem {
    long getNumber();

    String getId();

    String getText();

    void play(@NonNull TextToSpeechProvider provider);

    void reset();

    boolean isPlaying();

    boolean canBeActivated();

    void setListener(Listener listener);

    interface Listener {
        void onChanged(@NonNull TimeLineItem item);
    }
}
