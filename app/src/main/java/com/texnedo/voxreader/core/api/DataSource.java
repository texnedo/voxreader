package com.texnedo.voxreader.core.api;

import android.support.annotation.NonNull;

import com.texnedo.voxreader.storage.dao.DataSourceModel;

public interface DataSource extends TimeLineAccess {
    DataSourceState getState();

    DataSourceModel getModel();

    interface StateChangedListener {
        void onStateChanged(@NonNull DataSource dataSource, DataSourceState state);
    }

    void addStateChangedListener(@NonNull StateChangedListener listener);

    void removeStateChangedListener(@NonNull StateChangedListener listener);

    void play();

    void play(long from);

    void pause();

    void stop();

    void next();

    void previous();

    void clear();
}
