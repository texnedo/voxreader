package com.texnedo.voxreader.ui.notifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;

import com.texnedo.voxreader.core.api.TimeLineAccess;
import com.texnedo.voxreader.system.Preferences;
import com.texnedo.voxreader.utils.FileLog;

public class NotificationBarManager {
    private final static String LOG_TAG = "NotificationBarManager";
    private final Context context;
    private final Preferences preferences;
    private final NotificationManager systemManager;

    public NotificationBarManager(@NonNull Context context,
                                  @NonNull Preferences preferences) {
        this.context = context;
        this.preferences = preferences;
        this.systemManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    private void cancel(NotificationId id, @NonNull String tag) {
        try {
            FileLog.d(LOG_TAG, "cancel tag %s id %d", tag, id.value());
            systemManager.cancel(tag, id.value());
        } catch (NullPointerException | SecurityException ex) {
            //motorola XT1033 throws NPE
            FileLog.e(LOG_TAG, "cancel", ex);
        }
    }

    public void showPlayback(@NonNull TimeLineAccess dataSource) {
        NotificationBase notification = new PlaybackNotification(context, preferences, dataSource);
        showNotification(notification);
    }

    public void showProgress(@NonNull TimeLineAccess dataSource) {
        NotificationBase notification = new ProgressNotification(context, dataSource);
        showNotification(notification);
    }

    public void cancelAll(@NonNull TimeLineAccess dataSource) {
        for(NotificationId id : NotificationId.values()) {
            cancel(id, dataSource.getId());
        }
    }

    public void cancelAll() {
        try {
            FileLog.d(LOG_TAG, "cancel all");
            systemManager.cancelAll();
        } catch (NullPointerException | SecurityException ex) {
            //motorola XT1033 throws NPE
            FileLog.e(LOG_TAG, "cancel all", ex);
        }
    }

    private void showNotification(@NonNull NotificationBase notification) {
        NotificationCompat.Builder builder = notification.getBuilder();

        builder.setDefaults(0);
        builder.setLocalOnly(true);
        builder.setCategory(NotificationCompat.CATEGORY_MESSAGE);
        if (notification.isSilent()) {
            builder.setSound(null);
            builder.setLights(Color.WHITE, 0, 0);
        } else {
            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            builder.setSound(uri);
            builder.setLights(Color.WHITE, 1500, 1500);
        }

        Notification systemNotification = builder.build();
        if (notification.isSilent()) {
            systemNotification.defaults &= ~Notification.DEFAULT_VIBRATE;
            systemNotification.defaults &= ~Notification.DEFAULT_SOUND;
        } else {
            systemNotification.vibrate = new long[] {500, 500};
        }
        if (notification.shouldReplacePrevious()) {
            cancel(notification.getId(), notification.getTag());
        }
        safeNotify(notification.getTag(), notification.getId(), systemNotification);
    }

    private void safeNotify(String tag, NotificationId id, @NonNull Notification notification) {
        safeNotify(tag, id.value(), notification);
    }

    private void safeNotify(String tag, int id, @NonNull Notification notification) {
        try {
            FileLog.d(LOG_TAG, "safeNotify tag %s id %d", tag, id);
            systemManager.notify(tag, id, notification);
        } catch (SecurityException ex) {
            FileLog.e(LOG_TAG, "safeNotify error", ex);
        }
    }
}
