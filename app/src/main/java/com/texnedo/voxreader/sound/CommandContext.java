package com.texnedo.voxreader.sound;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.texnedo.voxreader.core.api.TextToSpeechProvider;

import java.util.Locale;

interface CommandContext {
    void playUtterance(@NonNull String id,
                       @NonNull String utterance,
                       @Nullable TextToSpeechProvider.SpeechVolume volume);

    void playSilentUtterance(@NonNull String id, long duration);

    void setSpeechRate(float rate);

    void setPitch(float pitch);

    void playEarcon(@NonNull String id, TextToSpeechProvider.EarconType type);

    void playMusic(@NonNull String id, @NonNull Uri resource, boolean loop);

    void setLanguage(@NonNull Locale locale);
}
