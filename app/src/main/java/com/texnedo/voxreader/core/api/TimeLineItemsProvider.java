package com.texnedo.voxreader.core.api;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.texnedo.voxreader.core.timeline.TimeLineItem;
import com.texnedo.voxreader.storage.ItemsCountChangedListener;
import com.texnedo.voxreader.storage.ItemsCountReceiver;
import com.texnedo.voxreader.storage.ItemsReceiver;

import java.util.List;

public interface TimeLineItemsProvider {
    void loadAllItems(@NonNull ItemsReceiver receiver);

    void loadItems(long from, long to, @NonNull ItemsReceiver receiver);

    void loadItem(long id, @NonNull ItemsReceiver receiver);

    void loadCount(@NonNull ItemsCountReceiver receiver);

    int getRangeSize();

    @Nullable
    List<TimeLineItem> getItems(long from, long to);

    @Nullable
    TimeLineItem getItem(long id);

    void addItemsCountChangedListener(@NonNull ItemsCountChangedListener listener);

    void removeItemsCountChangedListener(@NonNull ItemsCountChangedListener listener);
}
