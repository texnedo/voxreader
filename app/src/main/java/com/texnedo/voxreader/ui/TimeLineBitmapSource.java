package com.texnedo.voxreader.ui;

import android.graphics.Bitmap;

interface TimeLineBitmapSource {
    void requestBitmap(int width);

    void setLayoutListener(TimeLineBitmapListener listener);

    Bitmap getCachedBitmap(int width);

    String getId();
}
