package com.texnedo.voxreader.sound;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Simple command object cache to prevent unnecessary object creation.
 * */
class CommandCache<T extends Command> {
    private final ConcurrentLinkedQueue<T> commandsCache = new ConcurrentLinkedQueue<>();

    void recycle(T command) {
        commandsCache.offer(command);
    }

    T obtain() {
        T command = commandsCache.poll();
        if (command != null) {
            if (command.isInUse()) {
                throw new IllegalStateException();
            }
        }
        return command;
    }
}
