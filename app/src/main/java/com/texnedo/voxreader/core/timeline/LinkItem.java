package com.texnedo.voxreader.core.timeline;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.texnedo.voxreader.core.api.TextToSpeechProvider;
import com.texnedo.voxreader.storage.TimeLineModel;

import java.util.Locale;

public class LinkItem extends BaseTimeLineItem {
    String resultText;

    protected LinkItem(@NonNull TimeLineModel model) {
        super(model);
    }

    @Override
    public String getText() {
        if (resultText == null) {
            if (TextUtils.isEmpty(model.getText())) {
                resultText = "[link]";
            } else {
                resultText = model.getText();
            }
        }
        return resultText;
    }

    @Override
    protected void playInternal(@NonNull TextToSpeechProvider provider) {
        if (!TextUtils.isEmpty(model.getText())) {
            provider.playUtterance(getId(), model.getText());
        }
    }

    @Override
    public boolean canBeActivated() {
        return true;
    }

    @Override
    public String toString() {
        return model.getMeta();
    }
}
