package com.texnedo.voxreader.ui.notifications;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;

import com.texnedo.voxreader.Navigate;
import com.texnedo.voxreader.core.api.TimeLineAccess;

import java.util.Random;

abstract class NotificationBase {
    public static final String NOTIFICATION_ACTION_PLAY = "action_play";
    public static final String NOTIFICATION_ACTION_PAUSE = "action_pause";
    public static final String NOTIFICATION_ACTION_STOP = "action_stop";
    public static final String NOTIFICATION_ACTION_NEXT = "action_next";
    public static final String NOTIFICATION_ACTION_SKIP = "action_skip";
    public static final String NOTIFICATION_ACTION_DELETE = "action_delete";
    public static final String NOTIFICATION_ID_EXTRA = "notification_id";

    protected final Context context;

    protected NotificationBase(Context context) {
        this.context = context;
    }

    public abstract NotificationId getId();

    public abstract String getTag();

    protected abstract NotificationCompat.Builder getBuilder();

    protected void fillBuilder(NotificationCompat.Builder builder) {
        if (hasHeadsUp() || isSilent()) {
            builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
        } else {
            builder.setPriority(NotificationCompat.PRIORITY_MAX);
        }
    }

    @SuppressWarnings("RedundantIfStatement")
    protected boolean hasHeadsUp() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return false;
        }
        return false;
    }

    @SuppressWarnings("RedundantIfStatement")
    protected boolean hasExtendedNotifications() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            return false;
        }
        return true;
    }

    public abstract boolean isSilent();

    public abstract boolean shouldReplacePrevious();

    static abstract class IntentBuilder {
        protected static final Random random = new Random();
        protected final Intent intent;
        protected final Context context;

        public IntentBuilder(@NonNull Context context, @NonNull Intent intent, String action) {
            this.intent = intent;
            this.context = context;
            this.intent.setAction(action);
        }

        public IntentBuilder putExtra(@NonNull String type, String value) {
            intent.putExtra(type, value);
            return this;
        }

        public IntentBuilder putExtra(@NonNull String type, Integer value) {
            intent.putExtra(type, value);
            return this;
        }

        public abstract PendingIntent build();
    }

    static class ServiceIntentBuilder extends IntentBuilder {
        public ServiceIntentBuilder(@NonNull Context context, @NonNull String action) {
            super(context, new Intent(context, NotificationService.class), action);
        }

        @Override
        public PendingIntent build() {
            return PendingIntent.getService(
                    context,
                    random.nextInt(),
                    intent,
                    PendingIntent.FLAG_CANCEL_CURRENT
            );
        }
    }

    static class TimeLineIntentBuilder extends IntentBuilder {
        public TimeLineIntentBuilder(@NonNull Context context,
                                     @NonNull TimeLineAccess dataSource) {
            super(context, Navigate.getTimeLineIntent(context, dataSource.getId()), null);
        }

        @Override
        public PendingIntent build() {
            return PendingIntent.getActivity(
                    context,
                    random.nextInt(),
                    intent,
                    PendingIntent.FLAG_CANCEL_CURRENT
            );
        }
    }

    static PendingIntent getTimeLineIntent(@NonNull Context context,
                                       @NonNull TimeLineAccess dataSource) {
        return new TimeLineIntentBuilder(context, dataSource).build();
    }

    static PendingIntent getNextIntent(@NonNull Context context,
                                       @NonNull TimeLineAccess dataSource) {
        return new ServiceIntentBuilder(context, NOTIFICATION_ACTION_NEXT)
                .putExtra(NotificationBase.NOTIFICATION_ID_EXTRA, dataSource.getId())
                .build();
    }

    static PendingIntent getSkipIntent(@NonNull Context context,
                                       @NonNull TimeLineAccess dataSource) {
        return new ServiceIntentBuilder(context, NOTIFICATION_ACTION_SKIP)
                .putExtra(NotificationBase.NOTIFICATION_ID_EXTRA, dataSource.getId())
                .build();
    }

    static PendingIntent getStopIntent(@NonNull Context context,
                                       @NonNull TimeLineAccess dataSource) {
        return new ServiceIntentBuilder(context, NOTIFICATION_ACTION_STOP)
                .putExtra(NotificationBase.NOTIFICATION_ID_EXTRA, dataSource.getId())
                .build();
    }

    static PendingIntent getPauseIntent(@NonNull Context context,
                                       @NonNull TimeLineAccess dataSource) {
        return new ServiceIntentBuilder(context, NOTIFICATION_ACTION_PAUSE)
                .putExtra(NotificationBase.NOTIFICATION_ID_EXTRA, dataSource.getId())
                .build();
    }

    static PendingIntent getPlayIntent(@NonNull Context context,
                                        @NonNull TimeLineAccess dataSource) {
        return new ServiceIntentBuilder(context, NOTIFICATION_ACTION_PLAY)
                .putExtra(NotificationBase.NOTIFICATION_ID_EXTRA, dataSource.getId())
                .build();
    }

    static PendingIntent getDeleteIntent(@NonNull Context context,
                                         @NonNull TimeLineAccess dataSource) {
        return new ServiceIntentBuilder(context, NOTIFICATION_ACTION_DELETE)
                .putExtra(NotificationBase.NOTIFICATION_ID_EXTRA, dataSource.getId())
                .build();
    }
}
