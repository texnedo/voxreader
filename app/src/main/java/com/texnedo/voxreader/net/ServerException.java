package com.texnedo.voxreader.net;

public class ServerException extends Exception {
    private int responseCode;

    public ServerException(int responseCode) {
        this.responseCode = responseCode;
    }

    public int getResponseCode() {
        return responseCode;
    }

    @Override
    public String toString() {
        return "ServerException{" +
                "responseCode=" + responseCode +
                '}';
    }
}
