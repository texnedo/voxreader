package com.texnedo.voxreader.storage;

import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityUnitTestCase;

import com.texnedo.voxreader.MainActivity_;
import com.texnedo.voxreader.storage.dao.DataSourceModel;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static java.lang.Thread.sleep;

@RunWith(AndroidJUnit4.class)
public class DatabaseManagerTest extends ActivityUnitTestCase<MainActivity_> {
    public DatabaseManagerTest() {
        super(MainActivity_.class);
    }

    @Test
    public void testDb() throws InterruptedException {
        sleep(200);
        DatabaseManagerImpl manager = new DatabaseManagerImpl(getInstrumentation().getContext());
        DataSourceModel data1 = new DataSourceModel();
        data1.setUri("uri1");
        data1.setStarred(true);
        data1.setTimelinePath("path1");
        DataSourceModel data2 = new DataSourceModel();
        data2.setUri("uri2");
        data2.setStarred(true);
        data2.setTimelinePath("path2");
        manager.insert(data2);
        manager.close();

        DatabaseManagerImpl manager1 = new DatabaseManagerImpl(getInstrumentation().getContext());
        List<DataSourceModel> list = manager1.requestAll();
        assertEquals(list.size(), 2);
        assertEquals(list.get(0).getUri(), data1.getUri());
        assertEquals(list.get(1).getTimelinePath(), data2.getTimelinePath());
    }
}

