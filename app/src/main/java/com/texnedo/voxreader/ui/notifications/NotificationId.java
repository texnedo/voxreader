package com.texnedo.voxreader.ui.notifications;

public enum NotificationId {
    PLAYBACK {
        @Override
        public int value() {
            return 1;
        }
    },
    PROGRESS {
        @Override
        public int value() {
            return 2;
        }
    };

    public abstract int value();
}
