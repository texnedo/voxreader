package com.texnedo.voxreader.net;

import com.texnedo.voxreader.utils.FileLog;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class HttpConnectionImpl implements HttpConnection {
    private static final String LOG_TAG = "HttpConnectionImpl";
    //TODO - add something more appropriate here
    private static final String MOZILLA_USER_AGENT = "Mozilla/5.0 (Android 4.4; Mobile; rv:41.0) Gecko/41.0 Firefox/41.0";
    public static final int DEFAULT_CONNECTION_TIMEOUT = 30 * 1000;
    public static final int DEFAULT_READ_TIMEOUT = 30 * 1000;
    private final String url;
    private final HttpURLConnection connection;
    private final boolean checkResponseEncoding;

    private HttpConnectionImpl(String url, HttpURLConnection connection, boolean checkResponseEncoding) {
        this.url = url;
        this.connection = connection;
        this.connection.setRequestProperty("User-Agent", MOZILLA_USER_AGENT);
        this.checkResponseEncoding = checkResponseEncoding;
    }

    public static ConnectionBuilder getBuilder(String url) throws IOException, ClientException {
        return new BuilderImpl(url);
    }

    @Override
    public int getResponseCode() throws IOException, ClientException {
        if (Thread.interrupted()) {
            throw new ClientException("The thread has been cancelled before the request start");
        }
        FileLog.v(LOG_TAG, "getResponseCode %s %s", url, getRequestData());
        try {
            int responseCode = connection.getResponseCode();
            FileLog.v(LOG_TAG, "getResponseCode %s code %d", url, responseCode);
            return responseCode;
        } catch (IOException e) {
            int responseCode = connection.getResponseCode();
            FileLog.v(LOG_TAG, "getResponseCode %s code after exception %d", url, responseCode);
            return responseCode;
        }
    }

    @SuppressWarnings({"TryFinallyCanBeTryWithResources", "ConstantConditions"})
    @Override
    public String getResponseAsString() throws IOException, ServerException, ClientException {
        try {
            int responseCode = getResponseCode();
            if (Thread.interrupted()) {
                emptyAndClose();
                throw new ClientException("The thread has been cancelled after connection start");
            }
            if (responseCode != HttpURLConnection.HTTP_OK) {
                emptyAndClose();
                throw new ServerException(responseCode);
            }

            InputStream inputStream = getInputStream();

            try {
                String charset = "UTF-8";
                if (checkResponseEncoding) {
                    String contentType = getHeaderField("Content-Type");
                    if (contentType != null) {
                        for (String param : contentType.replace(" ", "").split(";")) {
                            if (param.startsWith("charset=")) {
                                charset = param.split("=", 2)[1];
                                break;
                            }
                        }
                    }
                }

                StringBuilder sb = new StringBuilder(1024);
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream, charset);
                try {
                    BufferedReader reader = new BufferedReader(inputStreamReader);
                    try {
                        for (String line; (line = reader.readLine()) != null; ) {
                            sb.append(line);
                        }
                    } finally {
                        reader.close();
                    }
                } finally {
                    inputStreamReader.close();
                }

                String response = sb.toString();
                FileLog.d(LOG_TAG, "getResponseAsString %s\n%s\n%s", url, getResponseData(), response);
                return response;
            } finally {
                inputStream.close();
            }

        } catch (Throwable ex) {
            FileLog.e(LOG_TAG, "getResponseAsString error", ex);
            throw  ex;
        } finally {
            connection.disconnect();
        }
    }

    @Override
    public String getHeaderField(String s) {
        return connection.getHeaderField(s);
    }

    private static void emptyAndClose(InputStream inputStream) throws IOException {
        if (inputStream == null)
            return;

        try {
            byte[] buf = new byte[1024];
            //noinspection StatementWithEmptyBody
            while (inputStream.read(buf) >= 0) ;
        } finally {
            try {
                inputStream.close();
            } catch (IOException ex) {
                FileLog.e(LOG_TAG, "failed to read all data from the stream", ex);
            }
        }
    }

    private void emptyAndClose() {
        try {
            emptyAndClose(connection.getInputStream());
        } catch (IOException e) {
            FileLog.e(LOG_TAG, "failed to clear the input stream");
        }
        try {
            emptyAndClose(connection.getErrorStream());
        } catch (IOException e) {
            FileLog.e(LOG_TAG, "failed to clear the error stream");
        }
        connection.disconnect();
    }

    private InputStream getInputStream() throws IOException {
        InputStream inputStream;
        try {
            inputStream = connection.getInputStream();
            try {
                emptyAndClose(connection.getErrorStream());
            } catch (IOException ex) {
                FileLog.e(LOG_TAG, "failed to clear the stream", ex);
            }
        } catch (FileNotFoundException e) {
            inputStream = connection.getErrorStream();
            if (inputStream == null)
                throw new IOException("errorStream is null");
        }
        return inputStream;
    }

    private String getRequestData() {
        StringBuilder stringBuilder = new StringBuilder();
        for (String header : connection.getRequestProperties().keySet()) {
            stringBuilder.append(header).append(" : ").append(connection.getRequestProperty(header)).append('\n');
        }
        return stringBuilder.toString();
    }

    private String getResponseData() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("contentLength : ").append(connection.getContentLength()).append('\n');
        for (String header : connection.getHeaderFields().keySet()) {
            stringBuilder.append(header).append(" : ").append(connection.getHeaderField(header)).append('\n');
        }
        return stringBuilder.toString();
    }

    private static class BuilderImpl implements ConnectionBuilder {
        private final HttpURLConnection connection;
        private final String url;
        private boolean checkResponseEncoding;

        private BuilderImpl(String url) throws IOException, ClientException {
            this.url = url;
            try {
                this.connection = (HttpURLConnection) new URL(url).openConnection();
            } catch (MalformedURLException e) {
                throw new ClientException(e);
            }
            setConnectTimeout(DEFAULT_CONNECTION_TIMEOUT);
            setReadTimeout(DEFAULT_READ_TIMEOUT);
            allowRedirects(false);
        }

        @Override
        public ConnectionBuilder setMethod(Method method) throws ProtocolException {
            switch (method) {
                case GET:
                    connection.setRequestMethod("GET");
                    connection.setDoInput(true);
                    connection.setDoOutput(false);
                    break;
                case POST:
                    connection.setRequestMethod("POST");
                    connection.setDoInput(true);
                    connection.setDoOutput(true);
                    break;
                case HEAD:
                    connection.setRequestMethod("HEAD");
                    connection.setDoInput(false);
                    connection.setDoOutput(false);
                    break;
            }
            return this;
        }

        @Override
        public ConnectionBuilder setKeepAlive(boolean keepAlive) {
            if (keepAlive)
                connection.addRequestProperty("Connection", "Keep-Alive");
            else
                connection.addRequestProperty("Connection", "Close");
            return this;
        }

        @Override
        public ConnectionBuilder allowRedirects(boolean allow) {
            connection.setInstanceFollowRedirects(allow);
            return this;
        }

        @Override
        public ConnectionBuilder setConnectTimeout(int ms) {
            connection.setConnectTimeout(ms);
            return this;
        }

        @Override
        public ConnectionBuilder setReadTimeout(int ms) {
            connection.setReadTimeout(ms);
            return this;
        }

        @Override
        public ConnectionBuilder addHeader(String header, String value) {
            connection.addRequestProperty(header, value);
            return this;
        }

        @Override
        public ConnectionBuilder setCheckResponseEncoding(boolean checkEncoding) {
            this.checkResponseEncoding = checkEncoding;
            return this;
        }

        @Override
        public ConnectionBuilder acceptGzip(boolean accept) {
            if (accept) {
                connection.addRequestProperty("Accept-Encoding", "gzip");
            }
            return this;
        }

        @Override
        public HttpConnection build() {
            return new HttpConnectionImpl(url, connection, checkResponseEncoding);
        }
    }
}
