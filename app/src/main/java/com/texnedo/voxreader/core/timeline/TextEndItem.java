package com.texnedo.voxreader.core.timeline;

import android.support.annotation.NonNull;

import com.texnedo.voxreader.core.api.TextToSpeechProvider;
import com.texnedo.voxreader.storage.TimeLineModel;

public class TextEndItem extends BaseTimeLineItem {
    protected TextEndItem(@NonNull TimeLineModel model) {
        super(model);
    }

    @Override
    public String getText() {
        return null;
    }

    @Override
    protected void playInternal(@NonNull TextToSpeechProvider provider) {
        provider.playEarcon(getId(), TextToSpeechProvider.EarconType.TEXT_END);
    }

    @Override
    public String toString() {
        return TextToSpeechProvider.EarconType.TEXT_END.name();
    }
}