package com.texnedo.voxreader.core.api;

import android.support.annotation.NonNull;

import java.util.Locale;

public interface LanguageDetector {
    void process(@NonNull String text);

    Locale getDetectedLocale();

    void complete();
}
