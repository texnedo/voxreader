package com.texnedo.voxreader.sound;

import android.net.Uri;
import android.support.annotation.NonNull;

public class PlayMusicCommand extends Command {
    private String id;
    private Uri resource;
    private boolean loop;

    protected PlayMusicCommand(CommandContext context) {
        super(context);
    }

    @Override
    public boolean waitForCompletion() {
        return false;
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    protected void recycleInternal() {

    }

    protected static Command obtain(@NonNull CommandContext context,
                                    @NonNull String id,
                                    @NonNull Uri resource,
                                    boolean loop) {

        PlayMusicCommand command = new PlayMusicCommand(context);
        command.id = id;
        command.resource = resource;
        command.loop = loop;
        return command;
    }

    @Override
    public void run() {
        context.playMusic(id, resource, loop);
    }
}
