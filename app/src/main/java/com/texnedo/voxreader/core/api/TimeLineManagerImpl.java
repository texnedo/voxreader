package com.texnedo.voxreader.core.api;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.texnedo.voxreader.data.BaseDataSource;
import com.texnedo.voxreader.storage.DatabaseManager;
import com.texnedo.voxreader.storage.ItemsChangeListener;
import com.texnedo.voxreader.storage.LoadItemsListener;
import com.texnedo.voxreader.storage.dao.DataSourceModel;
import com.texnedo.voxreader.system.Config;
import com.texnedo.voxreader.ui.notifications.NotificationBarManager;
import com.texnedo.voxreader.utils.FileLog;
import com.texnedo.voxreader.utils.Utils;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

//TODO - refactor visibility
public class TimeLineManagerImpl implements TimeLineManager {
    private final static String LOG_TAG = "TimeLineManager";
    private final Config config;
    private final DatabaseManager databaseManager;
    private final HashSet<DataSourceChangeListener> listeners = new HashSet<>();
    private final HashMap<String, BaseDataSource> dataSourcesLookup = new HashMap<>();
    private final ArrayList<DataSource> dataSources = new ArrayList<>();
    private final DataSourceStateChangeListener dataSourceListener = new DataSourceStateChangeListener();
    private final NotificationBarManager notificationManager;
    private boolean dataSourcesLoaded = false;

    public TimeLineManagerImpl(final Config config) {
        this.config = config;
        this.notificationManager = new NotificationBarManager(
                config.getContext(),
                config.getSettings()
        );
        this.databaseManager =
                config.getDatabaseManager(new ItemsChangeListener() {
                    @Override
                    public void onInserted(@NonNull DataSourceModel model) {
                    }

                    @Override
                    public void onDeleted(@NonNull DataSourceModel model) {
                    }

                    @Override
                    public void onUpdated(DataSourceModel dataSource) {

                    }

                    @Override
                    public void onCleared() {
                        notifyChanged();
                    }

                    @Override
                    public void onFailure() {
                        notifyFailure();
                        dataSourcesLoaded = false;
                        requestDataSources(new DataSourceRequestListener() {
                            @Override
                            public void onComplete(@NonNull List<TimeLineAccess> dataSources) {
                                notifyChanged();
                            }

                            @Override
                            public void onFailure() {

                            }
                        });
                    }
                });
    }

    @Override
    public void add(@NonNull final Uri uri) {
        config.getDispatcher().post(new Runnable() {
            @Override
            public void run() {
                BaseDataSource dataSource;
                try {
                    File file = new File(uri.getPath());
                    if (file.exists()) {
                        String ext = Utils.getFileExtension(file);
                        if (TextUtils.equals(ext, "txt")) {
                            dataSource = BaseDataSource.createTextFileDataSource(config, uri);
                        } else {
                            //TODO - support other formats: pdf fb2...
                            throw new IllegalArgumentException();
                        }
                    } else {
                        //TODO - check if this is txt file link
                        dataSource = BaseDataSource.createHtmlDataSource(config, uri);
                    }
                } catch (Exception ex) {
                    FileLog.e(LOG_TAG, "caught error during try to open uri as a local file", ex);
                    dataSource = BaseDataSource.createHtmlDataSource(config, uri);
                }
                databaseManager.insert(dataSource.getModel());
                dataSources.add(dataSource);
                dataSourcesLookup.put(dataSource.getId(), dataSource);
                dataSource.addStateChangedListener(dataSourceListener);
                notifyChanged();
            }
        });
    }

    @Override
    public void add(@NonNull final String text) {
        config.getDispatcher().post(new Runnable() {
            @Override
            public void run() {
                BaseDataSource dataSource;
                try {
                    URI target = URI.create(text);
                    dataSource = BaseDataSource.createHtmlDataSource(config, Uri.parse(target.toString()));
                } catch (Exception ex) {
                    dataSource = BaseDataSource.createTextDataSource(config, text);
                }
                databaseManager.insert(dataSource.getModel());
                dataSources.add(dataSource);
                dataSourcesLookup.put(dataSource.getId(), dataSource);
                dataSource.addStateChangedListener(dataSourceListener);
                notifyChanged();
            }
        });
    }

    @Override
    public void requestDataSources(@NonNull final DataSourceRequestListener listener) {
        config.getDispatcher().post(new Runnable() {
            @Override
            public void run() {
                if (dataSourcesLoaded) {
                    ArrayList<TimeLineAccess> result = new ArrayList<>(dataSources.size());
                    for (DataSource dataSource : dataSources) {
                        result.add(dataSource);
                    }
                    listener.onComplete(result);
                } else {
                    databaseManager.requestAll(new LoadItemsListener() {
                        @Override
                        public void onComplete(final List<DataSourceModel> items) {
                            config.getDispatcher().post(new Runnable() {
                                @Override
                                public void run() {
                                    ArrayList<TimeLineAccess> result = new ArrayList<>(items.size());
                                    dataSources.clear();
                                    for (DataSourceModel model : items) {
                                        BaseDataSource dataSource = BaseDataSource.create(config, model);
                                        result.add(dataSource);
                                        dataSources.add(dataSource);
                                        dataSourcesLookup.put(dataSource.getId(), dataSource);
                                        dataSource.addStateChangedListener(dataSourceListener);
                                    }
                                    dataSourcesLoaded = true;
                                    listener.onComplete(result);
                                }
                            });
                        }

                        @Override
                        public void onFailure() {
                            listener.onFailure();
                        }
                    });
                }
            }
        });
    }

    @Override
    public void requestDataSource(@NonNull final String id, @NonNull final DataSourceRequestListener listener) {
        config.getDispatcher().post(new Runnable() {
            @Override
            public void run() {
                if (dataSourcesLoaded) {
                    DataSource dataSource = dataSourcesLookup.get(id);
                    if (dataSource == null) {
                        //noinspection unchecked
                        listener.onComplete(Collections.EMPTY_LIST);
                    } else {
                        listener.onComplete(Collections.singletonList((TimeLineAccess) dataSource));
                    }
                } else {
                    databaseManager.requestOne(id, new LoadItemsListener() {
                        @Override
                        public void onComplete(final List<DataSourceModel> items) {
                            if (items.size() > 1) {
                                throw new IllegalStateException();
                            }
                            config.getDispatcher().post(new Runnable() {
                                @Override
                                public void run() {
                                    ArrayList<TimeLineAccess> result = new ArrayList<>(items.size());
                                    for (DataSourceModel model : items) {
                                        BaseDataSource dataSource = BaseDataSource.create(config, model);
                                        BaseDataSource existing = dataSourcesLookup.get(dataSource.getId());
                                        if (existing == null) {
                                            dataSources.add(dataSource);
                                            dataSourcesLookup.put(dataSource.getId(), dataSource);
                                            dataSource.addStateChangedListener(dataSourceListener);
                                        } else {
                                            dataSource = existing;
                                        }
                                        result.add(dataSource);
                                    }
                                    listener.onComplete(result);
                                }
                            });
                        }

                        @Override
                        public void onFailure() {
                            listener.onFailure();
                        }
                    });
                }
            }
        });
    }

    @Override
    public void delete(@NonNull final String id) {
        requestDataSource(id, new DataSourceRequestListener() {
            @Override
            public void onComplete(@NonNull List<TimeLineAccess> items) {
                BaseDataSource dataSource = dataSourcesLookup.remove(id);
                if (dataSource != null) {
                    dataSource.removeStateChangedListener(dataSourceListener);
                    dataSource.clear();
                    dataSources.remove(dataSource);
                    databaseManager.delete(dataSource.getModel());
                    notifyChanged();
                }
            }

            @Override
            public void onFailure() {

            }
        });
    }

    @Override
    public void deletedAll() {
        config.getDispatcher().post(new Runnable() {
            @Override
            public void run() {
                for (DataSource dataSource : dataSources) {
                    dataSource.removeStateChangedListener(dataSourceListener);
                    dataSource.clear();
                }
                dataSourcesLookup.clear();
                dataSources.clear();
                databaseManager.deleteAll();
                notifyChanged();
            }
        });
    }

    @Override
    public void play(@NonNull final String id) {
        requestDataSource(id, new DataSourceRequestListener() {
            @Override
            public void onComplete(@NonNull List<TimeLineAccess> dataSources) {
                if (dataSources.isEmpty()) {
                    FileLog.e(LOG_TAG, "failed to play datasource %s (no such item)", id);
                    return;
                }
                ((DataSource) dataSources.get(0)).play();
            }

            @Override
            public void onFailure() {
                //TODO - handler error
            }
        });
    }

    @Override
    public void pause(@NonNull final String id) {
        requestDataSource(id, new DataSourceRequestListener() {
            @Override
            public void onComplete(@NonNull List<TimeLineAccess> dataSources) {
                if (dataSources.isEmpty()) {
                    FileLog.e(LOG_TAG, "failed to pause datasource %s (no such item)", id);
                    return;
                }
                ((DataSource) dataSources.get(0)).pause();
            }

            @Override
            public void onFailure() {
                //TODO - handler error
            }
        });
    }

    @Override
    public void stop(@NonNull final String id) {
        requestDataSource(id, new DataSourceRequestListener() {
            @Override
            public void onComplete(@NonNull List<TimeLineAccess> dataSources) {
                if (dataSources.isEmpty()) {
                    FileLog.e(LOG_TAG, "failed to stop datasource %s (no such item)", id);
                    return;
                }
                ((DataSource) dataSources.get(0)).stop();
            }

            @Override
            public void onFailure() {
                //TODO - handler error
            }
        });
    }

    @Override
    public void next(@NonNull final String id) {
        requestDataSource(id, new DataSourceRequestListener() {
            @Override
            public void onComplete(@NonNull List<TimeLineAccess> dataSources) {
                if (dataSources.isEmpty()) {
                    FileLog.e(LOG_TAG, "failed to go next item in datasource %s (no such item)", id);
                    return;
                }
                ((DataSource) dataSources.get(0)).next();
            }

            @Override
            public void onFailure() {
                //TODO - handler error
            }
        });
    }

    @Override
    public void playNext(@NonNull final String currentId) {
        requestDataSources(new DataSourceRequestListener() {
            @Override
            public void onComplete(@NonNull List<TimeLineAccess> items) {
                if (items.isEmpty()) {
                    FileLog.e(
                            LOG_TAG,
                            "failed to go next datasource from current %s (no such item)",
                            currentId
                    );
                    return;
                }
                TimeLineAccess current = null;
                for (TimeLineAccess item : items) {
                    if (TextUtils.equals(item.getId(), currentId)) {
                        current = item;
                    } else if (current != null) {
                        ((DataSource) current).stop();
                        ((DataSource) item).play();
                        break;
                    }
                }
            }

            @Override
            public void onFailure() {

            }
        });
    }

    @Override
    public void previous(@NonNull final String id) {
        requestDataSource(id, new DataSourceRequestListener() {
            @Override
            public void onComplete(@NonNull List<TimeLineAccess> dataSources) {
                if (dataSources.isEmpty()) {
                    FileLog.e(LOG_TAG, "failed to go previous item in datasource %s (no such item)", id);
                    return;
                }
                ((DataSource) dataSources.get(0)).previous();
            }

            @Override
            public void onFailure() {
                //TODO - handler error
            }
        });
    }

    @Override
    public synchronized void addListener(@NonNull DataSourceChangeListener listener) {
        listeners.add(listener);
    }

    @Override
    public synchronized void removeListener(@NonNull DataSourceChangeListener listener) {
        listeners.remove(listener);
    }

    @Override
    public Context getContext() {
        return config.getContext();
    }

    @Override
    public Config getConfig() {
        return config;
    }

    private void notifyChanged() {
        List<DataSourceChangeListener> list = copyListeners();
        for (DataSourceChangeListener listener : list) {
            listener.onItemsChanged();
        }
    }

    private void notifyStateChanged(@NonNull DataSource dataSource) {
        List<DataSourceChangeListener> list = copyListeners();
        for (DataSourceChangeListener listener : list) {
            listener.onStateChanged(dataSource);
        }
    }

    private void notifyFailure() {
        List<DataSourceChangeListener> list = copyListeners();
        for (DataSourceChangeListener listener : list) {
            listener.onFailure();
        }
    }

    @NonNull
    private List<DataSourceChangeListener> copyListeners() {
        //TODO - optimize this
        List<DataSourceChangeListener> list;
        synchronized (this) {
            list = new ArrayList<>(listeners);
        }
        return list;
    }

    private class DataSourceStateChangeListener implements DataSource.StateChangedListener {
        @Override
        public void onStateChanged(@NonNull DataSource dataSource, DataSourceState state) {
            databaseManager.update(dataSource.getModel());
            notifyStateChanged(dataSource);
            if (dataSource.isPlaying() || dataSource.isPaused()) {
                notificationManager.showPlayback(dataSource);
            } else if (dataSource.isProcessing()) {
                notificationManager.showProgress(dataSource);
            } else {
                notificationManager.cancelAll(dataSource);
            }
            FileLog.v(
                    LOG_TAG,
                    "datasource %s (%s) state changed to %s",
                    dataSource.getName(),
                    dataSource.getId(),
                    state
            );
        }
    }
}
