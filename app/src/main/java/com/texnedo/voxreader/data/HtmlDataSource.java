package com.texnedo.voxreader.data;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;

import com.texnedo.voxreader.core.api.DataSourceState;
import com.texnedo.voxreader.core.api.TextToSpeechProvider;
import com.texnedo.voxreader.core.api.TimeLineBuilder;
import com.texnedo.voxreader.core.html.HtmlNodeProcessor;
import com.texnedo.voxreader.core.timeline.BaseTimeLineItem;
import com.texnedo.voxreader.net.HttpConnection;
import com.texnedo.voxreader.net.ServerException;
import com.texnedo.voxreader.storage.dao.DataSourceModel;
import com.texnedo.voxreader.system.Config;
import com.texnedo.voxreader.utils.FileLog;

import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.Future;

public class HtmlDataSource extends BaseDataSource {
    private static final String LOG_TAG = "HtmlDataSource";
    private final String originalUrl;

    protected HtmlDataSource(@NonNull Config config, @NonNull DataSourceModel model) {
        super(config, model);
        this.originalUrl = model.getContent();
    }

    private void createTimeLine(@NonNull List<Node> source) {
        FileLog.v(LOG_TAG, "create timeline from %s", originalUrl);
        TimeLineBuilder builder = getBuilder();
        for (Node node : source) {
            if (TextUtils.equals(node.nodeName(), "title")) {
                StringBuilder description = new StringBuilder();
                for (Node child : node.childNodes()) {
                    if (child instanceof TextNode) {
                        description.append(((TextNode)child).text());
                    }
                }
                setDescription(description.toString());
            } else {
                builder.appendNode(node);
            }
        }
        builder.complete();
        getStorage().flush();
        FileLog.v(LOG_TAG, "all items were added to a timeline from %s", originalUrl);
    }

    @Override
    protected Future<?> startLoadingInternal() {
        return config.getNetworkExecutor().submit(new Runnable() {
            @Override
            public void run() {
                try {
                    FileLog.v(LOG_TAG, "start loading timeline items from %s", originalUrl);
                    //TODO - check if this is html
                    final String htmlData = config.getConnectionBuilder(originalUrl)
                            .setMethod(HttpConnection.Method.GET)
                            .build()
                            .getResponseAsString();
                    HtmlNodeProcessor processor = new HtmlNodeProcessor(htmlData);
                    final List<Node> nodes = processor.getReadableNodes();
                    config.getDispatcher().post(new Runnable() {
                        @Override
                        public void run() {
                            createTimeLine(nodes);
                            setState(DataSourceState.READY);
                        }
                    });
                } catch (IOException e) {
                    //TODO - add here reconnect logic
                    FileLog.e(LOG_TAG, e, "failed to download data from %s", originalUrl);
                } catch (ServerException e) {
                    setState(DataSourceState.FAILED);
                    FileLog.e(LOG_TAG, e, "failed to download data from %s due to the server response", originalUrl);
                } catch (Exception e) {
                    setState(DataSourceState.FAILED);
                    FileLog.e(LOG_TAG, e, "failed to download data from %s due to an unknown error", originalUrl);
                }
            }
        });
    }

    @Override
    public String getId() {
        return Integer.toString(originalUrl.hashCode());
    }

    @Override
    protected DataSourceType getType() {
        return DataSourceType.HTML;
    }
}
