package com.texnedo.voxreader.sound;

import android.support.annotation.NonNull;

import com.texnedo.voxreader.core.api.TextToSpeechProvider;

class PlayCommand extends Command {
    private static final CommandCache<PlayCommand> commandsCache = new CommandCache<>();
    private String id;
    private String utterance;
    private TextToSpeechProvider.SpeechVolume volume;

    @Override
    public String id() {
        return id;
    }

    @Override
    public boolean waitForCompletion() {
        return true;
    }

    private PlayCommand(@NonNull CommandContext context) {
        super(context);
    }

    @Override
    protected void recycleInternal() {
        id = null;
        utterance = null;
        volume = TextToSpeechProvider.SpeechVolume.NORMAL;
        commandsCache.recycle(this);
    }

    protected static Command obtain(@NonNull CommandContext context,
                                        @NonNull String id,
                                        @NonNull String utterance,
                                        TextToSpeechProvider.SpeechVolume volume) {

        PlayCommand command = commandsCache.obtain();
        if (command == null) {
            command = new PlayCommand(context);
        }
        command.id = id;
        command.utterance = utterance;
        command.volume = volume;
        return command;
    }

    @Override
    public void run() {
        context.playUtterance(id, utterance, volume);
    }
}
