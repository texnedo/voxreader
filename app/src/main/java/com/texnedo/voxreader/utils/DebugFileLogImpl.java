package com.texnedo.voxreader.utils;

import android.content.Context;
import android.os.Environment;
import android.os.HandlerThread;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

class DebugFileLogImpl implements LogReceiver {
    private static final String VOXREADER_LOGS = "voxreader_logs";
    private static final String VOXREADER_LOG_WRITER = "voxreader_log_writer";
    private OutputStreamWriter streamWriter = null;
    private android.os.Handler logQueue;
    private SimpleDateFormat format;

    public DebugFileLogImpl(Context context) {
        try {
            if (context == null) {
                return;
            }
            File sdCard = Environment.getExternalStorageDirectory();
            if (sdCard == null) {
                return;
            }
            File dir = new File(sdCard.getAbsolutePath() + "/" + VOXREADER_LOGS + "/" + context.getPackageName());
            dir.mkdirs();
            this.format = new SimpleDateFormat("MM-dd HH:mm:ss.SSS", Locale.US);
            File currentFile = new File(dir, new SimpleDateFormat("yy-MM-dd", Locale.US).format(new Date()) + ".log");

            HandlerThread logWorker = new HandlerThread(VOXREADER_LOG_WRITER);
            logWorker.start();
            this.logQueue = new android.os.Handler(logWorker.getLooper());

            FileOutputStream stream = new FileOutputStream(currentFile, true);
            this.streamWriter = new OutputStreamWriter(stream);
            this.streamWriter.write(String.format(Locale.US, "---------------------%s---------------------\n", this.format.format(new Date())));
            this.streamWriter.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public void v(String tag, String message) {
        Log.v(tag, message);
        log("V", tag, message);
    }

    @Override
    public void v(String tag, String message, Throwable exception) {
        Log.v(tag, message, exception);
        log("V" ,tag, message, exception);
    }

    @Override
    public void e(String tag, String message) {
        Log.e(tag, message);
        log("E", tag, message);
    }

    @Override
    public void e(String tag, String message, Throwable exception) {
        Log.e(tag, message, exception);
        log("E" ,tag, message, exception);
    }

    @Override
    public void d(String tag, String message) {
        Log.d(tag, message);
        log("D", tag, message);
    }

    @Override
    public void d(String tag, String message, Throwable exception) {
        Log.d(tag, message, exception);
        log("D" ,tag, message, exception);
    }

    private void log(final String level, final String tag, final String message) {
        final long threadId = Thread.currentThread().getId();
        if (streamWriter != null) {
            logQueue.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        streamWriter.write(String.format(Locale.US, "%s(%d) %s/%s: %s\n", level, threadId, format.format(new Date()), tag, message));
                        streamWriter.flush();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void log(final String level, final String tag, final String message, final Throwable exception) {
        final long threadId = Thread.currentThread().getId();
        if (streamWriter != null) {
            logQueue.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        streamWriter.write(String.format(Locale.US, "%s(%d) %s/%s: %s\n", level, threadId, format.format(new Date()), tag, message));
                        StringWriter sw = new StringWriter();
                        PrintWriter pw = new PrintWriter(sw);
                        exception.printStackTrace(pw);
                        streamWriter.write(sw.toString());
                        streamWriter.flush();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
