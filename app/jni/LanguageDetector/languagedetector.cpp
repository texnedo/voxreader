#include "languagedetector.h"
#include <android/log.h>
#include <stdio.h>
#include <string.h>
#include "public/compact_lang_det.h"
#include "internal/lang_script.h"

#define LOG_TAG "LanguageDetector"
#define LOGV(...) __android_log_print(ANDROID_LOG_VERBOSE, LOG_TAG, __VA_ARGS__)
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)

LanguageDetector::LanguageDetector() {
}

const char* LanguageDetector::detect(const char* text) {
    if (!text || strlen(text) == 0) {
        LOGE("Failed to detect language from an empty text");
        return NULL;
    }
    LOGD("Detect language from text %s", text);
    try {
        bool retVal = false;
        CLD2::Language lang = CLD2::DetectLanguage(text, strlen(text), true, &retVal);
        if (lang == CLD2::UNKNOWN_LANGUAGE) {
            return NULL;
        }
        const char* langName = CLD2::LanguageName(lang);
        const char* langCode = CLD2::LanguageCode(lang);
        LOGD("Detected language %s (%s)", langName, langCode);
        return langCode;
    } catch(...) {
        LOGE("Failed to detect language because of exception");
        return NULL;
    }
}

extern "C" {
    JNIEXPORT jstring JNICALL Java_com_texnedo_voxreader_core_api_LanguageDetector2Impl_detect(JNIEnv* env, jobject thiz, jstring text) {
        jboolean iscopy;
        const char* empty = "";
        const char* str = env->GetStringUTFChars(text, &iscopy);
        LanguageDetector helper;
        const char* result = helper.detect(str);
        env->ReleaseStringUTFChars(text, str);
        if (!result) {
            return env->NewStringUTF(empty);
        } else {
            return env->NewStringUTF(result);
        }
    }
}
