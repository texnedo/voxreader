package com.texnedo.voxreader.ui;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.text.StaticLayout;

interface TimeLineBitmapListener {
    void onCompleted(@NonNull Bitmap bitmap);

    void onLayoutChanged();
}
