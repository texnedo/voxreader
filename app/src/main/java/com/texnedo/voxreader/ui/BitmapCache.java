package com.texnedo.voxreader.ui;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.util.Log;

import com.texnedo.voxreader.utils.FileLog;

import java.util.LinkedList;
import java.util.ListIterator;

final class BitmapCache <T> {
    private static final String LOG_TAG = "BitmapCache";
    private final LruCache lruCache;
    private final LinkedList<Bitmap> reusableBitmaps = new LinkedList<>();
    private final int maxAvailableSize;

    public BitmapCache(int maxCacheSize, int maxAvailableSize) {
        this.maxAvailableSize = maxAvailableSize;
        lruCache = new LruCache(maxCacheSize);
    }

    public Bitmap get(@NonNull T key, int width) {
        Bitmap bitmap = lruCache.get(key);
        if (bitmap != null && bitmap.getWidth() == width) {
            return bitmap;
        }
        return null;
    }

    public Bitmap put(@NonNull T key, @NonNull Bitmap value) {
        return lruCache.put(key, value);
    }

    public Bitmap create(int width, int height, Bitmap.Config config) {
        ListIterator<Bitmap> iterator = reusableBitmaps.listIterator();
        Bitmap suitable = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            while (iterator.hasNext()) {
                Bitmap existing = iterator.next();
                if (existing != null &&
                        existing.getWidth() >= width &&
                        existing.getHeight() >= height &&
                        existing.getConfig() == config) {
                    iterator.remove();
                    existing.reconfigure(width, height, config);
                    existing.eraseColor(Color.TRANSPARENT);
                    suitable = existing;
                    Log.v(LOG_TAG, String.format("existing bitmap resized for %d x %d", width, height));
                    break;
                }
            }
        }
        if (suitable == null) {
            suitable = Bitmap.createBitmap(width, height, config);
            Log.v(LOG_TAG, String.format("new bitmap created for %d x %d", width, height));
        }
        return suitable;
    }

    public void remove(@NonNull T key) {
        Bitmap bitmap = lruCache.remove(key);
        if (bitmap != null) {
            bitmap.recycle();
        }
    }

    public void evictAll() {
        lruCache.evictAll();
        for (Bitmap bitmap : reusableBitmaps) {
            if (bitmap != null && !bitmap.isRecycled()) {
                bitmap.recycle();
            }
        }
        reusableBitmaps.clear();
        FileLog.d(LOG_TAG, "all bitmap items has been removed");
    }

    private class LruCache extends android.util.LruCache<T, Bitmap> {
        public LruCache(int maxSize) {
            super(maxSize);
        }

        @Override
        protected void entryRemoved(boolean evicted, T key, Bitmap oldValue, Bitmap newValue) {
            super.entryRemoved(evicted, key, oldValue, newValue);
            if (oldValue != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    if (reusableBitmaps.size() > maxAvailableSize) {
                        Bitmap redundant = reusableBitmaps.pop();
                        if (redundant != null) {
                            redundant.recycle();
                        }
                    }
                    reusableBitmaps.push(oldValue);
                } else {
                    oldValue.recycle();
                }
            }
        }
    }
}
