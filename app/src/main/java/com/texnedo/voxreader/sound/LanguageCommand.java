package com.texnedo.voxreader.sound;

import android.support.annotation.NonNull;

import java.util.Locale;

public class LanguageCommand extends Command {
    private Locale locale;

    @Override
    public String id() {
        return null;
    }

    @Override
    public boolean waitForCompletion() {
        return false;
    }

    protected LanguageCommand(CommandContext context) {
        super(context);
    }

    @Override
    protected void recycleInternal() {
        locale = null;
    }

    @Override
    public void run() {
        context.setLanguage(locale);
    }

    protected static Command obtain(@NonNull CommandContext context,
                                    @NonNull Locale locale) {
        LanguageCommand command = new LanguageCommand(context);
        command.locale = locale;
        return command;
    }
}