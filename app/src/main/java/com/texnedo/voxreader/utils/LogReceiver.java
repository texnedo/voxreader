package com.texnedo.voxreader.utils;

public interface LogReceiver {
    void v(String tag, String message);
    void v(String tag, String message, Throwable exception);
    void e(String tag, String message);
    void e(String tag, String message, Throwable exception);
    void d(String tag, String message);
    void d(String tag, String message, Throwable exception);
}
