package com.texnedo.voxreader.net;

import java.io.IOException;

public interface HttpConnection {
    int getResponseCode() throws IOException, ClientException;

    String getResponseAsString() throws IOException, ServerException, ClientException;

    String getHeaderField(String s);

    enum Method {
        GET, POST, HEAD
    }

}
