package com.texnedo.voxreader.core.timeline;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.texnedo.voxreader.core.api.TextToSpeechProvider;
import com.texnedo.voxreader.storage.TimeLineModel;

public class ImageItem extends BaseTimeLineItem {
    protected ImageItem(@NonNull TimeLineModel model) {
        super(model);
    }

    @Override
    public String getText() {
        //TODO - move to resources
        return "[image]";
    }

    @Override
    protected void playInternal(@NonNull TextToSpeechProvider provider) {
        //TODO - add settings to enable/disable image alt reading
//        if (!TextUtils.isEmpty(model.getText())) {
//            provider.playUtterance(getId(), model.getText());
//        }
    }

    @Override
    public boolean canBeActivated() {
        return true;
    }

    @Override
    public String toString() {
        return model.getMeta();
    }
}