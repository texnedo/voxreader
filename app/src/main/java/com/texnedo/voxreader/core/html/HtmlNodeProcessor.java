package com.texnedo.voxreader.core.html;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.texnedo.voxreader.BuildConfig;
import com.texnedo.voxreader.core.utils.Utils;
import com.texnedo.voxreader.utils.FileLog;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;

import java.util.*;

public class HtmlNodeProcessor {
    private static final String LOG_TAG = "HtmlNodeProcessor";
    private static HashSet<String> allowedNodes;
    private static HashSet<String> extendedInfoNodes;
    private static HashSet<String> paragraphNodes;
    private static HashMap<String, Node> paragraphNodesCache;
    private final @NonNull String htmlSource;
    private Document document;

    public HtmlNodeProcessor(@NonNull String htmlSource) {
        this.htmlSource = htmlSource;
    }

    public List<Node> getReadableNodes() {
        LinkedList<Node> nextLevelElements = new LinkedList<>();
        nextLevelElements.add(getDocument());
        boolean hasNewElements = true;
        while(hasNewElements) {
            ListIterator<Node> iterator = nextLevelElements.listIterator();
            hasNewElements = false;
            while (iterator.hasNext()) {
                Node current = iterator.next();
                if (!current.childNodes().isEmpty()) {
                    if (!isExtendedInfoTag(current)) {
                        iterator.remove();
                        for (Node node : current.childNodes()) {
                            iterator.add(node);
                            hasNewElements = true;
                        }
                        if (isParagraphTag(current)) {
                            iterator.add(createParagraphNode(current));
                        }
                    }
                } else if (!isAllowedTag(current)) {
                    iterator.remove();
                }
            }
        }
        FileLog.v(LOG_TAG, "all extracted tags count %d", nextLevelElements.size());
        return nextLevelElements;
    }

    private Document getDocument() {
        if (document == null) {
            Readability readability = new Readability(htmlSource);
            readability.init();
            document = readability.getDocument();
        }
        return document;
    }

    private static boolean isAllowedTag(@NonNull Node node) {
        if (node instanceof TextNode) {
            return !((TextNode) node).isBlank();
        }
        return checkNode(node, getAllowedNodes());
    }

    private static HashSet<String> getAllowedNodes() {
        if (allowedNodes == null) {
            allowedNodes = new HashSet<>(getParagraphNodes());
            allowedNodes.addAll(getExtendedInfoNodes());
        }
        return allowedNodes;
    }

    private static HashSet<String> getExtendedInfoNodes() {
        if (extendedInfoNodes == null) {
            extendedInfoNodes = new HashSet<>();
            extendedInfoNodes.add("a");
            extendedInfoNodes.add("img");
            extendedInfoNodes.add("title");
        }
        return extendedInfoNodes;
    }

    private static HashSet<String> getParagraphNodes() {
        if (paragraphNodes == null) {
            paragraphNodes = new HashSet<>();
            paragraphNodes.add("p");
            paragraphNodes.add("h*");
        }
        return paragraphNodes;
    }

    private static Node createParagraphNode(@NonNull Node original) {
        if (paragraphNodesCache == null) {
            paragraphNodesCache = new HashMap<>();
        }
        Node node = paragraphNodesCache.get(original.nodeName());
        if (node != null) {
            return node;
        }
        if (!(original instanceof Element)) {
            return null;
        }
        Element originalElement = (Element) original;
        node = new Element(originalElement.tag(), originalElement.baseUri());
        paragraphNodesCache.put(original.nodeName(), node);
        return node;
    }

    private static boolean checkNode(@NonNull Node node, @NonNull HashSet<String> allowedNodes) {
        String tagName = node.nodeName().trim().toLowerCase();
        if (TextUtils.isEmpty(tagName)) {
            return false;
        }
        if (allowedNodes.contains(tagName)) {
            return true;
        }
        if (tagName.length() > 1 &&
                allowedNodes.contains(tagName.substring(0, 1) + "*") &&
                Utils.isDigits(tagName, 1)) {
            return true;
        }
        return false;
    }

    private static boolean isParagraphTag(@NonNull Node node) {
        return checkNode(node, getParagraphNodes());
    }

    private static boolean isExtendedInfoTag(@NonNull Node node) {
        return checkNode(node, getExtendedInfoNodes());
    }
}
