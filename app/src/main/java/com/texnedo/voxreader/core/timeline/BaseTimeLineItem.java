package com.texnedo.voxreader.core.timeline;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.texnedo.voxreader.core.api.TextToSpeechProvider;
import com.texnedo.voxreader.storage.TimeLineModel;
import com.texnedo.voxreader.storage.TimeLineModelFactory;
import com.texnedo.voxreader.utils.FileLog;

import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;

public abstract class BaseTimeLineItem implements TimeLineItem {
    private static final String LOG_TAG = "TimeLineItem";
    private static final int TEXT_ITEM_TYPE = 1;
    private static final int SILENCE_ITEM_TYPE = 2;
    private static final int LINK_ITEM_TYPE = 3;
    private static final int IMAGE_ITEM_TYPE = 4;
    private static final int HEADER_ITEM_TYPE = 5;
    private static final int TEXT_END_ITEM_TYPE = 6;
    protected final TimeLineModel model;
    private volatile boolean isPlaying = false;
    private volatile Listener listener;

    protected BaseTimeLineItem(@NonNull TimeLineModel model) {
        this.model = model;
    }

    public static BaseTimeLineItem createSimpleTextItem(@NonNull TimeLineModelFactory factory, @NonNull String text) {
        return new TextItem(factory.createModel(TEXT_ITEM_TYPE, 0, 0, text, null));
    }

    public static BaseTimeLineItem createLinkItem(@NonNull TimeLineModelFactory factory,
                                                  @NonNull String url,
                                                  @Nullable String description) {
        return new LinkItem(factory.createModel(LINK_ITEM_TYPE, 0, 0, description, url));
    }

    public static BaseTimeLineItem createImageItem(@NonNull TimeLineModelFactory factory,
                                                  @NonNull String url,
                                                  @Nullable String description) {
        return new ImageItem(factory.createModel(IMAGE_ITEM_TYPE, 0, 0, description, url));
    }

    public static BaseTimeLineItem createParagraphItem(@NonNull TimeLineModelFactory factory) {
        return new SilenceItem(factory.createModel(SILENCE_ITEM_TYPE, 0, 200, null, null));
    }

    public static BaseTimeLineItem createPeriodItem(@NonNull TimeLineModelFactory factory) {
        return new SilenceItem(factory.createModel(SILENCE_ITEM_TYPE, 0, 100, null, null));
    }

    public static BaseTimeLineItem createCommaItem(@NonNull TimeLineModelFactory factory) {
        return new SilenceItem(factory.createModel(SILENCE_ITEM_TYPE, 0, 20, null, null));
    }

    public static BaseTimeLineItem createHeaderItem(@NonNull TimeLineModelFactory factory) {
        return new HeaderItem(factory.createModel(HEADER_ITEM_TYPE, 0, 0, null, null));
    }

    public static BaseTimeLineItem createTextEndItem(@NonNull TimeLineModelFactory factory) {
        return new TextEndItem(factory.createModel(TEXT_END_ITEM_TYPE, 0, 0, null, null));
    }

    public static BaseTimeLineItem create(@NonNull TimeLineModel model) {
        switch (model.getType()) {
            case TEXT_ITEM_TYPE: {
                return new TextItem(model);
            }
            case SILENCE_ITEM_TYPE: {
                return new SilenceItem(model);
            }
            case IMAGE_ITEM_TYPE: {
                return new ImageItem(model);
            }
            case LINK_ITEM_TYPE: {
                return new LinkItem(model);
            }
            case HEADER_ITEM_TYPE: {
                return new HeaderItem(model);
            }
            case TEXT_END_ITEM_TYPE: {
                return new TextEndItem(model);
            }
            default: {
                throw new IllegalArgumentException(model + " is not supported");
            }
        }
    }

    @Override
    public final long getNumber() {
        return model.getId();
    }

    @Override
    public final String getId() {
        return Long.toString(model.getId());
    }

    @Override
    public final boolean isPlaying() {
        return isPlaying;
    }

    @Override
    public boolean canBeActivated() {
        return false;
    }

    @Override
    public final void setListener(Listener listener) {
        this.listener = listener;
    }

    @NonNull
    public final TimeLineModel getModel() {
        return model;
    }

    @Override
    public final void play(@NonNull TextToSpeechProvider provider) {
        playInternal(provider);
    }

    //TODO - hide this method call
    public void setIsPlaying(boolean value) {
        boolean previous = isPlaying;
        isPlaying = value;
        FileLog.d(LOG_TAG,
                "item id %s with text %s play changed %s -> %s",
                getId(),
                getText(),
                previous,
                value
        );
        if (previous != value && listener != null) {
            listener.onChanged(this);
        }
    }

    @Override
    public void reset() {
        setIsPlaying(false);
    }

    protected abstract void playInternal(@NonNull TextToSpeechProvider provider);
}
