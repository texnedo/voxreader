package com.texnedo.voxreader.storage;

import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityUnitTestCase;
import android.util.Log;

import com.texnedo.voxreader.MainActivity_;
import com.texnedo.voxreader.core.timeline.BaseTimeLineItem;
import com.texnedo.voxreader.core.timeline.TimeLineItem;
import com.texnedo.voxreader.storage.cache.TimeLineCache;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Thread.sleep;

@RunWith(AndroidJUnit4.class)
public class TimeLineCacheTest extends ActivityUnitTestCase<MainActivity_> {
    private static final String TAG = "TimeLineCacheTest";

    public TimeLineCacheTest() {
        super(MainActivity_.class);
    }

    @Test
    public void testPutSingleLoadSingle() throws Exception {
        TimeLineCache cache = new TimeLineCache(50, 5);
        TimeLineModelFactory factory = new TimeLineModelFactory() {
            private Long itemId = (long) 0;
            @Override
            public TimeLineModel createModel(int type, int flags, int duration, String text, String meta) {
                return new TimeLineModel(itemId++, type, flags, duration, text, meta);
            }
        };
        ArrayList<TimeLineItem> items = new ArrayList<>(100);
        for (int i = 0; i < 100; ++i) {
            items.add(BaseTimeLineItem.createSimpleTextItem(factory, "test text super " + i));
        }
        for (int i = 0; i < 100; ++i) {
            cache.put(items.get(i), false);
        }
        int count = 0;
        for (int i = 0; i < 100; ++i) {
            if (cache.get(i) != null) {
                count++;
            }
        }
        assertEquals(count, 50);
    }

    @Test
    public void testPutItemsByGroups() throws Exception {
        sleep(300);
        TimeLineCache cache = new TimeLineCache(10000, 10);
        TimeLineModelFactory factory = new TimeLineModelFactory() {
            private Long itemId = (long) 0;
            @Override
            public TimeLineModel createModel(int type, int flags, int duration, String text, String meta) {
                return new TimeLineModel(itemId++, type, flags, duration, text, meta);
            }
        };
        for (int i = 0; i < 1000; ++i) {
            ArrayList<TimeLineItem> items = new ArrayList<>(10);
            for (int j = 0; j < 10; ++j) {
                items.add(BaseTimeLineItem.createSimpleTextItem(factory, "test text super " + i));
            }
            cache.putAll(items, false);
        }
        int count = 0;
        long start = 0;
        while (true) {
            List<TimeLineItem> result = cache.getAll(start, start + 10 - 1);
            if (result == null) {
                break;
            }
            count++;
            start += 10;
        }
        assertEquals(count, 1000);
    }

    @Test
    public void testPutManyItems() throws Exception {
        TimeLineCache cache = new TimeLineCache(1000000, 100);
        TimeLineModelFactory factory = new TimeLineModelFactory() {
            private Long itemId = (long) 0;
            @Override
            public TimeLineModel createModel(int type, int flags, int duration, String text, String meta) {
                return new TimeLineModel(itemId++, type, flags, duration, text, meta);
            }
        };
        ArrayList<TimeLineItem> items = new ArrayList<>(1000050);
        for (int i = 0; i < 1000050; ++i) {
            items.add(BaseTimeLineItem.createSimpleTextItem(factory, "test text super " + i));
        }
        for (int i = 0; i < 1000050; ++i) {
            cache.put(items.get(i), false);
        }
        long start = System.currentTimeMillis();
        int count = 0;
        for (int i = 0; i < 1000050; ++i) {
            if (cache.get(i) != null) {
                count++;
            }
        }
        long diff = System.currentTimeMillis() - start;
        long start2 = System.currentTimeMillis();
        int count2 = 0;
        for (int i = 0; i < 1000050; ++i) {
            if (items.get(i) != null) {
                count2++;
            }
        }
        long diff2 = System.currentTimeMillis() - start2;

        Log.d(TAG, "testPutManyItems: diff1 = " + diff + " diff2 = " + diff2);
        assertEquals(count, 1000000);
        assertEquals(count2, 1000050);
    }
}