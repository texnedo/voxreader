package com.texnedo.voxreader.storage;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.texnedo.voxreader.core.api.TimeLineAccess;
import com.texnedo.voxreader.core.api.TimeLineItemsProvider;
import com.texnedo.voxreader.core.timeline.BaseTimeLineItem;
import com.texnedo.voxreader.core.timeline.TimeLineItem;

import java.util.List;

public interface TimeLineStorage extends TimeLineItemsProvider, TimeLineModelFactory {
    TimeLineStorage saveItem(@NonNull BaseTimeLineItem item);

    void loadItemsRange(long from, @NonNull ItemsReceiver receiver);

    void flush();

    void delete();

    boolean exists();
}
