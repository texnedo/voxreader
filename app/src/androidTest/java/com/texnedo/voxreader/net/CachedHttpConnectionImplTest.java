package com.texnedo.voxreader.net;

import android.test.ActivityUnitTestCase;

import com.texnedo.voxreader.MainActivity_;

public class CachedHttpConnectionImplTest extends ActivityUnitTestCase {

    public CachedHttpConnectionImplTest() {
        super(MainActivity_.class);
    }

    public void testGetResponseAsString() throws Exception {
        CachedHttpConnectionImpl.clearCache(getActivity().getApplicationContext());
        HttpConnection connection = CachedHttpConnectionImpl
                .getBuilder(getInstrumentation().getContext(), "http://www.bbc.com/news/world-middle-east-35681250")
                .build();
        long start = System.currentTimeMillis();
        String data = connection.getResponseAsString();
        long diff = System.currentTimeMillis() - start;
        //
        HttpConnection connection2 = CachedHttpConnectionImpl
                .getBuilder(getActivity().getApplicationContext(), "http://www.bbc.com/news/world-middle-east-35681250")
                .build();
        long start2 = System.currentTimeMillis();
        String data2 = connection2.getResponseAsString();
        long diff2 = System.currentTimeMillis() - start2;
        assertEquals(data, data2);
        if (diff2 > diff / 5) {
            throw new IllegalArgumentException(String.format("Second read must not use network requests %d %d", diff, diff2));
        }
    }
}