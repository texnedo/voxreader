package com.texnedo.voxreader.storage;

import android.support.annotation.NonNull;

import com.texnedo.voxreader.storage.dao.DataSourceModel;

public interface DatabaseManager {
    void close();

    void insert(@NonNull DataSourceModel dataSource);

    void delete(@NonNull DataSourceModel dataSource);

    void update(@NonNull DataSourceModel dataSource);

    void deleteAll();

    void requestAll(@NonNull LoadItemsListener listener);

    void requestOne(@NonNull String id, @NonNull LoadItemsListener listener);
}
