import java.io.File;
import java.io.IOException;

import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class VoxDaoGenerator {
    private static final int SCHEME_VERSION = 1;

    public static void main(String[] args) throws Exception {
        Schema schema = new Schema(SCHEME_VERSION, "com.texnedo.voxreader.storage.dao");

        Entity sch = schema.addEntity("DataSourceModel");
        sch.addIdProperty().autoincrement();
        sch.addStringProperty("content");
        sch.addStringProperty("timelinePath");
        sch.addBooleanProperty("starred");
        sch.addIntProperty("state");
        sch.addLongProperty("lastReadId").codeBeforeField("volatile");
        sch.addIntProperty("type");
        sch.addStringProperty("description");
        sch.addStringProperty("name");
        sch.addStringProperty("image");
        sch.addStringProperty("language").codeBeforeField("volatile");

        new de.greenrobot.daogenerator.DaoGenerator() {
            @Override
            protected File toFileForceExists(String filename) throws IOException {
                return new File(filename);
            }
        }.generateAll(schema,
                "./app/src/main/java/");
    }

}
