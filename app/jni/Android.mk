LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := languagedetector
LOCAL_SRC_FILES := \
./LanguageDetector/languagedetector.cpp \
./LanguageDetector/languagedetector.h \
./LanguageDetector/internal/cldutil.cc \
./LanguageDetector/internal/cldutil_shared.cc \
./LanguageDetector/internal/compact_lang_det.cc \
./LanguageDetector/internal/compact_lang_det_hint_code.cc \
./LanguageDetector/internal/compact_lang_det_impl.cc \
./LanguageDetector/internal/debug.cc \
./LanguageDetector/internal/fixunicodevalue.cc \
./LanguageDetector/internal/generated_entities.cc \
./LanguageDetector/internal/generated_language.cc \
./LanguageDetector/internal/generated_ulscript.cc \
./LanguageDetector/internal/getonescriptspan.cc \
./LanguageDetector/internal/lang_script.cc \
./LanguageDetector/internal/offsetmap.cc \
./LanguageDetector/internal/scoreonescriptspan.cc \
./LanguageDetector/internal/tote.cc \
./LanguageDetector/internal/utf8statetable.cc \
./LanguageDetector/internal/cld_generated_cjk_uni_prop_80.cc \
./LanguageDetector/internal/cld2_generated_cjk_compatible.cc \
./LanguageDetector/internal/cld_generated_cjk_delta_bi_4.cc \
./LanguageDetector/internal/generated_distinct_bi_0.cc \
./LanguageDetector/internal/cld2_generated_quadchrome_2.cc \
./LanguageDetector/internal/cld2_generated_deltaoctachrome.cc \
./LanguageDetector/internal/cld2_generated_distinctoctachrome.cc \
./LanguageDetector/internal/cld_generated_score_quad_octa_2.cc \
./LanguageDetector/public/compact_lang_det.h \
./LanguageDetector/public/encodings.h

LOCAL_LDLIBS := -llog -lz -landroid

LOCAL_CPP_FEATURES += exceptions

include $(BUILD_SHARED_LIBRARY)