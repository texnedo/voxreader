package com.texnedo.voxreader.core.timeline;

import android.support.annotation.NonNull;

import com.texnedo.voxreader.core.api.TextToSpeechProvider;
import com.texnedo.voxreader.storage.TimeLineModel;

public class TextItem extends BaseTimeLineItem {
    protected TextItem(@NonNull TimeLineModel model) {
        super(model);
    }

    @Override
    public String toString() {
        return model.getText();
    }

    @Override
    public String getText() {
        return model.getText();
    }

    @Override
    protected void playInternal(@NonNull TextToSpeechProvider provider) {
        provider.playUtterance(getId(), getText());
    }
}
