package com.texnedo.voxreader.storage;

import android.support.annotation.NonNull;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityUnitTestCase;

import com.texnedo.voxreader.MainActivity_;
import com.texnedo.voxreader.core.timeline.BaseTimeLineItem;
import com.texnedo.voxreader.core.timeline.TimeLineItem;
import com.texnedo.voxreader.system.Config;
import com.texnedo.voxreader.system.ConfigImpl;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

@RunWith(AndroidJUnit4.class)
public class TimeLineStorageImplTest extends ActivityUnitTestCase<MainActivity_> {

    public TimeLineStorageImplTest() {
        super(MainActivity_.class);
    }

    @Test
    public void testSaveItem() throws Exception {
        Config config = new ConfigImpl(getActivity());
        TimeLineStorageImpl test = new TimeLineStorageImpl(config, "test");
        test.delete();
        final int MAX_ITEMS = 10000;
        for (int i = 0; i < MAX_ITEMS; ++i) {
            test.saveItem(
                    new TestTimeLineItem(
                            test.createModel(
                                    0,
                                    0,
                                    0,
                                    "dsafkajshdf lkajsdhf lksd" + Integer.toString(i),
                                    "ertoiuwerdsfg sdfg sdfg" + Integer.toString(i))
                    )
            );
            if (i % 100 == 0) {
                test.flush();
            }
        }
        test.flush();
        TimeLineStorageImpl test2 = new TimeLineStorageImpl(config, "test");
        int start = 0;
        int checkId = 0;
        while (true) {
            TestItemsReceiver receiver = new TestItemsReceiver();
            int end = start + 100;
            test2.loadItems(start, end, receiver);
            if (receiver.items == null || receiver.items.isEmpty()) {
                break;
            }
            for (TimeLineItem item : receiver.items) {
                assertEquals(item.getNumber(), checkId);
                checkId++;
            }
            start = end;
        }
        assertEquals(checkId, MAX_ITEMS - 1);
    }

    private static class TestItemsReceiver implements ItemsReceiver {
        public List<TimeLineItem> items;
        private final ItemFactory factory = new ItemFactory() {
            @NonNull
            @Override
            public TimeLineItem create(@NonNull TimeLineModel model) {
                return new TestTimeLineItem(model);
            }
        };

        @Override
        public ItemFactory getFactory() {
            return factory;
        }

        @Override
        public void onComplete(@NonNull List<TimeLineItem> items) {
            this.items = items;
        }

        @Override
        public void onFailure(@NonNull Exception ex) {

        }
    }

    private static class TestTimeLineItem extends BaseTimeLineItem {

        protected TestTimeLineItem(@NonNull TimeLineModel model) {
            super(model);
        }

        @Override
        public String getText() {
            return "Lorem ipsum dolor sit amet";
        }
    }
}