package com.texnedo.voxreader.ui;

import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.texnedo.voxreader.R;
import com.texnedo.voxreader.core.api.TimeLineAccess;
import com.texnedo.voxreader.storage.ItemsCountReceiver;
import com.texnedo.voxreader.system.Config;
import com.texnedo.voxreader.utils.FileLog;
import com.texnedo.voxreader.utils.UiThread;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class TimeLineAdapter implements ListAdapter {
    private static final String LOG_TAG = "TimeLineAdapter";
    //TODO - move constants to config
    private static final int LOAD_GROUP_COUNT = 20;
    //
    private static final String VISIBLE_GROUP_INDEX_KEY = "visible_group_index";
    private static final float START_NEXT_LOAD_RATIO = 0.7f;
    public static final int RESTORE_GROUP_POSITION_DELAY = 100;
    private final List<TimeLineGroupViewHolder> groups = new ArrayList<>();
    private final HashSet<DataSetObserver> setObserverList = new HashSet<>();
    private final TimeLineGroupListener groupListener = new TimeLineGroupListener();
    private final LayoutInflater inflater;
    private final Config config;
    private final TimeLineAccess access;
    private final Runnable loadNextGroupRunnable = new Runnable() {
        @Override
        public void run() {
            loadNextGroups();
        }
    };
    private final ItemsCountChangedListener listener = new ItemsCountChangedListener();
    private ListView listView;
    private long itemsCount;
    private int groupCount;
    private Bundle savedState;

    public TimeLineAdapter(@NonNull final Config config,
                           @NonNull final TimeLineAccess access,
                           @Nullable Bundle savedState) {
        this.config = config;
        this.access = access;
        this.inflater = LayoutInflater.from(config.getContext());
        this.savedState = savedState;
        loadNextGroups();
    }

    public static void onTrimMemory() {
        TimeLineGroupViewHolder.onTrimMemory();
    }

    public void onSaveInstanceState(@NonNull Bundle outState) {
        if (listView != null && listView.getChildCount() != 0) {
            outState.putInt(VISIBLE_GROUP_INDEX_KEY, listView.getLastVisiblePosition());
        }
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int position) {
        return groups.size() - 1 >= position;
    }

    private void notifyChange() {
        for (DataSetObserver observer : setObserverList) {
            observer.onChanged();
        }
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
        access.addItemsCountChangedListener(listener);
        setObserverList.add(observer);
    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {
        access.removeItemsCountChangedListener(listener);
        setObserverList.remove(observer);
    }

    @Override
    public int getCount() {
        return groupCount;
    }

    @Override
    public Object getItem(int position) {
        return groups.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            view = inflater.inflate(R.layout.timeline_item, parent, false);
        } else {
            view = convertView;
        }
        if (listView != parent) {
            listView = (ListView) parent;
        }
        TimeLineGroupViewHolder group = groups.get(position);
        TimeLineView text = (TimeLineView) view.findViewById(R.id.name);
        text.setStaticLayoutDelegate(
                listView.getWidth(),
                group.getTimeLineLayoutDelegate()
        );
        checkAndPrefetchGroups(position);
        restoreGroupPosition();
        return view;
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return groups.isEmpty();
    }

    private Integer getGroupPositionToRestore() {
        if (savedState != null) {
            int value = savedState.getInt(VISIBLE_GROUP_INDEX_KEY);
            return value == 0 ? null : value;
        }
        return null;
    }

    private void restoreGroupPosition() {
        final Integer position = getGroupPositionToRestore();
        if (position != null && listView != null) {
            UiThread.runLater(new Runnable() {
                @Override
                public void run() {
                    listView.setSelection(position);
                }
            }, RESTORE_GROUP_POSITION_DELAY);
            savedState = null;
        }
    }

    private void checkAndPrefetchGroups(int position) {
        int diff = groups.size() - position;
        if (diff > LOAD_GROUP_COUNT) {
            return;
        }
        float percent = ((float)LOAD_GROUP_COUNT - diff) / (float)LOAD_GROUP_COUNT;
        if (percent >= START_NEXT_LOAD_RATIO) {
            UiThread.run(loadNextGroupRunnable);
        }
    }

    private void setItemsCount(long count) {
        itemsCount = count;
        groupCount = (int) Math.ceil(itemsCount / (float) access.getRangeSize());
    }

    private void loadNextGroups() {
        if (itemsCount == 0) {
            access.loadCount(new ItemsCountReceiver() {
                @Override
                public void onComplete(final long count) {
                    UiThread.run(new Runnable() {
                        @Override
                        public void run() {
                            FileLog.d(LOG_TAG, "total items count %d", count);
                            setItemsCount(count);
                            addGroups();
                        }
                    });
                }

                @Override
                public void onFailure(Exception ex) {

                }
            });
        } else {
            addGroups();
        }
    }

    private Integer getActivatedGroupIndex() {
        if (access.getPlayingId() == null) {
            return null;
        }
        return (int) Math.ceil(access.getPlayingId() / (float) access.getRangeSize());
    }

    private void addGroups() {
        final int currentGroupsCount = groups.size();
        final int maxFullGroupsCount = Math.round(itemsCount / access.getRangeSize());
        final int lastGroupSize = (int)(itemsCount - maxFullGroupsCount * access.getRangeSize());
        int nextGroupCount = currentGroupsCount + LOAD_GROUP_COUNT;
        Integer activatedGroup = getActivatedGroupIndex();
        if (activatedGroup != null && activatedGroup >= nextGroupCount) {
            nextGroupCount = activatedGroup + 1;
        }
        Integer lastVisibleGroup = getGroupPositionToRestore();
        if (lastVisibleGroup != null && lastVisibleGroup >= nextGroupCount) {
            nextGroupCount = lastVisibleGroup + 1;
        }
        FileLog.d(
                LOG_TAG,
                "add more groups: current count %d, max count %d",
                currentGroupsCount,
                maxFullGroupsCount
        );
        for (int i = currentGroupsCount; i < nextGroupCount && i < maxFullGroupsCount; ++i) {
            groups.add(
                    new TimeLineGroupViewHolder(
                            config,
                            i * access.getRangeSize(),
                            i * access.getRangeSize() + access.getRangeSize() - 1,
                            access,
                            groups.size(),
                            groupListener
                    )
            );
        }
        if (lastGroupSize != 0 && groups.size() == maxFullGroupsCount) {
            groups.add(
                    new TimeLineGroupViewHolder(
                            config,
                            itemsCount - lastGroupSize,
                            itemsCount - 1,
                            access,
                            groups.size(),
                            groupListener
                    )
            );
        }
        if (groups.size() != currentGroupsCount) {
            notifyChange();
        }
    }

    private class TimeLineGroupListener implements com.texnedo.voxreader.ui.TimeLineGroupListener {
        @Override
        public void onActivated(@NonNull TimeLineGroupViewHolder group, int offset) {
            if (listView != null) {
                FileLog.d(LOG_TAG, "group %s has been activated on offset %s", group, offset);
                listView.smoothScrollToPositionFromTop(group.getPosition(), offset);
            }
        }
    }

    private class ItemsCountChangedListener implements com.texnedo.voxreader.storage.ItemsCountChangedListener {
        @Override
        public void onComplete(final long count) {
            UiThread.run(new Runnable() {
                @Override
                public void run() {
                    if (count != itemsCount) {
                        setItemsCount(count);
                        loadNextGroups();
                    }
                }
            });
        }
    }
}
