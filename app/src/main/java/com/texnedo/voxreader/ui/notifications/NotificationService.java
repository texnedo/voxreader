package com.texnedo.voxreader.ui.notifications;

import android.app.IntentService;
import android.content.Intent;
import android.text.TextUtils;

import com.texnedo.voxreader.DataSourceActivity;
import com.texnedo.voxreader.VoxReaderApp;
import com.texnedo.voxreader.utils.FileLog;

import java.util.Locale;

public class NotificationService extends IntentService {
    private static final String LOG_TAG = "NotificationService";

    public NotificationService() {
        super("NotificationService");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        FileLog.v(LOG_TAG, "service destroyed");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent == null) {
            return;
        }
        String action = intent.getAction();
        if (TextUtils.isEmpty(action)) {
            FileLog.e(LOG_TAG, "Wrong action type detected");
            return;
        }
        String id = intent.getStringExtra(NotificationBase.NOTIFICATION_ID_EXTRA);
        if (TextUtils.isEmpty(id)) {
            FileLog.e(LOG_TAG, "Wrong action payload id detected");
            return;
        }
        FileLog.v(LOG_TAG, "received extra %s from notification %s", action, id);
        switch (action) {
            case NotificationBase.NOTIFICATION_ACTION_NEXT: {
                VoxReaderApp.getManager().playNext(id);
                break;
            }
            case NotificationBase.NOTIFICATION_ACTION_PAUSE: {
                VoxReaderApp.getManager().pause(id);
                break;
            }
            case NotificationBase.NOTIFICATION_ACTION_SKIP: {
                VoxReaderApp.getManager().next(id);
                break;
            }
            case NotificationBase.NOTIFICATION_ACTION_PLAY: {
                VoxReaderApp.getManager().play(id);
                break;
            }
            case NotificationBase.NOTIFICATION_ACTION_STOP: {
                VoxReaderApp.getManager().stop(id);
                break;
            }
            case NotificationBase.NOTIFICATION_ACTION_DELETE: {
                VoxReaderApp.getManager().stop(id);
                break;
            }
            default: {
                throw new IllegalArgumentException(
                        String.format(Locale.US, "Wrong action type %s for NotificationService detected", action)
                );
            }
        }
    }
}
